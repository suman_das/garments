<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyTargetSummary extends Model
{
      protected $table   = 'daily_target_summary';
      
      protected $guarded = [];
}
