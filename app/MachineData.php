<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachineData extends Model
{
   protected $table = 'machine_data';
   protected $guarded = [
      'id','created_at', 'updated_at'
   ];
   
   
   	public function tradersinfo()
    {
        return $this->hasOne(Traders::class, 'id', 'traders_info_id');
    }
	public function ordersinfo()
    {
        return $this->hasMany(Order::class, 'order_id', 'id');
    }
}
