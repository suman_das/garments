<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
   protected $table = 'daily_targets';
   protected $guarded = [
      'id','created_at', 'updated_at'
   ];
}
