<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeptNPT extends Model
{
   protected $table = 'npt_report';
   protected $guarded = [
      'id','created_at', 'updated_at'
   ];
}
