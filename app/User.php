<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use EntrustUserTrait; // add this trait to your user model
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /* Relation(s) */
    public function type()
    {
        return $this->belongsTo('App\Role');
    }

    /**
    * An hub users to a role
    */
    public function role()
    {
        return $this->belongsTo('App\Role', 'id', 'id');
    }
    public function unit()
{
    return $this->belongsToMany('App\Unit', 'users_unit');
}
	
	public function tradersinfo()
    {
        return $this->hasMany(Traders::class, 'user_id', 'id');
    }
	public function ordersinfo()
    {
        return $this->hasMany(Order::class, 'user_id', 'id');
    }
	
	public function ordersummaryinfo()
    {
        return $this->hasMany(OrderSummary::class, 'user_id', 'id');
    }
}
