<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
    use SoftDeletes;
    
   protected $table = 'unit';
   protected $guarded = [
      'id','created_at', 'updated_at'
   ];

   protected $dates=[
    'deleted_at'
];
   
   	
}
