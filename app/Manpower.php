<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manpower extends Model
{
   protected $table = 'manpower';
   protected $guarded = [
      'id','created_at', 'updated_at'
   ];
}
