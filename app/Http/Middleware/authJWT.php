<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;

class authJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::toUser($request->input('token'));
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                   $content = [
                    'data' => [],
                    'error' =>'Token is Invalid' ,
                    'status' => 'Fail',
                    'status_code' => 400
                    ];
                return response()->json($content);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                $content = [
                    'data' => [],
                    'error' =>'Token is Expired' ,
                    'status' => 'Fail',
                    'status_code' => 400
                    ];
                return response()->json($content);
            }else{
                $content = [
                    'data' => [],
                    'error' =>'Something is wrong' ,
                    'status' => 'Fail',
                    'status_code' => 400
                    ];
                return response()->json($content);
            }
        }
        return $next($request);
    }
}
