<?php
namespace App\Http\Controllers\SuperAdmin\Product;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\User;
use Datatables;
use App\Role;

use App\UserType;

use App\Product;
use App\Order;
use App\OrderSummary;
use App\Traders;
use Validator;
use Session;
use Redirect;
use Image;
use DB;
use Auth;
use Entrust;
use Excel;


class ProductDetailsController extends Controller
{
    private $view_path = "core.superadmin.product.";
    private $route_path = "superadmin.Product.";
    public function index() {
   
      $traders = Traders::groupBy('ppo')->pluck('ppo','user_id')->toArray();
        return view($this->view_path.'index',compact('traders'));
    }
    
    public function dataTableList(Request $request)
    {
        $requestData = $request->all();
        
        $tdate=$requestData['columns'][1]['search']['value'];
        $fdate=$requestData['columns'][0]['search']['value'];
        
        $today=date('Y-m-d H:i:s');
   
        $columns = array(

            0 => 'id'
        );

        $query = OrderSummary::with('tradersinfo')->select('*');
        
        $query2 = OrderSummary::with('tradersinfo')->select('*');
        $parameter = array();
       

        if (!empty($requestData['columns'][3]['search']['value'])) {   //name
            $parameter["ppo"]=$requestData['columns'][3]['search']['value'];
            $query->where('order_summary.user_id','=',$requestData['columns'][3]['search']['value']);
            $query2->where('order_summary.user_id','=',$requestData['columns'][3]['search']['value']);
        }
        
        if (!empty($requestData['columns'][2]['search']['value'])) {   //name
            $parameter["traders_info_id"]=$requestData['columns'][2]['search']['value'];
            $query->where('order_summary.traders_info_id','=',$requestData['columns'][2]['search']['value']);
            $query2->where('order_summary.traders_info_id','=',$requestData['columns'][2]['search']['value']);
        }
        
        
        $parameter["tdate"]=$requestData['columns'][1]['search']['value'];
        $parameter["fdate"]=$requestData['columns'][0]['search']['value'];
        
        if( !empty($requestData['columns'][1]['search']['value']) )
        {
            $tdate=$requestData['columns'][1]['search']['value'];
        }  else {
            $tdate=date('Y-m-d  H:i:s');
        }
        if ((!empty($fdate))&&!empty($tdate)) 
             {
                
               $query->whereBetween('order_summary.created_at',array($fdate,$tdate));
                $query2->whereBetween('order_summary.created_at',array($fdate,$tdate));
             
        }else
        {
            $query->whereDate('order_summary.created_at','<',$today);
            $query2->whereDate('order_summary.created_at','<',$today);
             $fdate='';
        }
        
       
       // print_r($query->toSql());exit;
        $requestData['order'][0]['dir'] = 'DESC';
        
        $totalData = count($query2->get()->toarray());
        $totalFiltered = $totalData;
        $query2->orderBy('order_summary.'.$columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir']);
        $query2->offset($requestData['start']);
        $query2->limit($requestData['length']);
        
       
        $query =$query2->get()->toarray();
        //print_r($query);exit;
        $data = array();
        $j = 0;
        foreach($query as $row) {
          
            $nestedData = array();
            $nestedData[] = $row["tradersinfo"]["outlet_code"];
            $nestedData[] = $row["tradersinfo"]["outlet_name"];
            $nestedData[] =  $row["tradersinfo"]["ppo"];
            $nestedData[] =  $row["tradersinfo"]["territory"];
            $nestedData[]= $row["price"];
            $nestedData[]= $row["created_at"];
            $nestedData[]= '<a href="'.route($this->route_path."ProductDetails.view", $row["id"]).'">View Details</a>';
            $data[] = $nestedData;
        }
    
        //print_r($data);exit;
        //$query = '<tbody class="employee-grid-error"><tr><th colspan="3"><a href="'.$url.'?queryinfo=' .  http_build_query($parameter) . '">Download CSV </a></th></tr></tbody>';
        //$query2 = '<tbody class="employee-grid-error"><tr><th colspan="3"><a href="download_sms_summaryLog.php?queryinfo=' . $queryData . '">Download Summary Report CSV </a></th></tr></tbody>';
        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data,
            "query"=>$query
           
                // total data array
        );

        echo json_encode($json_data);
    }
    
    public function downloadExcel()

	{
            $today=date('Y-m-d H:i:s');
            if (Input::has('tdate'))
            {
               $tdate=Input::get('tdate'); 
            }else
            {
                 $tdate=date('Y-m-d  H:i:s');
            }
            if (Input::has('fdate'))
            {
                $fdate=Input::get('fdate'); 
            }
            
            $query = uploadCode::with('userinfo')->select('*');
            
            
            if (Input::has('dealer_code'))
            {
              $dealer_code= Input::get('dealer_code');
              $query->whereHas('userinfo', function ($quee) use ($requestData){
                    $quee->where('users.dealer_code', '=', $requestData['columns'][2]['search']['value']);
                });  
            }
            
            
            if (Input::has('customer_mobile'))
            {   
                $customer_mobile= Input::get('customer_mobile');
                $query->where('product_information.customer_mobile','=',$customer_mobile);  
            }
            
            if (Input::has('status'))
            {   
                $status= Input::get('status');
                $query->where('product_information.status','=',$status);  
            }
            
           
            if (Input::has('w_stock_staus'))
            {   
                $w_stock_staus= Input::get('w_stock_staus');
                $query->where('product_information.w_stock_staus','=',$w_stock_staus);  
            }
            if (Input::has('model'))
            {   
                $model= Input::get('model');
                $query->where('product_information.model','=',$model);  
            }
          
            
            if ((!empty($fdate))&&!empty($tdate)) 
             {
               $query->whereBetween('product_information.created_at',array($fdate,$tdate));
             
            }else
            {
                $query->whereDate('product_information.created_at','<',$today);
                
                 $fdate='';
            }
            
            $data = $query->get()->toArray();
            $data2=array();
             foreach($data as $key=>$row) {
           
            $nestedData = array();
            $nestedData["Dealer Name"] = $row["userinfo"]["name"];
            $nestedData["Dealer Code"] = $row["userinfo"]["dealer_code"];
            $nestedData["Imei 1"] = $row["imei1"];
            $nestedData["Imei 2"]= $row["imei2"];
            $nestedData["Serial No."]= $row["sl"];;
            $nestedData["Activation Mobile"]= $row["customer_mobile"];
            $nestedData["Color"]= $row["color"];
            $nestedData["Expire date"]= $row["sl"];
            $nestedData["STS"]= $row["sts_status"];
            $nestedData["Stock"]= $row["w_stock_staus"];
            $nestedData["Created AT"]= $row["created_at"];
           
            
            $data2[] = $nestedData;
        }
            return Excel::create('ReportLog', function($excel) use ($data2) {

                    $excel->sheet('ReportLog', function($sheet) use ($data2)

            {

                            $sheet->fromArray($data2);

            });

            })->download('csv');

	}
        public function delete(Request $request) {
            
            
            $tasks = uploadCode::find($request->id);
             
            $clone  = $tasks->replicate();
            unset($clone['created_at'],$clone['updated_at']);
            $data = json_decode($clone, true);
            //print_r($data);exit;
            Product::create($data);

//            $data = json_decode($clone, true);
//            Product::create($data);

            $productinfo = uploadCode::find($request->id);
            $productinfo->delete();
            return response()->json([], 200);
            
        }
        
         public function traders(Request $request)
        {
            $requestData = $request->all();
            return $traders = Traders::where('user_id', '=',$requestData["ppo"])->pluck('outlet_code','id')->toArray();
            
        }
         public function view(Request $request,$id)
        {
            
            $order = OrderSummary::with('tradersinfo','ordersinfo')->where('id',$id)->select('*')->first();
            //dd($order);exit;
             return view($this->view_path.'view',compact('order'));
           
            
        }
}
