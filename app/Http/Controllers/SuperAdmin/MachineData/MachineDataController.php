<?php

namespace App\Http\Controllers\SuperAdmin\MachineData;

use App\Http\Controllers\Controller;

use App\Role;
use Validator;
use Redirect;
use App\User;
use App\MachineData;
use App\UserType;
use Session;


use Illuminate\Http\Request;

class MachineDataController extends Controller
{
  private $view_path = "core.superadmin.machinedata.";
  private $route_path = "superadmin.machinedata.";

  public function index(Request $request )
  {
    //$roles = \DB::table('roles')->orderBy('display_name')->get();
    $users = "";
    $old = [ 'unit' => '','start_date' => ''];

    if($request->has('query'))
    {
        $old['unit'] = $request->input('unit');
        $old['date'] = $request->input('date');
        $manpower =MachineData::select('*');
        if ($request->has('unit')) {
         $manpower->where('unit', $request->input('unit'));
         }
        if ($request->has('date')) {
            $manpower->whereDate('date', $request->input('date'));
        }
     //dd($request->has('date'));exit;
        $manpowers = $manpower->paginate(1);
     
    }else{
      $manpowers = MachineData::paginate(1);;
     
    }
    return view($this->view_path.'index', compact('manpowers'));
  }

  public function create()
  {
    return view($this->view_path.'create');
  }

  
  public function store(Request $request)
  {
      
        $alldata=$request->all();
        $validation = Validator::make($request->all(), [
                'rented_machine' => 'required',
                'idle_machine' => 'required',
                'used_machine' => 'required',
                'sample_section' => 'required',
                'finishing_section' => 'required',
                'cutting_section' => 'required',
                'power' => 'required',
                'mvc_godwon' => 'required',
                
                
            ]);

        if($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        } 
    
   // dd($dealer_code);
    $Order = new MachineData;
    $Order->fill($request->except('query','_token'));
    $Order->date=date('Y-m-d');
    $Order->save();
    
    Session::flash('success', 'You have been successfully added new data!');

    return redirect()->route($this->route_path.'index');
  }

  public function show(Request $request, $id)
  {
    $user = User::findOrFail($id);
    return view($this->view_path.'show', compact('user'));
  }

  public function edit(Request $request, $id)
  {
    $manpower = MachineData::findOrFail($id);
    Session::flash('success', 'You have been successfully updaed new data!');
    return view($this->view_path.'edit', compact('manpower'));
  }

  public function update(Request $request, $id)
  {
    $alldata=$request->all();
    $validation = Validator::make($request->all(), [
                'rented_machine' => 'required',
                'idle_machine' => 'required',
                'used_machine' => 'required',
                'sample_section' => 'required',
                'finishing_section' => 'required',
                'cutting_section' => 'required',
                'power' => 'required',
                'mvc_godwon' => 'required',
                
                
            ]);

    if($validation->fails()) {
        return Redirect::back()->withErrors($validation)->withInput();
    } 

      
      
    $manpower = Order::MachineData($id);
    $manpower->fill($request->except('query','_token'));
    $manpower->save();
   Session::flash('success', 'You have been successfully updaed new data!');
    return redirect()->route($this->route_path.'index');
  }
  
 
}
