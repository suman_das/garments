<?php

namespace App\Http\Controllers\SuperAdmin\Manpower;

use App\Http\Controllers\Controller;

use App\Role;
use Validator;
use Redirect;
use App\User;
use App\Manpower;
use App\UserType;
use Session;


use Illuminate\Http\Request;

class ManpowerController extends Controller
{
  private $view_path = "core.superadmin.manpower.";
  private $route_path = "superadmin.manpower.";

  public function index(Request $request )
  {
    //$roles = \DB::table('roles')->orderBy('display_name')->get();
    $users = "";
    $old = [ 'unit' => '','date' => ''];

    if($request->has('query'))
    {
        $old['unit'] = $request->input('unit');
        $old['date'] = $request->input('date');
        $manpower =manpower::select('*');
        if ($request->has('unit')) {
         $manpower->where('unit', $request->input('unit'));
         }
        if ($request->has('date')) {
            $manpower->whereDate('date', $request->input('date'));
        }
     //dd($request->has('date'));exit;
        $manpowers = $manpower->paginate(1);
     
    }else{
      $manpowers = manpower::paginate(1);;
     
    }
    return view($this->view_path.'index', compact('manpowers'));
  }

  public function create()
  {
    return view($this->view_path.'create');
  }

  
  public function store(Request $request)
  {
      
        $alldata=$request->all();
        $validation = Validator::make($request->all(), [
                'p_operator' => 'required',
                'p_swing_iron' => 'required',
                'p_lc_sv' => 'required',
                'date' => 'required',
                
            ]);

        if($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        } 
    
   // dd($dealer_code);
    $manpower = new manpower;
    $manpower->fill($request->except('query','_token'));
    $manpower->save();
    
    Session::flash('success', 'You have been successfully added new data!');

    return redirect()->route($this->route_path.'index');
  }

  public function show(Request $request, $id)
  {
    $user = User::findOrFail($id);
    return view($this->view_path.'show', compact('user'));
  }

  public function edit(Request $request, $id)
  {
    $manpower = manpower::findOrFail($id);
    Session::flash('success', 'You have been successfully updaed new data!');
    return view($this->view_path.'edit', compact('manpower'));
  }

  public function update(Request $request, $id)
  {
    $alldata=$request->all();
    $validation = Validator::make($request->all(), [
            'p_operator' => 'required',
            'p_swing_iron' => 'required',
            'p_lc_sv' => 'required',
            'date' => 'required',

        ]);

    if($validation->fails()) {
        return Redirect::back()->withErrors($validation)->withInput();
    } 

      
      
    $manpower = manpower::findOrFail($id);
    $manpower->fill($request->except('query','_token'));
    $manpower->save();
   Session::flash('success', 'You have been successfully updaed new data!');
    return redirect()->route($this->route_path.'index');
  }
  
 
}
