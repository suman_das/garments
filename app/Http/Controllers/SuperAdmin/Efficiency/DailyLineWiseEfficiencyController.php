<?php

namespace App\Http\Controllers\SuperAdmin\DeptNPT;

use App\Http\Controllers\Controller;

use App\Role;
use Validator;
use Redirect;
use App\User;
use App\DeptNPT;
use App\UserType;
use Session;


use Illuminate\Http\Request;

class DeptNPTController extends Controller
{
  private $view_path = "core.superadmin.npt.";
  private $route_path = "superadmin.npt.";

  public function index(Request $request )
  {
    //$roles = \DB::table('roles')->orderBy('display_name')->get();
    $users = "";
    $old = [ 'unit' => '','start_date' => ''];

    if($request->has('query'))
    {
        $old['unit'] = $request->input('unit');
        $old['date'] = $request->input('date');
        $manpower =DeptNPT::select('*');
        if ($request->has('unit')) {
         $manpower->where('unit', $request->input('unit'));
         }
        if ($request->has('start_date')) {
            $manpower->whereDate('start_date', $request->input('date'));
        }
     //dd($request->has('date'));exit;
        $manpowers = $manpower->paginate(1);
     
    }else{
      $manpowers = DeptNPT::paginate(1);;
     
    }
    return view($this->view_path.'index', compact('manpowers'));
  }

  public function create()
  {
    return view($this->view_path.'create');
  }

  
  public function store(Request $request)
  {
      
        $alldata=$request->all();
        $validation = Validator::make($request->all(), [
                'tch' => 'required',
                'mr' => 'required',
                'mch' => 'required',
                'cd' => 'required',
                'ped' => 'required',
                'pd' => 'required',
                'pl' => 'required',
                'qa' => 'required',
                'mb' => 'required',
                'ad' => 'required',
                'fd' => 'required',
                'pf' => 'required',
                'vp' => 'required',
                'line' => 'required',
                'unit' => 'required',
                
            ]);

        if($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        } 
    
   // dd($dealer_code);
    $Order = new DeptNPT;
    $Order->fill($request->except('query','_token'));
    $Order->date=date('Y-m-d');
    $Order->save();
    
    Session::flash('success', 'You have been successfully added new data!');

    return redirect()->route($this->route_path.'index');
  }

  public function show(Request $request, $id)
  {
    $user = User::findOrFail($id);
    return view($this->view_path.'show', compact('user'));
  }

  public function edit(Request $request, $id)
  {
    $manpower = DeptNPT::findOrFail($id);
    Session::flash('success', 'You have been successfully updaed new data!');
    return view($this->view_path.'edit', compact('manpower'));
  }

  public function update(Request $request, $id)
  {
    $alldata=$request->all();
    $validation = Validator::make($request->all(), [
                'tch' => 'required',
                'mr' => 'required',
                'mch' => 'required',
                'cd' => 'required',
                'ped' => 'required',
                'pd' => 'required',
                'pl' => 'required',
                'qa' => 'required',
                'md' => 'required',
                'ad' => 'required',
                'fd' => 'required',
                'pf' => 'required',
                'vp' => 'required',
                'line' => 'required',
                'unit' => 'required',
                
            ]);

    if($validation->fails()) {
        return Redirect::back()->withErrors($validation)->withInput();
    } 

      
      
    $manpower = Order::DeptNPT($id);
    $manpower->fill($request->except('query','_token'));
    $manpower->save();
   Session::flash('success', 'You have been successfully updaed new data!');
    return redirect()->route($this->route_path.'index');
  }
  
 
}
