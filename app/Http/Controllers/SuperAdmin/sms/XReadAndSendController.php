<?php

namespace App\Http\Controllers\sms;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\User;
use Datatables;
use App\Role;
use App\UserType;
use App\ReadAndSend;
use App\CustomerInfo;
use App\ServiceLog;
use App\SMSLog;
use Validator;
use Session;
use Redirect;
use Image;
use DB;
use Auth;
use Entrust;
use Carbon\Carbon;

class ReadAndSendController extends Controller {

    public function getSms(Request $request) {
        //print_r($request->all());exit;
         @$sms = Input::get('sms');
         @$msisdn = Input::get('msisdn');
         //ACIP<space>Code Number<space>Buyer Number and send to 6969
        if (isset($sms)&&(!empty($sms))&&(!empty($msisdn))) {
            @$sms = Input::get('sms');
            @$msisdn = Input::get('msisdn');
            $msisdn = '88' . substr($msisdn, -11);
            $hotkey=substr($sms, 0, 4);
            $smsString = trim(substr($sms, 4));
            $code = trim(strstr($smsString, ' ', true));
            $customerMobileNo = trim(strstr($smsString, ' '), " ");
//            $customerMobileNo = trim(strstr($chasisNO_string, ' ', true));
//            $name = trim(strstr($chasisNO_string, ' '), " ");
            $current = Carbon::now();
            $expireDate= $current->addDays(365);
            $dealerInfo = DB::table('dealerinfo')->where('mobile', $msisdn)->first();
            $dealerinfoQuantity = \App\dealerDetails::where('mobile', $msisdn)
                    ->count();
            //print_r($dealerInfo);exit;
            if ($dealerinfoQuantity>0) {
                $dealerInfoId = $dealerInfo->id;
                $dealer_code = $dealerInfo->dealer_code;
                $dealer_name = $dealerInfo->dealer_name;
                $territory = $dealerInfo->territory;
                
            }  else {
                $dealerInfoId='';
                $dealer_code='';
                $dealer_name='';
                $territory='';
            }
            
            $codeInfo = DB::table('aci_code_info')->where('pump_code',$code)->first();
            $codeInfoQuantity = \App\CodeDetails::where('pump_code', $code)
                    ->count();
            if ($codeInfoQuantity>0) {
                $codeInfoId = $codeInfo->id;
                $pump_code = $codeInfo->pump_code;
                $model = $codeInfo->model;
                $amount = $codeInfo->amount;
                $days = $codeInfo->warranty;
                $expireDate= $current->addDays($days);
            }else {
                $codeInfoId='';
                $pump_code='';
                $model='';
                $amount='';
                $expireDate='';
            }
            
            if (($dealerinfoQuantity > 0) && ($codeInfoQuantity > 0)) {
                if ($codeInfo->status == 0) {
                    $status='Success';
                    DB::table('aci_code_info')
                            ->where('pump_code', $code)
                            ->update(['status' => 1,'dealer_id' => $dealerInfoId,'buyer_mobile'=>$customerMobileNo]);
                    $replyText = "Sothik Code Number. Agami 72 Ghontar bhetore apnar registered bkash number-e $amount Taka Pouche jabe. Amader Sathei Thakun.";
                    $customerReply="Congratulation! You have purchased Original ACI PUMP. Your validity will expire on $expireDate.";
                    $status='Success';
                } elseif ($codeInfo->status == 1) {
                    $replyText = "This Motors Already Sold";
                    $status='Duplicate';
                }
            }  else {
                if($codeInfoQuantity<=0){
                    $replyText = " Apni bhul Code number probesh korechen. Onugroho kore sothik code number diye chesta korun.";
                    $status='Wrong Code';
                }else
                {
                    $replyText = "Dukkhito ! Apnar Mobile Number ei Shebar jonno projojjo nohe." ;
                    $status='Wrong Dealer';
                }
                
            }
            $smsBody=$sms;
                if($status=='Success'){
                    $user ="aci_pump"; 
                    $pass = 't23<0N18'; 
                    $sid = "ACIPUMP";
                    $contact=$customerMobileNo;
                    $sms=$customerReply;
                    //$url="http://sms.sslwireless.com/pushapi/idea_networks/server.php";t23<0N18
                    $url="http://192.168.92.138/pushapi/dynamic/server.php";
                    $unique_id="acisms";
                    $param="user=$user&pass=$pass&sid=$sid&";	
                    $sms="sms[0][0]=$contact&sms[0][1]=".urlencode($sms)."&sms[0][2]=$unique_id";
                    $data=$param.$sms.$sid;
                    $crl = curl_init();
                    curl_setopt($crl,CURLOPT_SSL_VERIFYPEER,FALSE);
                    curl_setopt($crl,CURLOPT_SSL_VERIFYHOST,2);
                    curl_setopt($crl,CURLOPT_URL,$url); 
                    curl_setopt($crl,CURLOPT_HEADER,0);
                    curl_setopt($crl,CURLOPT_RETURNTRANSFER,1);
                    curl_setopt($crl,CURLOPT_POST,1);
                    curl_setopt($crl,CURLOPT_POSTFIELDS,$data); 
                    echo $response = curl_exec($crl);
                    curl_close($crl);  
                }else
                {
                    $customerReply='';
                    $response='';
                }
                
                 $SmsLog = new SMSLog();
                    $SmsLog->dealer_code=$dealer_code;
                    $SmsLog->plumber_msisdn=$msisdn;
                    $SmsLog->dealer_name=$dealer_name;
                    $SmsLog->territory=$territory;
                    $SmsLog->sms_body=$smsBody;
                    $SmsLog->reply_body=$replyText;
                    $SmsLog->customer_mobile=$customerMobileNo;
                    $SmsLog->status=$status;
                    $SmsLog->expireDate=$expireDate;
                    $SmsLog->amount=$amount;
                    $SmsLog->model=$model;
                    $SmsLog->pump_code=$code;
                    $SmsLog->save();
                
                
                
                echo $replyText;
            
            
        }
    }

    


}
