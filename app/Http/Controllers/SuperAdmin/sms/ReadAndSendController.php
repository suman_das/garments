<?php

namespace App\Http\Controllers\sms;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\User;
use Datatables;
use App\Role;
use App\UserType;
use App\ReadAndSend;
use App\CustomerInfo;
use App\ServiceLog;
use App\SMSLog;
use Validator;
use Session;
use Redirect;
use Image;
use DB;
use Auth;
use Entrust;
use Carbon\Carbon;

class ReadAndSendController extends Controller {

    public function getSms(Request $request) {
        //print_r($request->all());exit;
         @$sms = Input::get('sms');
         @$msisdn = Input::get('msisdn');
         //ACIP<space>Code Number<space>Buyer Number and send to 6969
        if (isset($sms)&&(!empty($sms))&&(!empty($msisdn))) {
            @$sms = Input::get('sms');
            @$msisdn = Input::get('msisdn');
            $msisdn = '88' . substr($msisdn, -11);
            $hotkey=substr($sms, 0, 4);
            $smsString = trim(substr($sms, 4));
            $code = trim(strstr($smsString, ' ', true));
            $customerMobileNo =trim(strstr($smsString, ' '), " ");
//            $customerMobileNo = trim(strstr($chasisNO_string, ' ', true));
//            $name = trim(strstr($chasisNO_string, ' '), " ");
            $current = Carbon::now();
            //$expireDate= $current->addDays(365);
            $dealerInfo = DB::table('dealerinfo')->where('mobile', $msisdn)->first();
            $dealerinfoQuantity = \App\dealerDetails::where('mobile', $msisdn)
                    ->count();
            //print_r($dealerInfo);exit;
            if ($dealerinfoQuantity>0) {
                $dealerInfoId = $dealerInfo->id;
                $dealer_code = $dealerInfo->dealer_code;
                $dealer_name = $dealerInfo->dealer_name;
                $territory = $dealerInfo->territory;
                
            }  else {
                $dealerInfoId='';
                $dealer_code='';
                $dealer_name='';
                $territory='';
            }
            
            $codeInfo = DB::table('aci_code_info')->where('pump_code',$code)->first();
            $codeInfoQuantity = \App\CodeDetails::where('pump_code', $code)
                    ->count();
            if ($codeInfoQuantity>0) {
                $codeInfoId = $codeInfo->id;
                $pump_code = $codeInfo->pump_code;
                $model = $codeInfo->model;
                $amount = $codeInfo->amount;
                $days = $codeInfo->warranty;
                $expireDate= $current->addDays($days);
            }else {
                $codeInfoId='';
                $pump_code='';
                $model='';
                $amount='';
                $expireDate='';
            }
			$pattern = "/^(?:\+?88)?01[15-9]\d{8}$/";


			if(preg_match($pattern,$customerMobileNo)) 
			{
			  $notvalid=1;

			  
			}else{
				$notvalid=0;
			}
			//echo $notvalid;exit;
            if(empty($customerMobileNo)||(empty($code))||($notvalid ==0)){
				 $replyText = "আপনি ভুল ফরম্যাটে মেসেজ পাঠিয়েছেন। অনুগ্রহ করে সঠিক ফরম্যাটে মেসেজ পাঠান ।";
                $status='Invalid';
			}
            elseif(($dealerinfoQuantity > 0) && ($codeInfoQuantity > 0)) {
                if ($codeInfo->status == 0) {
                    $status='Success';
                    DB::table('aci_code_info')
                            ->where('pump_code', $code)
                            ->update(['status' => 1,'dealer_id' => $dealerInfoId,'buyer_mobile'=>$customerMobileNo]);
                    $replyText = "সঠিক কোড নাম্বার। আগামী ৭২ ঘন্টার ভেতর আপনার বিকাশ নাম্বারে $amount টাকা পৌঁছে যাবে।";
					//$replyText = "সঠিক কোড নাম্বার। ঈদের ছুটির পর আপনার বিকাশ নাম্বারে $amount টাকা পৌঁছে যাবে।";
                    $customerReply="স্বাগতম ! আপনি সঠিক এসিআই পাম্প ক্রয় করেছেন। আপনার ওয়ারেন্টির মেয়াদঃ $expireDate  ।";
                    $status='Success';
                } elseif ($codeInfo->status == 1) {
                    $replyText = "দুঃখিত, এই কোডটি ইতিপূর্বে ব্যবহার করা হয়েছে|";
                    $status='Duplicate';
                }
            }  else {
                if($dealerinfoQuantity<=0){
					$replyText = "দুঃখিত, আপনার নাম্বার এই সেবার জন্য প্রযোজ্য নয়।" ;
                    $status='Wrong Code';
                }else
                {
                    
					$replyText = "আপনি ভুল কোড নাম্বার প্রবেশ করেছেন। অনুগ্রহ করে সঠিক কোড দিয়ে চেষ্টা করুন।";
                    $status='Wrong Dealer';
                }
                
            }
            $smsBody=$sms;
                if($status=='Success'){
                    $user ="aci_pump"; 
                    $pass = '25<0G64p'; 
                    $sid = "ACIPUMPBD";
                    $contact=$customerMobileNo;
                    //$sms=$customerReply;
					$sms=strtoupper(bin2hex(iconv('UTF-8','UCS-2BE',$customerReply)));
                    //$url="http://sms.sslwireless.com/pushapi/idea_networks/server.php";t23<0N18
                    $url="http://192.168.92.138/pushapi/dynamic/server.php";
                    $unique_id="acisms";
                    $param="user=$user&pass=$pass&sid=$sid&";	
                    $sms="sms[0][0]=$contact&sms[0][1]=".urlencode($sms)."&sms[0][2]=$unique_id";
                    $data=$param.$sms.$sid;
                    $crl = curl_init();
                    curl_setopt($crl,CURLOPT_SSL_VERIFYPEER,FALSE);
                    curl_setopt($crl,CURLOPT_SSL_VERIFYHOST,2);
                    curl_setopt($crl,CURLOPT_URL,$url); 
                    curl_setopt($crl,CURLOPT_HEADER,0);
                    curl_setopt($crl,CURLOPT_RETURNTRANSFER,1);
                    curl_setopt($crl,CURLOPT_POST,1);
                    curl_setopt($crl,CURLOPT_POSTFIELDS,$data); 
                    echo $response = curl_exec($crl);
                    curl_close($crl);  
                }else
                {
                    $customerReply='';
                    $response='';
                }
                
                 $SmsLog = new SMSLog();
                    $SmsLog->dealer_code=$dealer_code;
                    $SmsLog->plumber_msisdn=$msisdn;
                    $SmsLog->dealer_name=$dealer_name;
                    $SmsLog->territory=$territory;
                    $SmsLog->sms_body=$smsBody;
                    $SmsLog->reply_body=$replyText;
                    $SmsLog->customer_mobile=$customerMobileNo;
                    $SmsLog->status=$status;
                    $SmsLog->expireDate=$expireDate;
                    $SmsLog->amount=$amount;
                    $SmsLog->model=$model;
                    $SmsLog->pump_code=$code;
                    $SmsLog->save();
                
                
                
                echo 'NO';
				$user ="aci_pump"; 
				$pass = '25<0G64p'; 
				$sid = "ACIPUMPBD";
				$contact=$msisdn;
				//$sms=$customerReply;
				$sms=strtoupper(bin2hex(iconv('UTF-8','UCS-2BE',$replyText)));
				//$url="http://sms.sslwireless.com/pushapi/idea_networks/server.php";t23<0N18
				$url="http://192.168.92.138/pushapi/dynamic/server.php";
				$unique_id="acisms";
				$param="user=$user&pass=$pass&sid=$sid&";	
				$sms="sms[0][0]=$contact&sms[0][1]=".urlencode($sms)."&sms[0][2]=$unique_id";
				$data=$param.$sms.$sid;
				$crl = curl_init();
				curl_setopt($crl,CURLOPT_SSL_VERIFYPEER,FALSE);
				curl_setopt($crl,CURLOPT_SSL_VERIFYHOST,2);
				curl_setopt($crl,CURLOPT_URL,$url); 
				curl_setopt($crl,CURLOPT_HEADER,0);
				curl_setopt($crl,CURLOPT_RETURNTRANSFER,1);
				curl_setopt($crl,CURLOPT_POST,1);
				curl_setopt($crl,CURLOPT_POSTFIELDS,$data); 
				$response = curl_exec($crl);
				curl_close($crl);  
            
            
        }
    }

    


}
