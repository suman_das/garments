<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\UserType;
use App\Unit;
use App\User;
use App\DailyTargetSummary;
use DB;
use App\Manpower;
use App\Target;
use Carbon\Carbon;

class DashboardController extends Controller {

  private $view_path = "core.superadmin.";

  public function dashboard()
  {
    $roles = \Auth::user()->roles;
    $units = \Auth::user()->unit->first()->id;
    $time    = Carbon::now()->addHour()->format('H');
    $hours   = DailyTargetSummary::join('daily_targets', 'daily_target_summary.target_id', '=', 'daily_targets.id')->join('order_info', 'daily_targets.order_id', '=', 'order_info.id')
                  ->where('daily_target_summary.l_date', date('Y-m-d'))
                  ->where('order_info.unit_id', $units)
                  ->where('daily_target_summary.hour_no', '<=', $time)
                  ->where('daily_target_summary.initial_target', '!=', 0)
                  ->pluck('daily_target_summary.hour_no')
                  ->toArray();
    $totalMan=Manpower::where('unit_id','=',$units)->where('date',date('Y-m-d'))->sum('total_manpower');
    $totalMachine=DB::table('machine_data')->where('unit_id','=',$units)->where('date',date('Y-m-d'))->sum('total');
    $totalOperator=Manpower::where('unit_id','=',$units)->where('date',date('Y-m-d'))->sum('p_swing_iron');
    $plcsv=Manpower::where('unit_id','=',$units)->where('date',date('Y-m-d'))->sum('p_lc_sv');
    $totalManpower=$totalOperator+$plcsv;
    
    $data   = DB::table('daily_targets')->select('daily_targets.id','daily_targets.sam','daily_targets.p_operator','daily_targets.s_helper_ironman','daily_targets.order_id','daily_targets.line_no')->join('order_info', 'daily_targets.order_id', '=', 'order_info.id')
                  ->whereDate('daily_targets.created_at', Carbon::today())->get()
                  ->toArray();
    //print_r($data);exit;
    $jsonEficiency='';
    $totalProduct=0;
    $eficiencyLine=array();
    $eficiency=array();
    foreach ($data as $value) {
        
        
        $sam=$value->sam;
        $manpower=$value->p_operator+$value->s_helper_ironman;
       
        foreach ($hours as $hour) {
            $totalNPT=0;
             $totalHourlyProduct=0;
             $totalLineProduct=0;
            $totalHourlyProduct =DailyTargetSummary::join('daily_targets', 'daily_target_summary.target_id', '=', 'daily_targets.id')
                    ->join('order_info', 'daily_targets.order_id', '=', 'order_info.id')
                    ->where('unit_id','=',$units)
                    ->where('hour_no','=',$hour)
                    ->where('target_id','=',$value->id)
                    ->sum('output');
            $totalLineProduct =DailyTargetSummary::join('daily_targets', 'daily_target_summary.target_id', '=', 'daily_targets.id')
                    ->join('order_info', 'daily_targets.order_id', '=', 'order_info.id')
                    ->where('unit_id','=',$units)
                    ->where('hour_no','=',$hour)
                    ->where('target_id','=',$value->id)
                    ->where('daily_target_summary.line_no', '=', $value->line_no)
                    ->sum('output');
            
            $stats = DB::table('npt_report')
                ->select(DB::raw('sum(tch + mr+mch+cd+ped+pd+pl+qa+mb+ad+fd+pf+vp) as npt'))
                ->where('unit_id', '=', $units)
                ->where('date', '=', date('Y-m-d'))
                ->where('hour_no', '=', $hour)
                ->where('order_no', '=', $value->order_id)->get();
            
            $statsLine = DB::table('npt_report')
                ->select(DB::raw('sum(tch + mr+mch+cd+ped+pd+pl+qa+mb+ad+fd+pf+vp) as npt'))
                ->where('unit_id', '=', $units)
                ->where('date', '=', date('Y-m-d'))
                ->where('hour_no', '=', $hour)
                ->where('line', '=', $value->line_no)
                ->where('order_no', '=', $value->order_id)->get();
            if(!count($stats)){
               $sam1=$stats->npt=0; 
            } else {
                $sam1=$stats->npt=0;
            }
            if(!count($statsLine)){
               $nptLine=$statsLine->npt=0; 
            } else {
                $nptLine=$statsLine->npt=0;
            }
            $totalNPT =$sam1;
            //print_r($value->line_no);exit;
            $lineNO=$value->line_no;
            //$totalHourlyProduct ['data']['sam'][$hour]=$totalNPT;
            $totalProduct +=$totalHourlyProduct;
           $eficiency[$hour][]=((($totalHourlyProduct*$sam)/($manpower*(1+$totalNPT)*60-(68*3*60)+(67*3*60+5*1*60)))/100);
           $eficiencyLine[$lineNO][]=((($totalLineProduct*$sam)/($manpower*(1+$nptLine)*60-(68*3*60)+(67*3*60+5*1*60)))/100);
             
        }
    
       
           
    }
    $eficiencyLineArray=array();
     foreach ($eficiencyLine as $k=>$subArray) {
           $eficiencyLineArray[$k]= (array_sum($subArray)/count($subArray));
           
        }
    
    
//print_r($eficiencyLineArray);exit;
        $sumArray = array();

        foreach ($eficiency as $k=>$subArray) {
           $sumArray[]= (array_sum($subArray)/count($subArray));
           
        }
        $sumAvgArray = (array_sum($sumArray)/count($sumArray));

        foreach ($sumArray as $k=>$subSumArray) {
           $sumArray2[]= (int)$sumAvgArray;
           
        }

      
    
    $hours=(json_encode($hours));
    $eficiency=json_encode($sumArray);
    $eficiency2=json_encode($sumArray2);
    return view($this->view_path.'dashboard',compact('hours','eficiency','eficiency2','sumAvgArray','eficiencyLineArray','totalProduct','totalMan','totalMachine'));
  }
}