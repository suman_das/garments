<?php

namespace App\Http\Controllers\SuperAdmin\Report;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\User;
use Datatables;
use App\Role;
use App\UserType;
use App\SmsLog;
use Validator;
use Session;
use Redirect;
use Image;
use DB;
use Auth;
use Entrust;
use Excel;


class DetailsReportController extends Controller
{
    private $view_path = "core.superadmin.report.";
    private $route_path = "superadmin.detailsreport.";
    public function report() {
        
      
        return view($this->view_path.'index',compact('territory','amount','model'));
    }
    
    public function dataTableList(Request $request)
    {
        $requestData = $request->all();
        
        $tdate=$requestData['columns'][1]['search']['value'];
        $fdate=$requestData['columns'][0]['search']['value'];
        
        $today=date('Y-m-d H:i:s');
   
        $columns = array(

            0 => 'id'
        );

        
        //$reserves = DB::table('reserves')->selectRaw('*, count(*)')->groupBy('day');
        
        $query = SmsLog::select('*');
        
        $query2 = SmsLog::select('*');
        $parameter = array();

        if (!empty($requestData['columns'][3]['search']['value'])) {   //name
           
           $parameter["msisdn"]=$requestData['columns'][3]['search']['value'];
            $query->where('sms_log.msisdn','=',$requestData['columns'][3]['search']['value']);
            $query2->where('sms_log.msisdn','=',$requestData['columns'][3]['search']['value']);
                    
                    
        }
        
        if (!empty($requestData['columns'][2]['search']['value'])) {
            
            $parameter["traders_code"]=$requestData['columns'][2]['search']['value'];
            $query->where('sms_log.traders_code','=',$requestData['columns'][2]['search']['value']);
            $query2->where('sms_log.traders_code','=',$requestData['columns'][2]['search']['value']);
        }
        if (isset($requestData['columns'][4]['search']['value'])) {   //name
            $parameter["status"]=$requestData['columns'][4]['search']['value'];
            $query->where('sms_log.status','=',$requestData['columns'][4]['search']['value']);
            $query2->where('sms_log.status','=',$requestData['columns'][4]['search']['value']);
        }
      
        
        $parameter["tdate"]=$requestData['columns'][1]['search']['value'];
        $parameter["fdate"]=$requestData['columns'][0]['search']['value'];
        
        if( !empty($requestData['columns'][1]['search']['value']) )
        {
            $tdate=$requestData['columns'][1]['search']['value'];
        }  else {
            $tdate=date('Y-m-d  H:i:s');
        }
        if ((!empty($fdate))&&!empty($tdate)) 
             {
                
               $query->whereBetween('sms_log.created_at',array($fdate,$tdate));
                $query2->whereBetween('sms_log.created_at',array($fdate,$tdate));
             
        }else
        {
            $query->whereDate('sms_log.created_at','<',$today);
            $query2->whereDate('sms_log.created_at','<',$today);
             $fdate='';
        }


        

                
        $requestData['order'][0]['dir'] = 'DESC';
        
        $totalData = count($query2->get()->toarray());
        $totalFiltered = $totalData;
        $query2->orderBy('sms_log.'.$columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir']);
        $query2->offset($requestData['start']);
        $query2->limit($requestData['length']);
        
        
        $query =$query2->get()->toarray();
        //dd($query);
        $data = array();
        $j = 0;
        foreach($query as $row) {
           
            $nestedData = array();
            $nestedData[] = $row["msisdn"];
            $nestedData[]= $row["sms"];
            $nestedData[]= $row["reply"];
            $nestedData[]= $row["traders_code"];
            $nestedData[]= $row["traders_amount"];
            $nestedData[]= $row["customer_amount"];
            $nestedData[]= $row["status"];
            $nestedData[]= $row["created_at"];
            
            $data[] = $nestedData;
        }
    $url= url('superadmin/detailsreport/downloadReport');
        //print_r($data);exit;
        $query = '<tbody class="employee-grid-error"><tr><th colspan="3"><a href="'.$url.'?queryinfo=' .  http_build_query($parameter) . '">Download CSV </a></th></tr></tbody>';
        //$query2 = '<tbody class="employee-grid-error"><tr><th colspan="3"><a href="download_sms_summaryLog.php?queryinfo=' . $queryData . '">Download Summary Report CSV </a></th></tr></tbody>';
        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data,
            "query"=>$query
           
                // total data array
        );

        echo json_encode($json_data);
    }
    
    public function downloadExcel()

	{
            $today=date('Y-m-d H:i:s');
            if (Input::has('tdate'))
            {
               $tdate=Input::get('tdate'); 
            }else
            {
                 $tdate=date('Y-m-d  H:i:s');
            }
            if (Input::has('fdate'))
            {
                $fdate=Input::get('fdate'); 
            }
            
            $query = SmsLog::select('*');
            
            
            if (Input::has('traders_code'))
            {
              $traders_code= Input::get('traders_code');
              $query->where('sms_log.traders_code','=',$traders_code); 
            }
            
            
            
            if (Input::has('msisdn'))
            {   
                $customer_mobile= Input::get('msisdn');
                $query->where('sms_log.msisdn','=',$customer_mobile);  
            }
            
            if (Input::has('status'))
            {   
                $status= Input::get('status');
                $query->where('sms_log.status','=',$status);  
            }
            
            if ((!empty($fdate))&&!empty($tdate)) 
             {
               $query->whereBetween('sms_log.created_at',array($fdate,$tdate));
             
            }else
            {
                $query->whereDate('sms_log.created_at','<',$today);
                
                 $fdate='';
            }
            
            $data = $query->get()->toArray();
            //print_r($data);exit;
            $data2=array();
             foreach($data as $key=>$row) {
           
            $nestedData = array();
            $nestedData["Mobile No"] = $row["msisdn"];
            $nestedData["SMS"]= $row["sms"];
            $nestedData["Reply"]= $row["reply"];;
            $nestedData["Tradrs Code"]= $row["traders_code"];
            $nestedData["Traders Amount"]= $row["traders_amount"];
            $nestedData["Customer Amount"]= $row["customer_amount"];
            $nestedData["Status"]= $row["status"];
            $nestedData["Created AT"]= $row["created_at"];
           
            
            $data2[] = $nestedData;
        }
        //print_r($data2);exit;
            return Excel::create('ReportLog', function($excel) use ($data2) {

                    $excel->sheet('ReportLog', function($sheet) use ($data2)

            {

                            $sheet->fromArray($data2);

            });

            })->download('csv');

	}
}
