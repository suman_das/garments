<?php

namespace App\Http\Controllers\SuperAdmin\Order;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Session;
use App\Item;
use Validator;
use Illuminate\Http\Request;

class ItemController extends Controller
{
      

      public function store(Request $request)
      {
            $validator = Validator::make(request()->all(), [
                  'item_name' => 'required|unique:items,item_name',
            ]);

            if ($validator->fails()) {
                  $e = $validator->errors();
                  
                  return response()->json([
                        'error' => 1,
                        'msg'   => $e->all()
                  ]);
            }


            try{
                  $item             = new Item();
                  $item->item_name = request()->item_name;
                  //$style->status     = 1;
                  $item->save();

                  return response()->json([
                        'error' => 0,
                        'item' => $item,
                        'msg'   => ['Item has been added.']
                  ]);
            }
            catch(Exception $e){
                  return response()->json([
                        'error' => 1,
                        'msg'   => [$e]
                  ]);
            }
      }
}
