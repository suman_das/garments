<?php

namespace App\Http\Controllers\SuperAdmin\Order;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Session;
use App\Style;
use Validator;
use Illuminate\Http\Request;

class StyleController extends Controller
{
      

      public function store(Request $request)
      {
            $validator = Validator::make(request()->all(), [
                  'style_name' => 'required|unique:styles,style_name',
            ]);

            if ($validator->fails()) {
                  $e = $validator->errors();
                  
                  return response()->json([
                        'error' => 1,
                        'msg'   => $e->all()
                  ]);
            }


            try{
                  $style             = new Style();
                  $style->style_name = request()->style_name;
                  //$style->status     = 1;
                  $style->save();

                  return response()->json([
                        'error' => 0,
                        'style' => $style,
                        'msg'   => ['Style has been added.']
                  ]);
            }
            catch(Exception $e){
                  return response()->json([
                        'error' => 1,
                        'msg'   => [$e]
                  ]);
            }
      }
}
