<?php

namespace App\Http\Controllers\SuperAdmin\Order;

use App\Http\Controllers\Controller;

use App\Role;
use Validator;
use Redirect;
use App\User;
use App\Order;
use App\Buyer;
use App\Style;
use App\ItemModel;
use App\Item;
use App\UserType;
use Session;


use Illuminate\Http\Request;

class OrderController extends Controller
{
  private $view_path = "core.superadmin.order.";
  private $route_path = "superadmin.order.";

  public function index(Request $request )
  {
    //$roles = \DB::table('roles')->orderBy('display_name')->get();
    $users = "";
    $old = [ 'unit' => '','start_date' => ''];

    if($request->has('query'))
    {
        $old['unit'] = $request->input('unit');
        $old['date'] = $request->input('date');
        $unit = \Auth::user()->unit->first()->id;
      $manpowers =Order::select('order_info.*', 'item_name', 'model_name', 'style_name', 'buyer_name')
        ->join('buyers', 'buyers.id', '=', 'order_info.buyer_id')
        ->join('models', 'models.id', '=', 'order_info.buyer_id')
        ->join('styles', 'styles.id', '=', 'order_info.buyer_id')
        ->join('items', 'items.id', '=', 'order_info.buyer_id')
        ->where('order_info.unit_id',$unit);
        if ($request->has('unit')) {
         $manpower->where('order_info.unit', $request->input('unit'));
         }
        if ($request->has('start_date')) {
            $manpower->whereDate('order_info.start_date', $request->input('date'));
        }
     //dd($request->has('date'));exit;
        $manpowers = $manpower->paginate(1);
     
    }else{
      $unit = \Auth::user()->unit->first()->id;
      $manpowers =Order::select('order_info.*', 'item_name', 'model_name', 'style_name', 'buyer_name')
    ->join('buyers', 'buyers.id', '=', 'order_info.buyer_id')
    ->join('models', 'models.id', '=', 'order_info.buyer_id')
    ->join('styles', 'styles.id', '=', 'order_info.buyer_id')
    ->join('items', 'items.id', '=', 'order_info.buyer_id')
    ->where('order_info.unit_id',$unit)
    ->paginate(1);
     
    }
    return view($this->view_path.'index', compact('manpowers'));
  }

  public function create()
  {
    $buyer = Buyer::pluck('buyer_name', 'id')->toArray();
    $style = Style::pluck('style_name', 'id')->toArray();
    $model = ItemModel::pluck('model_name', 'id')->toArray();
    $item = Item::pluck('item_name', 'id')->toArray();
    
    return view($this->view_path.'create',compact('buyer','style','model','item'));
  }

  
  public function store(Request $request)
  {
      
        $alldata=$request->all();
        $validation = Validator::make($request->all(), [
                'buyer_id' => 'required',
                'style_id' => 'required',
                'model_id' => 'required',
                'start_date' => 'required',
                'item_id' => 'required',
                
            ]);

        if($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        } 
    
    $unit = \Auth::user()->unit;
    $Order = new Order;
    $Order->fill($request->except('query','_token'));
    $Order->unit_id=$unit;
    $Order->save();
    
    Session::flash('success', 'You have been successfully added new data!');

    return redirect()->route($this->route_path.'index');
  }

  public function show(Request $request, $id)
  {
    $user = User::findOrFail($id);
    return view($this->view_path.'show', compact('user'));
  }

  public function edit(Request $request, $id)
  {
    $manpower = Order::findOrFail($id);
    Session::flash('success', 'You have been successfully updaed new data!');
    return view($this->view_path.'edit', compact('manpower'));
  }

  public function update(Request $request, $id)
  {
    $alldata=$request->all();
    $validation = Validator::make($request->all(), [
                'buyer_name' => 'required',
                'style' => 'required',
                'model' => 'required',
                'start_date' => 'required',
                'unit' => 'required',
                
            ]);

    if($validation->fails()) {
        return Redirect::back()->withErrors($validation)->withInput();
    } 

      
      
    $manpower = Order::findOrFail($id);
    $manpower->fill($request->except('query','_token'));
    $manpower->save();
   Session::flash('success', 'You have been successfully updaed new data!');
    return redirect()->route($this->route_path.'index');
  }
  
 
}
