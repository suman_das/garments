<?php

namespace App\Http\Controllers\SuperAdmin\Order;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Session;
use App\ItemModel;
use Validator;
use Illuminate\Http\Request;

class ModelController extends Controller
{
      

      public function store(Request $request)
      {
            $validator = Validator::make(request()->all(), [
                  'model_name' => 'required|unique:models,model_name',
            ]);

            if ($validator->fails()) {
                  $e = $validator->errors();
                  
                  return response()->json([
                        'error' => 1,
                        'msg'   => $e->all()
                  ]);
            }


            try{
                  $model             = new ItemModel();
                  $model->model_name = request()->model_name;
                  //$style->status     = 1;
                  $model->save();

                  return response()->json([
                        'error' => 0,
                        'model' => $model,
                        'msg'   => ['Model has been added.']
                  ]);
            }
            catch(Exception $e){
                  return response()->json([
                        'error' => 1,
                        'msg'   => [$e]
                  ]);
            }
      }
}
