<?php

namespace App\Http\Controllers\SuperAdmin\Order;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Session;
use App\Buyer;
use Validator;
use Illuminate\Http\Request;

class BuyerController extends Controller
{
     
      public function store(Request $request)
      {
          
            $validator = Validator::make(request()->all(), [
                  'buyer_name' => 'required|unique:buyers,buyer_name',
            ]);

            if ($validator->fails()) {
                  $e = $validator->errors();
                  
                  return response()->json([
                        'error' => 1,
                        'msg'   => $e->all()
                  ]);
            }

            try{
                  $buyer             = new Buyer();
                  $buyer->buyer_name = request()->buyer_name;
                  //$buyer->status     = 1;
                  $buyer->save();

                  return response()->json([
                        'error' => 0,
                        'buyer' => $buyer,
                        'msg'   => ['Buyer has been added.']
                  ]);
            }
            catch(Exception $e){
                  return response()->json([
                        'error' => 1,
                        'msg'   => [$e]
                  ]);
            }
      }
}
