<?php

namespace App\Http\Controllers\SuperAdmin\Registration;

use App\Http\Controllers\Controller;
use App\Model\Participant;
use Illuminate\Http\Request;

class ParticipantController extends Controller
{
  private $view_path  = "core.superadmin.registration.participants.";
  private $route_path = "superadmin.registration.participants.";

  public function index(Request $request)
  {
    $old = [
      'query' => '',

      'audition_zone' => '',
      'name' => '',
      'age' => '',
      'address' => '',

      'mobile' => '',
      'phone_home' => '',
      'occupation' => '',
      'email' => '',

      'nid_pp_no_birth_cert' => '',
      'previously_perticipated' => '',
      'created_at_starting_date' => '',
      'created_at_ending_date' => ''
    ];

    $participants = "";

    if($request->has('query'))
    {
      $old['query'] = $request->input('query');

      $old['audition_zone'] = $request->input('audition_zone');
      $old['name'] = $request->input('name');
      $old['age'] = $request->input('age');
      $old['address'] = $request->input('address');

      $old['mobile'] = $request->input('mobile');
      $old['phone_home'] = $request->input('phone_home');
      $old['occupation'] = $request->input('occupation');
      $old['email'] = $request->input('email');

      $old['nid_pp_no_birth_cert'] = $request->input('nid_pp_no_birth_cert');
      $old['previously_perticipated'] = $request->input('previously_perticipated');
      $old['created_at_starting_date'] = $request->input('created_at_starting_date');
      $old['created_at_ending_date'] = $request->input('created_at_ending_date');


      $participants = Participant::where(function($query) use($request){

        if($request->has('audition_zone') && $request->audition_zone != "")
        {
          $query->where('audition_zone', $request->audition_zone);
        }

        if($request->has('name') && $request->name != "")
        {
          $query->where('name', 'like', '%'.$request->name.'%');
        }

        if($request->has('age') && $request->age != "")
        {
          $query->where('age', $request->age);
        }

        if($request->has('address') && $request->address != "")
        {
          $query->where('address', 'like', '%'.$request->address.'%');
        }


        if($request->has('mobile') && $request->mobile != "")
        {
          $query->where('mobile', 'like', '%'.$request->mobile.'%');
        }

        if($request->has('phone_home') && $request->phone_home != "")
        {
          $query->where('phone_home', 'like', '%'.$request->phone_home.'%');
        }

        if($request->has('occupation') && $request->occupation != "")
        {
          $query->where('occupation', 'like', '%'.$request->occupation.'%');
        }

        if($request->has('email') && $request->email != "")
        {
          $query->where('email', $request->email);
        }

        if($request->has('nid_pp_no_birth_cert') && $request->nid_pp_no_birth_cert != "")
        {
          $query->where('nid_pp_no_birth_cert', $request->nid_pp_no_birth_cert);
        }

        if($request->has('previously_perticipated') && $request->previously_perticipated != "")
        {
          $query->where('previously_perticipated', $request->previously_perticipated);
        }

        if($request->has('created_at_starting_date') && $request->created_at_starting_date != "")
        {
          $query->where('created_at', '>=', $request->created_at_starting_date);
        }

        if($request->has('created_at_ending_date') && $request->created_at_ending_date != "")
        {
          $query->where('created_at', '<=', $request->created_at_ending_date);
        }

      })->paginate(15);

    }else{
      $participants = Participant::orderBy('created_at')->paginate(15);
    }

    return view($this->view_path.'index', compact('participants', 'old'));
  }

  public function show(Request $request, $id)
  {
    $participant = Participant::where('id', $id)->get();

    if(!count($participant))
    {
      return redirect($this->route_path.'index');
    }

    $participant = $participant->first();
    return view($this->view_path.'show', compact('participant'));
  }

  public function download(Request $request)
  {

      $participants = "";

      if($request->has('query'))
      {
        $participants = Participant::where(function($query) use($request){

          if($request->has('audition_zone') && $request->audition_zone != "")
          {
            $query->where('audition_zone', $request->audition_zone);
          }

          if($request->has('name') && $request->name != "")
          {
            $query->where('name', 'like', '%'.$request->name.'%');
          }

          if($request->has('age') && $request->age != "")
          {
            $query->where('age', $request->age);
          }

          if($request->has('address') && $request->address != "")
          {
            $query->where('address', 'like', '%'.$request->address.'%');
          }


          if($request->has('mobile') && $request->mobile != "")
          {
            $query->where('mobile', 'like', '%'.$request->mobile.'%');
          }

          if($request->has('phone_home') && $request->phone_home != "")
          {
            $query->where('phone_home', 'like', '%'.$request->phone_home.'%');
          }

          if($request->has('occupation') && $request->occupation != "")
          {
            $query->where('occupation', 'like', '%'.$request->occupation.'%');
          }

          if($request->has('email') && $request->email != "")
          {
            $query->where('email', $request->email);
          }

          if($request->has('nid_pp_no_birth_cert') && $request->nid_pp_no_birth_cert != "")
          {
            $query->where('nid_pp_no_birth_cert', $request->nid_pp_no_birth_cert);
          }

          if($request->has('previously_perticipated') && $request->previously_perticipated != "")
          {
            $query->where('previously_perticipated', $request->previously_perticipated);
          }

          if($request->has('created_at_starting_date') && $request->created_at_starting_date != "")
          {
            $query->where('created_at', '>=', $request->created_at_starting_date);
          }

          if($request->has('created_at_ending_date') && $request->created_at_ending_date != "")
          {
            $query->where('created_at', '<=', $request->created_at_ending_date);
          }
        })->orderBy('created_at')->get();
      }else{
        $participants = Participant::orderBy('created_at')->get();
      }

      $fileName = 'Report'.time().'.csv';
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$fileName}");
        header("Expires: 0");
        header("Pragma: public");
      $fh = @fopen( 'php://output', 'w' );
      if(count($participants))
      {
        $headerRow = [
          'Audition Zone', 'Name', 'Age', 'Date of Birth',
          'Address', 'Mobile', 'Home Phone', 'Occupation',
          'Email', 'NID Passport Birth Certificate', 'Previously Perticipated', 'Year Of Participantion',
          'Registered At'];

        fputcsv($fh, $headerRow);

        foreach($participants as $p)
        {
           fputcsv($fh,[
              $p->audition_zone,
              $p->name,
              $p->age,
              date('Y-m-d', strtotime($p->dob)),

              $p->address,
              $p->mobile,
              $p->phone_home,
              $p->occupation,

              $p->email,
              $p->nid_pp_no_birth_cert,
              $p->previously_perticipated,
              $p->year_of_participation,

              $p->created_at
            ]);
        }
      }

      fclose($fh);
      exit;
    return redirect($this->route_path.'index');
  }
}