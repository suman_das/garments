<?php

namespace App\Http\Controllers\SuperAdmin\UserManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StatusController extends Controller
{
  private $view_path = "core.superadmin.user-management.";
  private $route_path = "superadmin.user-management.";

  public function change(Request $request, $id)
  {
    $user = User::findOrFail($id);

    if($user->status == 1)
    {
      $user->status = 0 ;
    }else{
      $user->status = 1 ;
    }

    $user->updated_at = date('Y-m-d H:i:s');
    $user->updated_by = \Auth::user()->id;
    $user->update();

    return redirect()->route($this->route_path.'index');
  }
}
