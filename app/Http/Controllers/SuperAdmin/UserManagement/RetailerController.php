<?php

namespace App\Http\Controllers\SuperAdmin\UserManagement;

use App\Http\Controllers\Controller;

use App\Role;
use Validator;
use Redirect;
use App\User;
use App\Traders;
use App\UserType;
use Illuminate\Http\Request;

class RetailerController extends Controller
{
  private $view_path = "core.superadmin.retailer-management.";
  private $route_path = "superadmin.retailer-management.";

  public function index(Request $request )
  {
    $roles = \DB::table('roles')->orderBy('display_name')->get();
    $users = "";
    $old = [ 'name' => ''];

    if($request->has('query'))
    {
      $old['query'] = $request->input('query');
      $old['name'] = $request->input('name');

      $users = User::where(function($query) use($request){

        if($request->has('role_id') && $request->role_id != ""){
          $role_id = $request->role_id;
          $query->whereHas('roles', function($query2) use($role_id) {
            $query2->where('roles.id', $role_id);
          });
        }

        if($request->has('name') && $request->name != ""){
          $query->where('name', 'LIKE', '%'.$request->name.'%');
        }

      })->paginate(15);

    }else{
      $users = Traders::paginate(15);;
     
    }
    return view($this->view_path.'index', compact('users', 'roles'));
  }

  public function create()
  {
    $roles = \DB::table('roles')->pluck('name','id')->toArray();
    //$divisions = \DB::table('bd_info')->pluck('division','division')->toArray();
    $distributors = User::whereHas('roles' , function($q){
        $q->where('name', 'owner');
    })->get()->pluck('email','id')->toArray();
    
    return view($this->view_path.'create', compact('distributors','divisions'));
  }

  
  public function store(Request $request)
  {
      
        $alldata=$request->all();
        
        $validation = Validator::make($request->all(), [
                'outlet_code' => 'required',
                'outlet_name' => 'required',
                'address' => 'required',
                'outlet_contact' => 'required',
                'channel_name' => 'required',
                'route' => 'required',
                
            ]);

        if($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        } 
    
    $user = User::findOrFail($request->dealer_id);
    
   
   // dd($dealer_code);
    $traders = new Traders;
    $traders->fill($request->except('roles','query','_token','dealer_id'));
    $traders->user_id = $request->dealer_id;
    $traders->ppo = $user->name;
    $traders->ppo_mobile = $user->email;
    $traders->save();
    
    

    return redirect()->route($this->route_path.'index');
  }

  public function show(Request $request, $id)
  {
    $user = User::findOrFail($id);
    return view($this->view_path.'show', compact('user'));
  }

  public function edit(Request $request, $id)
  {
    $user = User::findOrFail($id);
    $cdivision=$user->division;
    $cdistrict=$user->district;
    $dealer_id=$user->dealer_id;
   
    $distributors = User::with(['roles' => function($q){
        $q->where('name', 'distributoradmin');
    }])->get()->pluck('full_name','id')->toArray();
    
   $divisions = \DB::table('bd_info')->pluck('division','division')->toArray();
   $districts = \DB::table('bd_info')->where('division','=',$divisions)->pluck('district','district')->toArray();
   $roles = \DB::table('roles')->pluck('name','id');
    return view($this->view_path.'edit', compact('roles','user','divisions','districts','cdivision','cdistrict','distributors'));
  }

  public function update(Request $request, $id)
  {
    $alldata=$request->all();
    $validation = Validator::make($request->all(), [
              'name' => 'required',
              'shop_name' => 'required',
              'designation' => 'required',
              'address' => 'required',
              'contact_no' => 'required',
              'dealer_id' => 'required',

          ]);

      if($validation->fails()) {
          return Redirect::back()->withErrors($validation)->withInput();
      }  
      
    $user              = User::findOrFail($id);
    $user->fill($request->except('roles','query','_token','remember_token'));
    $roles = $user->roles->first()->toArray();
        
    $rid=3;
    
    $user->password = bcrypt($request->password);
    $user->created_by = auth()->user()->id;
    $user->updated_by = auth()->user()->id;
  
    $user->save();
    $user->roles()->detach($rid);
    $user->attachRole(3);
    return redirect()->route($this->route_path.'index');
  }
  
  public function districtlist(Request $request)
    {
        $requestData = $request->all();
        $division=$requestData['division'];
        return $district = $districts = \DB::table('bd_info')->where('division','=',$division)
                ->pluck('district','district')->toArray();;
    }
    
    public function userDetails(Request $request)
    {
        $requestData = $request->all();
         return $user              = User::findOrFail($requestData['distributor'])->toArray();
        
    }
}
