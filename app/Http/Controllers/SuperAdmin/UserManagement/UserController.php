<?php
namespace App\Http\Controllers\SuperAdmin\UserManagement;

use App\Http\Controllers\Controller;

use App\Role;
use Validator;
use Redirect;
use App\User;
use App\UserType;
use Illuminate\Http\Request;

class UserController extends Controller
{
  private $view_path = "core.superadmin.user-management.";
  private $route_path = "superadmin.user-management.";

  public function index(Request $request )
  {
    $roles = \DB::table('roles')->orderBy('display_name')->get();
    $users = "";
    $old = [ 'name' => ''];

    if($request->has('query'))
    {
      $old['query'] = $request->input('query');
      $old['name'] = $request->input('name');

      $users = User::where(function($query) use($request){

        if($request->has('role_id') && $request->role_id != ""){
          $role_id = $request->role_id;
          $query->whereHas('roles', function($query2) use($role_id) {
            $query2->where('roles.id', $role_id);
          });
        }

        if($request->has('name') && $request->name != ""){
          $query->where('name', 'LIKE', '%'.$request->name.'%');
        }

      })->paginate(15);

    }else{
      $users = User::with('roles')->orderBy('id','DESC')->paginate(1);
    }
    //print_r($users->toArray());exit;
    return view($this->view_path.'index', compact('users', 'roles'));
  }

  public function create()
  {
    $roles = \DB::table('roles')->pluck('name','id')->toArray();
    //$divisions = \DB::table('bd_info')->pluck('division','division')->toArray();
    //$districts = \DB::table('bd_info')->pluck('district','district')->toArray();
    return view($this->view_path.'create', compact('roles','divisions'));
  }

  public function store(Request $request)
  {
      
    $alldata=$request->all();
    $validation = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|between:5,15',
                'msisdn' => 'required|between:10,25',
                'roles' => 'required',
                
            ]);

    if($validation->fails()) {
        return Redirect::back()->withErrors($validation)->withInput();
    } 
    $user = new User;
    $user->fill($request->except('roles','password','query','_token'));
    $user->password = bcrypt($request->password);
    $user->remember_token = str_random(60);
    $user->save();
    
    $user->attachRole($request->input('roles'));

    return redirect()->route($this->route_path.'index');
  }

  public function show(Request $request, $id)
  {
    $user = User::findOrFail($id);
    return view($this->view_path.'view', compact('user'));
  }

  public function edit(Request $request, $id)
  {
    $user = User::findOrFail($id);
    $roles = \DB::table('roles')->pluck('name','id');
    $role_type = Role::where('id', '=', $user->user_type_id)->pluck('id');
    return view($this->view_path.'edit', compact('roles','user','role_type'));
  }

  public function update(Request $request, $id)
  {
    $alldata=$request->all();
        
        $validation = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|between:5,15',
                'msisdn' => 'required|between:10,25',
                'roles' => 'required',
                
            ]);

        if($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        } 
      
    $user              = User::findOrFail($id);
    $user->fill($request->except('roles','query','_token','remember_token'));
   // $roles = $user->roles->first()->toArray();
        
    //$rid=$roles["id"];
    
    //$user->password = bcrypt($request->password);
    $user->created_by = auth()->user()->id;
    $user->updated_by = auth()->user()->id;
    
    $roles = $user->roles->first()->toArray();
        //print_r($roles);exit;
        $rid=$roles["id"];
  
    $user->save();
    $user->roles()->detach($rid);
    $user->attachRole($request->input('roles'));
    return redirect()->route($this->route_path.'index');
  }
  
  public function districtlist(Request $request)
    {
        $requestData = $request->all();
        $division=$requestData['division'];
        return $district = $districts = \DB::table('bd_info')->where('division','=',$division)
                ->pluck('district','district')->toArray();;
    }
}
