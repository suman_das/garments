<?php

namespace App\Http\Controllers\SuperAdmin\UserManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Session;

class PasswordController extends Controller
{
  private $view_path = "core.superadmin.user-management.password.";
  private $route_path = "superadmin.user-management.";

  public function get($id)
  {
    $user = User::findOrFail($id);
    return view($this->view_path.'show', compact('user'));
  }

  public function change(Request $request, $id)
  {
    $user = User::findOrFail($id);

    if( (
        $request->password == "" ||
        $request->password != $request->confirm_password) )
    {
     
      return redirect()->route($this->route_path.'password-change', $user->id)->withErrors(['error', 'Password is not match']);
    }

    $user->password = \Hash::make($request->password) ;
    $user->updated_at = date('Y-m-d H:i:s');
    $user->updated_by = \Auth::user()->id;
    $user->update();
     Session::flash('message', "Password Changes Successfully");
    return redirect()->route($this->route_path.'index');
  }
}
