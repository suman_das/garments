<?php

namespace App\Http\Controllers\SuperAdmin\Target;

use App\Http\Controllers\Controller;
use App\Role;
use Validator;
use Redirect;
use App\User;
use App\Target;
use App\Order;
use App\UserType;
use Session;
use DB;
use App\DailyTargetSummary;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TargetController extends Controller {

    private $view_path = "core.superadmin.target.";
    private $route_path = "superadmin.target.";
    private $setslotTarget = 0;

    public function index(Request $request) {
        //$roles = \DB::table('roles')->orderBy('display_name')->get();
        $users = "";
        $old = ['unit' => '', 'start_date' => ''];

        if ($request->has('query')) {
            $old['unit'] = $request->input('unit');
            $old['date'] = $request->input('date');
            $manpower = MachineData::select('*');
            if ($request->has('unit')) {
                $manpower->where('unit', $request->input('unit'));
            }
            if ($request->has('date')) {
                $manpower->whereDate('date', $request->input('date'));
            }
            //dd($request->has('date'));exit;
            $manpowers = $manpower->paginate(1);
        } else {
            $manpowers = MachineData::paginate(1);
            ;
        }
        return view($this->view_path . 'index', compact('manpowers'));
    }

    public function create() {
       // $buyer = Order::select(DB::raw("CONCAT(unit_id,buyer_name,style,model,item) AS display_name"), 'id')->pluck('display_name', 'id')->toArray();
         $unit = \Auth::user()->unit->first()->id;
      $buyer =Order::select(DB::raw("CONCAT(buyer_name,style_name,model_name,item_name) AS display_name"), 'order_info.id')
    ->join('buyers', 'buyers.id', '=', 'order_info.buyer_id')
    ->join('models', 'models.id', '=', 'order_info.buyer_id')
    ->join('styles', 'styles.id', '=', 'order_info.buyer_id')
    ->join('items', 'items.id', '=', 'order_info.buyer_id')
    ->where('order_info.unit_id',$unit)->pluck('display_name', 'id')->toArray();
// dump output to see how it looks.
        //$buyer = Order::pluck('id', 'id')->toArray();
       $totalTarget = Target::select('daily_targets.*','buyer_name','style_name','model_name','item_name')->where('status', 1)
                                ->join('order_info', 'daily_targets.order_id', '=', 'order_info.id')
                                ->join('buyers', 'buyers.id', '=', 'order_info.buyer_id')
                                ->join('models', 'models.id', '=', 'order_info.buyer_id')
                                ->join('styles', 'styles.id', '=', 'order_info.buyer_id')
                                ->join('items', 'items.id', '=', 'order_info.buyer_id')
                                ->whereDate('daily_targets.created_at', date('Y-m-d'))
                                //->with('buyer', 'style')
                                ->orderBy('line_no')
                                ->orderBy('daily_targets.created_at', 'desc')
                                ->get();
        return view($this->view_path . 'create', compact('buyer','totalTarget'));
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'date' => 'required',
                    'line_no' => 'required',
                    'order_id' => 'required',
                    'todays_target' => 'required',
                    'num_of_hours' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                            ->withErrors($validator)
                            ->withInput();
        }

        try {
            # validation step 2
            $isData = Target::whereDate('created_at', request()->date)
                    ->where('line_no', request()->line_no)
                    ->where('order_id', request()->order_id)
                    ->where('status', 1)
                    ->first();

            if (count($isData)) {
                Session::flash('errorMsg', 'Style is already exixts on line no ' . request()->line_no);
                return back();
            }

            # validation step 3
            $setTargetTime = Carbon::createFromFormat('Y-m-d', request()->date)->format('Y-m-d H:i:s');
            $setTargetTime = strtotime($setTargetTime);
            $onlyEnableTime = strtotime(date('Y-m-d') . ' 08:00:00');
            /* if( $setTargetTime > $onlyEnableTime ){
              Session::flash('errorMsg', 'You are not able to set target after working hour has started.');
              return back();
              } */



            $hours = request()->num_of_hours;
            asort($hours);

            // dd($hours);

            $slotHourData = [];
            $slot = request()->todays_target ? floor(request()->todays_target / count($hours)) : 0;
            $mod = request()->todays_target ? floor(request()->todays_target % count($hours)) : 0;

            foreach ($hours as $key => $hour) {
                $slotHourData[$key]['hour'] = $hour;
                $slotHourData[$key]['initial_target'] = $slot;
            }

            $target_sum = array_sum(
                    array_column($slotHourData, 'initial_target')
            );

            # Escape 0
            if ($mod > 0) {
                $last_key_target_should_be = ($slot) + $mod;
                $slotHourData[0]['initial_target'] = $last_key_target_should_be;
            } 
            //print_r($slotHourData);exit;
            
            # save target                  
            DB::beginTransaction();

            $dt = new Target();
            $dt->fill(request()->except(['_token', 'date', 'num_of_hours']));
            $dt->created_at = Carbon::createFromFormat('Y-m-d', request()->date);
            $dt->num_of_hours = implode(',', $hours);
            $dt->status = 1;
            $dt->save();
            $last_id=$dt->id;


            # save target summary
            $attr = [];
            $tsummary = 0;
            $hourSlot = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
            
            foreach( $hourSlot as $dh )
                  {
                        # If hour exists than set slot target
                        $this->isLineHourExists($dh, $slotHourData);

                        $attr[] = [
                              'l_date'            => request()->date,
                              'line_no'           => request()->line_no,
                              'order_id'          => request()->order_id,
                              'target_id'         => $last_id,
                              'unit_setting_id'   => request()->buyer_id,
                              'hour_no'           => $dh,
                              'initial_target'    => $this->setslotTarget,
                              'target'            => $this->setslotTarget,
                              'output'            => 0,
                              'line_def'          => 0,
                              'output_percentage' => 0,
                              'created_at'        => new \DateTime(),
                              'updated_at'        => new \DateTime()
                        ];

                        $this->setslotTarget = 0;
                  }


            $tsummary  = DailyTargetSummary::insert($attr);
            Session::flash('successMsg', 'Target has been successfully added !');
            DB::commit();
            return back();
        } catch (Exception $e) {
            Session::flash('errorMsg', $e);
            DB::rollback();
            return back();
        }
    }
    
      private function isLineHourExists($needle, $haystack, $strict = false)
      {
            foreach ($haystack as $item) {
                  if (($strict ? $item['hour'] === $needle : $item['hour'] == $needle) || (is_array($item['hour']) && in_array_r($needle, $item['hour'], $strict))) {
                        $this->setslotTarget = $item['initial_target']; 
                        return true;
                  }
            }
            return false;
      }

    public function show(Request $request, $id) {
        $user = User::findOrFail($id);
        return view($this->view_path . 'show', compact('user'));
    }

     public function edit($id)
      {
            $time    = Carbon::now()->addHour()->format('H');
            $target  = Target::findOrFail($id);
            $summary = DailyTargetSummary::where('l_date', date('Y-m-d'))
                              ->where('line_no', $target->line_no)
                              //->where('style_no', $target->style_id)
                              //->where('buyer_no', $target->buyer_id)
                              ->where('hour_no', $time)
                              ->first();

            return view($this->view_path . 'edit', compact('target', 'summary', 'time'));
      }

      /**
       * Add target hour form
       */
      public function addHour($id)
      {
            $time    = Carbon::now()->addHour()->format('H');
            $target  = Target::findOrFail($id);
            $summary = DailyTargetSummary::where('l_date', date('Y-m-d'))
                              ->where('target_id', $target->id)
                              ->get();
            
            $hours   = DailyTargetSummary::where('l_date', date('Y-m-d'))
                              ->where('target_id', $target->id)
                              ->where('hour_no', '>=', $time)
                              ->where('initial_target', 0)
                              ->pluck('hour_no', 'hour_no')
                              ->toArray();

            return view($this->view_path . 'add-hour', compact('target', 'summary', 'time', 'hours'));
      }

      /**
       * Remove target hour form
       */
      public function removeHour($id)
      {
            $time    = Carbon::now()->addHour()->format('H');
            $target  = Target::findOrFail($id);
            $summary = DailyTargetSummary::where('l_date', date('Y-m-d'))
                              ->where('target_id', $target->id)
                              ->get();
            
            $hours   = DailyTargetSummary::where('l_date', date('Y-m-d'))
                              ->where('target_id', $target->id)
                              ->where('hour_no', '>=', $time)
                              ->where('initial_target', '!=', 0)
                              ->pluck('hour_no', 'hour_no')
                              ->toArray();

            # Escape last hour
            array_pop($hours);

            return view($this->view_path .'remove-hour', compact('target', 'summary', 'time', 'hours'));
      }      

      /**
       * Update target
       */
      public function update($id, Request $request)
      {
            $validator = Validator::make($request->all(), [
                  'updated_target'  => 'required',
            ]);

            if ($validator->fails()) {
                  return back()
                        ->withErrors($validator)
                        ->withInput();
            }

            $time    = Carbon::now()->addHour()->format('H');
            $target  = Target::findOrFail($id);

            if( ($request->updated_target < 0) ){
                  Session::flash('errorMsg', 'Invalid target !!!');
                  return back();
            }

            $summary = DailyTargetSummary::where('l_date', date('Y-m-d'))
                              ->where('line_no', $target->line_no)
                              ->where('style_no', $target->style_id)
                              ->where('buyer_no', $target->buyer_id)
                              ->where('hour_no', $time)
                              ->first();

            try{
                  DB::beginTransaction();

                  $updatedTarget = $request->updated_target;

                  if( $updatedTarget > $summary->target ){
                        $needToBeAdded = $updatedTarget - $summary->target;

                        # update total target
                        $target->todays_target = $target->todays_target + $needToBeAdded;
                        $target->save();

                        # update target summary
                        $summary->initial_target      = $summary->initial_target + $needToBeAdded;
                        $summary->target              = $summary->target + $needToBeAdded;
                        $summary->hour_planned_status = 'U';
                        $summary->save();
                  }
                  elseif( $updatedTarget < $summary->target ){
                        $needToBeSub = $summary->target - $updatedTarget;

                        # update total target
                        $target->todays_target = $target->todays_target - $needToBeSub;
                        $target->save();

                        # update target summary
                        $summary->initial_target      = $summary->initial_target - $needToBeSub;
                        $summary->target              = $summary->target - $needToBeSub;
                        $summary->hour_planned_status = 'U';
                        $summary->save();
                  }

                  DB::commit();
                  Session::flash('successMsg', 'Upcomming target has been successfully updated!');
                  return back();
            }
            catch( Exception $e ){
                  DB::rollback();
                  Session::flash('errorMsg', $e);
                  return back();
            }
            
            
      }

      /**
       * Update additional hour & target
       */
      public function updateHour($id, Request $request)
      {
            $validator = Validator::make($request->all(), [
                  'additional_hour'   => 'required',
                  'additional_target' => 'required',
            ]);

            if ($validator->fails()) {
                  return back()
                        ->withErrors($validator)
                        ->withInput();
            }

            $time    = Carbon::now()->addHour()->format('H');
            $target  = Target::findOrFail($id);

            if( ($request->additional_target <= 0) ){
                  Session::flash('errorMsg', 'Invalid target !!!');
                  return back();
            }

            $summary = DailyTargetSummary::where('l_date', date('Y-m-d'))
                              ->where('target_id', $target->id)
                              ->where('initial_target', 0)
                              ->where('hour_no', '>=', $time)
                              ->where('hour_no', '=', $request->additional_hour)
                              ->firstOrFail();

            try{
                  DB::beginTransaction();

                  $targetNumOfHours = explode(',', $target->num_of_hours);

                  array_push($targetNumOfHours, $request->additional_hour);
                  sort($targetNumOfHours);

                  # update daily target table
                  $target->num_of_hours  = implode(',', $targetNumOfHours);
                  $target->todays_target = ($target->todays_target + $request->additional_target);
                  $target->save();

                  # update daily target summary
                  $summary->hour_planned_status = 'U';
                  $summary->initial_target      = $request->additional_target;
                  $summary->target              = $request->additional_target;
                  $summary->save();

                  DB::commit();
                  Session::flash('successMsg', 'Additional target has been successfully updated!');
                  return back();
            }
            catch( Exception $e ){
                  DB::rollback();
                  Session::flash('errorMsg', $e);
                  return back();
            }
            
            
      }

      /**
       * remove additional hour
       */
      public function removeSingleHour($id, Request $request)
      {
            $validator = Validator::make($request->all(), [
                  'additional_hour'   => 'required'
            ]);

            if ($validator->fails()) {
                  return back()
                        ->withErrors($validator)
                        ->withInput();
            }

            $time    = Carbon::now()->addHour()->format('H');
            $target  = Target::findOrFail($id);

            $summary = DailyTargetSummary::where('l_date', date('Y-m-d'))
                              ->where('target_id', $target->id)
                              ->where('initial_target', '!=', 0)
                              ->where('hour_no', '>=', $time)
                              ->where('hour_no', '=', $request->additional_hour)
                              ->firstOrFail();

            try{
                  DB::beginTransaction();

                  $targetNumOfHours = explode(',', $target->num_of_hours);

                  $afterRemoved = array_filter($targetNumOfHours, function($num) use ($request){
                        return $num != $request->additional_hour;
                  });

                  sort($afterRemoved);

                  # update daily target table
                  $target->num_of_hours  = implode(',', $afterRemoved);
                  $target->save();

                  # update this hour target to next available hour
                  $toBeUpdate = DailyTargetSummary::where('l_date', date('Y-m-d'))
                                    ->where('target_id', $target->id)
                                    ->where('initial_target', '!=', 0)
                                    ->where('hour_no', '>=', $time)
                                    ->get();

                  if( $toBeUpdate ){
                        $newTotalTargetOfThoseNextHours = $toBeUpdate->sum('target');

                        $newTarget                 = [];
                        $slot                      = floor($newTotalTargetOfThoseNextHours / (count($toBeUpdate)-1) );
                        $last_key_target_should_be = ($slot) + ($newTotalTargetOfThoseNextHours - ($slot * (count($toBeUpdate)-1) ));

                        foreach( $toBeUpdate as $tu ){
                              $tu->target = $slot;
                              $tu->save();
                        }

                        # Merge
                        $lastObjectOfToBeUpdate = $toBeUpdate->last();
                        $lastObjectOfToBeUpdate->target = $slot;
                        $lastObjectOfToBeUpdate->save();
                  }
                  else{
                        DB::rollback();
                        Session::flash('errorMsg', 'Next available hour target not found.');
                        return back();
                  }


                  # update selected hour's daily target summary
                  $summary->hour_planned_status = 'C';
                  $summary->initial_target      = 0;
                  $summary->target              = 0;
                  $summary->save();

                  DB::commit();
                  Session::flash('successMsg', 'Changed hour has been successfully updated!');
                  return back();
            }
            catch( Exception $e ){
                  DB::rollback();
                  Session::flash('errorMsg', $e);
                  return back();
            }
            
            
      }


}
