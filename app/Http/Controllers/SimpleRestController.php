<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;
use Validator;


abstract class SimpleRestController extends Controller
{
    private $dt;
    private $resourceBase;

    public function __construct()
    {
        $resourceBase=Route::currentRouteName();
    }

    protected static abstract function simpleRestConfig();

    public function getSimpleRestConfig(){
        $defaultArray=array(
            'theme'=>'garments',
            'model'=>'',
            'title'=>'',
            'links'=>array(
            ),
            'query'=>'',
            "fields"=>array(
            )
        );
        
        $configArray=static::simpleRestConfig();
        // foreach($configArray['fields'] as $k=>$v){
        //     $fieldsArrayItem=array();
        //     $fieldsArrayItem['fieldId']=''; 
        //     $fieldsArrayItem['data']=$configArray['fields'][$k]['fieldId']; 
        //     $fieldsArrayItem['label']=ucwords(str_replace('_',' ',substr($fieldsArrayItem['data'],strrpos($fieldsArrayItem['data'],'.')+1) ));
        //     $fieldsArrayItem['fieldType']='text';
        //     $fieldsArrayItem['validation']="";
        //     $configArray['fields'][$k] = array_merge( $fieldsArrayItem, $configArray['fields'][$k] );
        // }
        foreach($configArray['fields'] as $k=>$v){
            // creating default $fieldsArrayItem
            $fieldsArrayItem=array();
            $fieldsArrayItem['fieldId']=''; 
            $fieldsArrayItem['data']=$v[0]; 
            $fieldsArrayItem['label']=ucwords(str_replace('_',' ',substr($fieldsArrayItem['data'],strrpos($fieldsArrayItem['data'],'.')+1) ));
            $fieldsArrayItem['fieldType']='text';
            $fieldsArrayItem['validation']="";
            // Formatting $configArray
            $fieldsArrayItem=array();
            $fieldsArrayItem['fieldId']=@$v[0]; 
            $fieldsArrayItem['data']=$v[1]; 
            $fieldsArrayItem['label']=$v[2];
            $fieldsArrayItem['fieldType']=$v[3];
            $fieldsArrayItem['validation']=@$v[4];
            // merging
            $configArray['fields'][$k] = array_merge( $fieldsArrayItem, $configArray['fields'][$k] );
        }
        return array_merge( $defaultArray, $configArray );
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $dt=$this->getSimpleRestConfig();
        $resourceBase=explode('.',Route::currentRouteName())[0];

        // Getting option data for select
        $selectArray=array();
        foreach($dt['fields'] as $currentField){
            if($currentField['fieldType']=="select" || $currentField['fieldType']=="radio" || $currentField['fieldType']=="checkbox"){
                $selectTable=explode('.',$currentField['data'])[0];
                if($selectTable!==""){ // Need to mention selectTable for relational column
                    $selectArray[ $currentField['fieldId'] ]=DB::table( $selectTable )
                        ->select('id',$currentField['data'].' as optionText')
                        ->orderBy( $currentField['data'] )
                        ->get()
                        ->toArray();
                }
                else{
                    die('data should be in relationalTble.field format for '.$currentField['fieldId'].'');
                }
            }
        }

        return view('simpleRest.'.$dt['theme'].'.index',compact('dt','resourceBase','selectArray'));
    }


    public function datatableJsonData(){
        $dt=$this->getSimpleRestConfig();
        $resourceBase= explode('/',Route::getFacadeRoot()->current()->uri())[1];

        // Creating Where clause Array, Applying where clause and get Datatable JSON data 
        $whereArray=array();
        foreach($dt['fields'] as $f){
            if( isset( $_POST[ $f['fieldId'] ] ) && $_POST[ $f['fieldId'] ]!=="" ){
                $whereArray[ $f['fieldId'] ] = $_POST[ $f['fieldId'] ];
            }
        }//print_r($whereArray);
        
        // Applying where clause and get Datatable JSON data 
        // dd($dt['query']);
        $dt['query'] = $dt['query']->where($whereArray);    //dd($dt['query']->toSql());
        $dtJson=Datatables::of( $dt['query'] );

        // Finding the action column
        $actionColumn=array();
        foreach($dt['fields'] as $currentField){
            if($currentField['fieldId']=="action"){
                $actionColumn=$currentField;
            }
        }
        $actionColumnButtonsArray=explode('|',$actionColumn[4]);
        $actionColumnButtons=array();
        foreach($actionColumnButtonsArray as $item){
            $actionColumnButtons[$item]=true;
        }

        // Attahcing action buttons to action column
        if(isset($actionColumnButtons['disable']) ){
            $dtJson= $dtJson->addColumn('action', function ($dtJson) use($actionColumnButtons,$dt,$resourceBase) {
                $links="";
                if( isset($actionColumnButtons['links']) ){
                    foreach($actionColumnButtons['links'] as $link){
                        $link[1]=str_replace('{id}',$dtJson->id,$link[1]);
                        $links.='<a class="btn btn-xs btn-primary" href="'.$link[1].'"><i class="'.$link[2].'"></i> '.$link[0].'</a>';
                    }
                }
                return ''
                .($dtJson->isActive=="active"?
                    '<form method="post" action="'.url($resourceBase.'/'.$dtJson->id).'">':'<form method="post" action="'.url($resourceBase.'/'.$dtJson->id.'/restore').'">')
                    .'<input type="hidden" name="_token" value="'.csrf_token().'" />'         
                .($dtJson->isActive=="active"?
                    ( (isset($actionColumnButtons['show']) ) ? '<a href="'.$resourceBase.'/'.$dtJson->id.'" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> Show</a>': '' )
                    .( (isset($actionColumnButtons['edit']) ) ? '<a href="'.$resourceBase.'/'.$dtJson->id.'/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i> Edit</a>': '' )
                    .'<input name="_method" type="hidden" value="DELETE">
                    <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm(\'Are You Sure You Want To Disable This '.$dt['title'].'?\')"><i class="glyphicon glyphicon-trash"></i> Disable</button>'
                :'<input name="_method" type="hidden" value="PATCH">
                    <button type="submit" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-repeat"></i> Restore</button>
                ').
                $links
                .'</form>';
            });
        }
        else{
            $dtJson= $dtJson->addColumn('action', function ($dtJson) use(&$actionColumnButtons) {
                return ' &nbsp; '
                .( (isset($actionColumnButtons['show']) ) ? '<a href="'.$resourceBase.'/'.$dtJson->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-show"></i> Show</a>': '' ).' &nbsp; '
                .( (isset($actionColumnButtons['edit']) ) ? '<a href="'.$resourceBase.'/'.$dtJson->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-show"></i> Edit</a>': '' )
                .' &nbsp; ';
            });

        }
        $dtJson= $dtJson->toJson();
        return $dtJson;
    }


    // /**
    //  * Show the form for creating a new resource.
    //  */
    public function create()
    {
        $dt=$this->getSimpleRestConfig();
        $resourceBase=explode('.',Route::currentRouteName())[0];

        // Getting option data for select
        $selectArray=array();
        foreach($dt['fields'] as $currentField){
            if($currentField['fieldType']=="select" || $currentField['fieldType']=="radio"|| $currentField['fieldType']=="checkbox"){
                $selectTable=explode('.',$currentField['data'])[0];
                if($selectTable!==""){ // Need to mention selectTable for relational column
                    $selectArray[ $currentField['fieldId'] ]=DB::table( $selectTable )
                        ->select('id',$currentField['data'].' as optionText')
                        ->orderBy( $currentField['data'] )
                        ->get()
                        ->toArray();
                }
                else{
                    die('data should be in relationalTble.field format for '.$currentField['fieldId'].'');
                }
            }
        }

        return view('simpleRest.'.$dt['theme'].'.create',compact('dt','resourceBase','selectArray'));
    }


    // /**
    //  * Store a newly created resource in storage.
    //  */
    public function store(Request $request){
        $dt=$this->getSimpleRestConfig();
        $resourceBase=explode('.',Route::currentRouteName())[0];

        $v=array();
        foreach($dt['fields'] as $currentField){
            if($currentField['fieldType']!==""){
                $v[ $currentField['fieldId'] ] = $currentField['validation'];
            }
        }
        $validator = Validator::make($request->all(), $v);

		if ($validator->fails()) {
            \Session::flash('flashError','Please fix the errors:');
            return redirect($resourceBase.'/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        // Saving
        $row=new $dt['model'];
        foreach($dt['fields'] as $currentField){
            // Upload Image
            if($currentField['fieldType']=="fileUpload"){
                $path = $request->file( $currentField['fieldId'] )->storeAs(
                    'uploads/'.$resourceBase, 
                    @$id.'_'.time().'.'.$request->file( $currentField['fieldId'] )->getClientOriginalExtension(), 
                    'public'
                );  //print_r($_FILES); dd($path.'|');
                $row->{$currentField['fieldId']}=$path;
            }            
            else if($currentField['fieldType']=="checkboxSingle"){
                if( $request->has($currentField['fieldId']) ) {
                    $row->{$currentField['fieldId']}=1;
                }
                else{
                    $row->{$currentField['fieldId']}=0;
                }
            }
            
            else if($currentField['fieldType']=="checkbox"){
                if( $request->has($currentField['fieldId']) ) {
                    $row->{$currentField['fieldId']}=implode(',',$request->get($currentField['fieldId']));
                }
                else{
                    $row->{$currentField['fieldId']}='';
                }
            }
            // Adding to object to save
            else if($currentField['fieldType']!==""){
                $row->{$currentField['fieldId']}=$request->get($currentField['fieldId']);
            }
            else{}
        }
        $row->save();
		
		\Session::flash('flashSuccess','Entry added successfully.');

		return redirect($resourceBase);
    }


    // /**
    //  * Display the specified resource.
    //  */
    
    public function show($id)
    {        
        $dt=$this->getSimpleRestConfig();
        $resourceBase=explode('.',Route::currentRouteName())[0];

        // $select=array();
        // foreach($dt['fields'] as $currentField){
        //     if($currentField['fieldType']!==""){
        //         $select[] = $currentField['data'];
        //     }
        // }
        // Getting option data for select
        $selectArray=array();
        foreach($dt['fields'] as $currentField){
            if($currentField['fieldType']=="select" || $currentField['fieldType']=="radio"|| $currentField['fieldType']=="checkbox"){
                $selectTable=explode('.',$currentField['data'])[0];
                if($selectTable!==""){ // Need to mention selectTable for relational column
                    $selectArray[ $currentField['fieldId'] ]=DB::table( $selectTable )
                        ->select('id',$currentField['data'].' as optionText')
                        ->orderBy( $currentField['data'] )
                        ->get()
                        ->toArray();
                }
                else{
                    die('data should be in relationalTble.field format for '.$currentField['fieldId'].'');
                }
            }
        }

        $row=$dt['query']->find($id);

		return view('simpleRest.'.$dt['theme'].'.show',compact('dt','row','resourceBase','selectArray'));
    }


    // /**
    //  * Show the form for editing the specified resource.
    //  */
    public function edit($id)
    {
        $dt=$this->getSimpleRestConfig();
        $resourceBase=explode('.',Route::currentRouteName())[0];

        // Getting option data for select
        $selectArray=array();
        foreach($dt['fields'] as $currentField){
            if($currentField['fieldType']=="select" || $currentField['fieldType']=="radio"|| $currentField['fieldType']=="checkbox"){
                $selectTable=explode('.',$currentField['data'])[0];
                if($selectTable!==""){ // Need to mention selectTable for relational column
                    $selectArray[ $currentField['fieldId'] ]=DB::table( $selectTable )
                        ->select('id',$currentField['data'].' as optionText')
                        ->orderBy( $currentField['data'] )
                        ->get()
                        ->toArray();
                }
                else{
                    die('data should be in relationalTble.field format for '.$currentField['fieldId'].'');
                }
            }
        }

        $row=$dt['query']->find($id);
		
		return view('simpleRest.'.$dt['theme'].'.edit',compact('dt','row','resourceBase','selectArray'));
    }


    // /**
    //  * Update the specified resource in storage.
    //  */
    public function update(Request $request, $id)
    {
        $dt=$this->getSimpleRestConfig();
        $resourceBase=explode('.',Route::currentRouteName())[0];

        $id=(int) $id;
        $v=array();
        foreach($dt['fields'] as $currentField){
            if($currentField['fieldType']!==""){
                $v[ $currentField['fieldId'] ] = $currentField['validation'];
            }
        }
        $validator = Validator::make($request->all(), $v);
		
		if ($validator->fails()) {
            return redirect($resourceBase.'/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        }
		
        // Saving 
		$row=$dt['query']->find($id);
		foreach($dt['fields'] as $currentField){
            // Upload Image
            if($currentField['fieldType']=="fileUpload"){
                $path = $request->file( $currentField['fieldId'] )->storeAs(
                    'uploads/'.$resourceBase, 
                    $id.'_'.time().'.'.$request->file( $currentField['fieldId'] )->getClientOriginalExtension(), 
                    'public'
                );  //print_r($_FILES); dd($path.'|');
                $row->{$currentField['fieldId']}=$path;
            }
            else if($currentField['fieldType']=="checkboxSingle"){
                if( $request->has($currentField['fieldId']) ) {
                    $row->{$currentField['fieldId']}=1;
                }
                else{
                    $row->{$currentField['fieldId']}=0;
                }
            }
            else if($currentField['fieldType']=="checkbox"){
                if( $request->has($currentField['fieldId']) ) {
                    $row->{$currentField['fieldId']}=implode(',',$request->get($currentField['fieldId']));
                }
                else{
                    $row->{$currentField['fieldId']}='';
                }
            }
            // Adding to object to save
            else if($currentField['fieldType']!==""){
                $row->{$currentField['fieldId']}=$request->get($currentField['fieldId']);
            }
            else{}
        }
		$row->save();
		
		\Session::flash('flashSuccess',$dt['title'].' Updated Successfully.');
		
		return redirect($resourceBase);
    }

    
    // /**
    //  * Remove the specified resource from storage.
    //  */
    public function destroy($id)
    {
        $dt=$this->getSimpleRestConfig();
        $resourceBase=explode('.',Route::currentRouteName())[0];

		$id=(int) $id;
        $row=$dt['query']->find($id)->delete();
		\Session::flash('flashSuccess',$dt['title'].' Disabled Successfully.');
		return redirect($resourceBase);
    }
	
	
	public function restore($id=0)
    {
        $dt=$this->getSimpleRestConfig();
        //$resourceBase=explode('.',Route::currentRouteName())[0];
        $resourceBase= explode('/',Route::getFacadeRoot()->current()->uri())[0];

		$id=(int) $id;
        $row=$dt['query']->find($id)->restore();
		\Session::flash('flashSuccess',$dt['title'].' Restored Successfully.');
		return redirect($resourceBase);
    }
    
}


/*
#
# SimpleRestDocumentation
# Last updated: 2018.06.19
# Objective: to have a configuration controller from where all regular BREAD operations can be done in a RESTful way
#
==================== Assumptions ====================
# Table name is plurized (as per laravel recommendation)
# Column names are singluar
# Table is normalized as much as possible
# Every table will have a field: 
    `id` int() NOT NULL AUTO_INCREMENT
# As per laravel coding style every table will have following fields:                 
    `created_at` datetime NOT NULL,                         
    `updated_at` datetime DEFAULT NULL,                     
    `deleted_at` datetime DEFAULT NULL,

==================== Sample Codes ====================
-------------------- Route Code --------------------
Route::resource('medicine','MedicineController',['only'=>[
    'index','create','store','show','edit','update','destroy'
]]);
Route::patch('medicine/{id}/restore','MedicineController@restore');
Route::POST('datatableJsonData/medicine','MedicineController@datatableJsonData');

-------------------- Model Code --------------------
<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Test extends Model
{
    use SoftDeletes;
	
	protected $dates=[
		'deleted_at'
	];
}

-------------------- Controller Code --------------------
<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Validator;

use App\Test;

class TestController extends SimpleRestController
{

    public function __construct(){
    }

    protected static function simpleRestConfig(){
        return array(
            'model'=>'App\Test',
            'title'=>'Test',
            'links'=>array(
                array('Add New', url('department/create'), 'fa fa-lg fa-plus'),
            ),
            'query'=>Test::withTrashed()
                ->select(['test.id as id', 'name', 'departments.department_name as department_id',DB::raw(" if(test.deleted_at is null,'active','inactive') as isActive")])
                ->JOIN('departments','test.department_id','=','departments.id'),
            "fields"=>array(
                array('fieldId'=>'id', 'data'=>'id', 'label'=>'ID', 'fieldType'=>''),
                array('fieldId'=>'name', "name","Name","text", 'validation'=>"required|min:5|max:125"),
                array('fieldId'=>'department_id', 'data'=>'departments.department_name', 'label'=>'Department', 'fieldType'=>'select', 'validation'=>"required|integer|min:1"),
                array('fieldId'=>'action', 'data'=>'action', 'label'=>'Action', 'fieldType'=>'', 'edit|show|disable' )
            )
        );
    }
    
}

==================== simpleRestConfig() method Details ====================
# Main Parameters:
    'model' => 'App\ModelName',
    'title' =>'XXX',    // this will be used in text as: 'XXX list' or 'XXX is updated' etc
    'query' =>Test::withTrashed()->select([columns]),    // withTrashed() have to be used, can use join
    'fields'=>array() // see below for details
# fields array have following items, first 4 item(field,data,label,fieldType) are compulsory: 
    fieldId:* unique
            * should be column name of main table
    data:   * source of data
            * Usually same as field, but for relational data it will be relationaTable.columnToDisplay
    label:  * Used as table header or label in create/edit etc
    fieldType:* type of field during CRUD operation
            * Keep blank if you do not want user to set it during CRUD. example: auto increment field 
            * Possible fieldType values:
                * text
                * textArea
                * select
                * radio
                * datepicker
                * datetimepicker
                * checkboxSingle : if checkbox values is saved in db as 0 or 1 in tinyint/int column
                * checkbox : if checkbox values is saved in db as comma separated values
    validation:* as of laravel validation(not all are implemened yet) 
# If field id is 'action' then it is considered as special column/field
    * action column can have some extra parameters-
        * 'edit'=>true, 
        * 'show'=>true, 
        * 'disable'=>true
        * 'links'=>array(
                array('title', url('abc/def/{id}'), 'fa fa-lg fa-cog'),
            )
*/