<?php
namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Validator;

use App\Unit;

class UnitController extends SimpleRestController
{

    public function __construct(){
    }

    protected static function simpleRestConfig(){
        return array(
            'model'=>'App\Unit',
            'title'=>'Unit',
            'links'=>array(
                array('Add New Unit', url('unit/create'), 'fa fa-lg fa-plus'),
            ),
            'query'=>Unit::withTrashed()
                ->select(['units.id as id','unit_name', DB::raw(" if(units.deleted_at is null,'active','inactive') as isActive")]),
            "fields"=>array(
                //array('fieldId','data','label','fieldType','validations/buttons'),
                array('id','id','ID','',''),
                array('unit_name','unit_name','Unit Name','text',"required|min:2"),
                array('action','','Action','','edit|show|disable' )
            )
        );
    }
    
}
