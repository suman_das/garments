<?php

namespace App\Http\Controllers\DistributorAdmin\Report;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\User;
use Datatables;
use App\Role;

use App\UserType;

use App\uploadCode;

use Validator;
use Session;
use Redirect;
use Image;
use DB;
use Auth;
use Entrust;
use Excel;


class DetailsReportController extends Controller
{
    private $view_path = "core.distributoradmin.report.";
    private $route_path = "distributoradmin.detailsreport.";
    public function report() {
        
        $retailers = User::with(['roles' => function($q){
            $q->where('name', 'retaileradmin');
        }])->where('created_by',Auth::user()->id)->pluck('name','id')->toArray();
        //dd($users);exit;
        //$retailers = BikeDetails::select('*')->groupBy('color')->lists('color')->toArray();
        //$territory = dealerDetails::select('territory')->groupBy('territory')->lists('territory','territory')->toArray();
       // $amount = uploadCode::select('amount')->groupBy('amount')->pluck('amount','amount')->toArray();
        $model = uploadCode::select('model')->groupBy('model')->pluck('model','model')->toArray();
       
        return view($this->view_path.'index',compact('retailers','amount','model'));
    }
    
    public function dataTableList(Request $request)
    {
        $requestData = $request->all();
        
        $tdate=$requestData['columns'][1]['search']['value'];
        $fdate=$requestData['columns'][0]['search']['value'];
        
        $today=date('Y-m-d H:i:s');
   
        $columns = array(

            0 => 'id'
        );

        //echo Auth::user()->dealer_code;exit;
        //$reserves = DB::table('reserves')->selectRaw('*, count(*)')->groupBy('day');
        
        $query = uploadCode::with('userinfo')->select('*');
        
        $query2 = uploadCode::with('userinfo')->select('*');
        
        
        $query->whereHas('userinfo', function ($quee){
                    $quee->where('users.id', '=',Auth::user()->id);
                });
        $query2->whereHas('userinfo', function ($quee) {
                $quee->where('users.id', '=',Auth::user()->id);
            });
        
        $parameter = array();

        if (!empty($requestData['columns'][2]['search']['value'])) {   
    
            $parameter["retailer_id"]=$requestData['columns'][2]['search']['value'];
            
            $query->where('product_information.retailer_id','=',$requestData['columns'][2]['search']['value']);
            $query2->where('product_information.retailer_id','=',$requestData['columns'][2]['search']['value']);
                    
                    
        }
        
        if (!empty($requestData['columns'][3]['search']['value'])) {
            
            $parameter["customer_mobile"]=$requestData['columns'][3]['search']['value'];
            
            $query->where('product_information.customer_mobile','=',$requestData['columns'][3]['search']['value']);
            $query2->where('product_information.customer_mobile','=',$requestData['columns'][3]['search']['value']);
        }
        if (isset($requestData['columns'][4]['search']['value'])) {   //name
            $parameter["status"]=$requestData['columns'][4]['search']['value'];
            $query->where('product_information.status','=',$requestData['columns'][4]['search']['value']);
            $query2->where('product_information.status','=',$requestData['columns'][4]['search']['value']);
        }
      
        
        if (isset($requestData['columns'][5]['search']['value'])) {   //name
            $parameter["dealer_stock_status"]=$requestData['columns'][5]['search']['value'];
            $query->where('product_information.dealer_stock_status','=',$requestData['columns'][5]['search']['value']);
            $query2->where('product_information.dealer_stock_status','=',$requestData['columns'][5]['search']['value']);
        }
        if (!empty($requestData['columns'][6]['search']['value'])) {   //name
            $parameter["model"]=$requestData['columns'][6]['search']['value'];
            $query->where('product_information.model','=',$requestData['columns'][6]['search']['value']);
            $query2->where('product_information.model','=',$requestData['columns'][6]['search']['value']);
        }

     
        $parameter["tdate"]=$requestData['columns'][1]['search']['value'];
        $parameter["fdate"]=$requestData['columns'][0]['search']['value'];
        
        if( !empty($requestData['columns'][1]['search']['value']) )
        {
            $tdate=$requestData['columns'][1]['search']['value'];
        }  else {
            $tdate=date('Y-m-d  H:i:s');
        }
        if ((!empty($fdate))&&!empty($tdate)) 
             {
                
               $query->whereBetween('product_information.w_dispatch_date',array($fdate,$tdate));
                $query2->whereBetween('product_information.w_dispatch_date',array($fdate,$tdate));
             
        }else
        {
            $query->whereDate('product_information.w_dispatch_date','<',$today);
            $query2->whereDate('product_information.w_dispatch_date','<',$today);
             $fdate='';
        }


        //dd($query->get());
        
                
        $requestData['order'][0]['dir'] = 'DESC';
        
        $totalData = count($query2->get()->toarray());
        $totalFiltered = $totalData;
        $query2->orderBy('product_information.'.$columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir']);
        $query2->offset($requestData['start']);
        $query2->limit($requestData['length']);
       
        
        $query =$query2->get();
        
        $data = array();
        $j = 0;
        foreach($query as $row) {
           //dd(@$row->retailInfo->name);
            $nestedData = array();
            $nestedData[] = @$row->retailInfo->name;
            $nestedData[] = @$row->retailInfo->dealer_code;
            $nestedData[] = $row->imei1;
            $nestedData[]= $row->sl;
            $nestedData[]= $row->color;
            $nestedData[]= $row->sts_date;
            $nestedData[]= $row->sts_status;
            $nestedData[]= ($row->dealer_stock_status==0) ? 'Yes' : 'No';
            $nestedData[]= $row->d_dispatch_date;
            $nestedData[]= $row->r_price;
            
            $data[] = $nestedData;
        }
    $url= url('superadmin/detailsreport/downloadReport');
        //print_r($data);exit;
        $query = '<tbody class="employee-grid-error"><tr><th colspan="3"><a href="'.$url.'?queryinfo=' .  http_build_query($parameter) . '">Download CSV </a></th></tr></tbody>';
        //$query2 = '<tbody class="employee-grid-error"><tr><th colspan="3"><a href="download_sms_summaryLog.php?queryinfo=' . $queryData . '">Download Summary Report CSV </a></th></tr></tbody>';
        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data,
            "query"=>$query
           
                // total data array
        );

        echo json_encode($json_data);
    }
    
    public function downloadExcel()

	{
            $today=date('Y-m-d H:i:s');
            if (Input::has('tdate'))
            {
               $tdate=Input::get('tdate'); 
            }else
            {
                 $tdate=date('Y-m-d  H:i:s');
            }
            if (Input::has('fdate'))
            {
                $fdate=Input::get('fdate'); 
            }
            
            $query = uploadCode::with('userinfo')->select('*');
            $query->whereHas('userinfo', function ($quee) use ($requestData){
                    $quee->where('users.dealer_code', '=',Auth::user()->dealer_code);
                });
        
            
            if (Input::has('dealer_code'))
            {
              $dealer_code= Input::get('dealer_code');
              $query->whereHas('userinfo', function ($quee) use ($requestData){
                    $quee->where('users.dealer_code', '=', $requestData['columns'][2]['search']['value']);
                });  
            }
            
            
            if (Input::has('customer_mobile'))
            {   
                $customer_mobile= Input::get('customer_mobile');
                $query->where('product_information.customer_mobile','=',$customer_mobile);  
            }
            if (Input::has('retailer_id'))
            {   
                $retailer_id= Input::get('retailer_id');
                $query->where('product_information.retailer_id','=',$retailer_id);  
            }
            
            if (Input::has('status'))
            {   
                $status= Input::get('status');
                $query->where('product_information.status','=',$status);  
            }
            
           
            if (Input::has('dealer_stock_status'))
            {   
                $w_stock_staus= Input::get('dealer_stock_status');
                $query->where('product_information.dealer_stock_status','=',$w_stock_staus);  
            }
            if (Input::has('model'))
            {   
                $model= Input::get('model');
                $query->where('product_information.model','=',$model);  
            }
          
            
            if ((!empty($fdate))&&!empty($tdate)) 
             {
               $query->whereBetween('product_information.d_dispatch_date',array($fdate,$tdate));
             
            }else
            {
                $query->whereDate('product_information.d_dispatch_date','<',$today);
                
                 $fdate='';
            }
            
            $data = $query->get()->toArray();
            $data2=array();
             foreach($data as $key=>$row) {
           
            $nestedData = array();
            $nestedData["Dealer Name"] = $row["userinfo"]["name"];
            $nestedData["Dealer Code"] = $row["userinfo"]["dealer_code"];
            $nestedData["Imei 1"] = $row["imei1"];
            $nestedData["Imei 2"]= $row["imei2"];
            $nestedData["Serial No."]= $row["sl"];;
            $nestedData["Activation Mobile"]= $row["customer_mobile"];
            $nestedData["Color"]= $row["color"];
            $nestedData["STS"]= $row["sts_status"];
            $nestedData["Stock"]= $row["dealer_stock_status"];
            $nestedData["Dispatch At"]= $row["d_dispatch_date"];
           
            
            $data2[] = $nestedData;
        }
            return Excel::create('ReportLog', function($excel) use ($data2) {

                    $excel->sheet('ReportLog', function($sheet) use ($data2)

            {

                            $sheet->fromArray($data2);

            });

            })->download('csv');

	}
}
