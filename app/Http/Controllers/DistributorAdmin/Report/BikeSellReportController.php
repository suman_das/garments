<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\User;
use Datatables;
use App\Role;

use App\UserType;
use App\dealerDetails;
use App\BikeDetails;
use App\Zone;
use Validator;
use Session;
use Redirect;
use Image;
use DB;
use Auth;
use Entrust;
use Excel;


class BikeSellReportController extends Controller
{
    public function report() {
        //dd('hi');
        if(!Entrust::can('user-list')) { abort(403); }
        //$dealers = DB::table('dealerinfo')->orderBy('id','DESC')->paginate(10);
        $colors = BikeDetails::select('color')->groupBy('color')->lists('color')->toArray();
        $divisions = dealerDetails::select('division')->groupBy('division')->lists('division')->toArray();
        //$divisions = json_decode(json_encode($divisions), true);
        //$divisions = $divisions->toArray();
        //$divisions=DB::table('dealerinfo')->groupBy('division')->get()->toArray();
        $dealers=dealerDetails::groupBy('code')->lists('name','code')->toArray();
        $zone=Zone::groupBy('zone')->lists('zone','zone')->toArray();
        //print_r($zone);
        $models=BikeDetails::groupBy('model')->lists('model')->toArray();
        //$dealerTypes = json_decode(json_encode($dealerTypes), true);
       //print_r($divisions);exit;
        return view('report.index',compact('divisions','dealerTypes','colors','dealers','models','zone'));
    }
    
    public function dataTableList(Request $request)
    {
        $requestData = $request->all();
        
        $tdate=$requestData['columns'][1]['search']['value'];
        $fdate=$requestData['columns'][0]['search']['value'];
        
        $today=date('Y-m-d H:i:s');
   
        $columns = array(

            0 => 'id'
        );


        
        
        $query = BikeDetails::select('bikeinfo.*','customerinfo.cname','customerinfo.cusomerContact','dealerinfo.name','dealerinfo.zone');
        $query->leftJoin('customerinfo', 'bikeinfo.id', '=', 'customerinfo.bikeInfo_id');
        $query->leftJoin('dealerinfo', 'bikeinfo.dealerCode', '=', 'dealerinfo.code');
        $query2 = BikeDetails::select('bikeinfo.*','customerinfo.cname','customerinfo.cusomerContact','dealerinfo.name','dealerinfo.zone');
        $query2->leftJoin('customerinfo', 'bikeinfo.id', '=', 'customerinfo.bikeInfo_id');
        $query2->leftJoin('dealerinfo', 'bikeinfo.dealerCode', '=', 'dealerinfo.code');

        $parameter = array();

        if (!empty($requestData['columns'][2]['search']['value'])) {   //name
            //$parameter.=" AND dealerinfo.phoneNumber = '" . $requestData['columns'][0]['search']['value'] . "' ";
            $parameter["engineNo"]=$requestData['columns'][2]['search']['value'];
            $query->where('bikeinfo.engineNo','=',$requestData['columns'][2]['search']['value']);
            $query2->where('bikeinfo.engineNo','=',$requestData['columns'][2]['search']['value']);
        }
        if (!empty($requestData['columns'][3]['search']['value'])) {   //name
            $parameter["chesisNo"]=$requestData['columns'][3]['search']['value'];
            $query->where('bikeinfo.chesisNo','=',$requestData['columns'][3]['search']['value']);
            $query2->where('bikeinfo.chesisNo','=',$requestData['columns'][3]['search']['value']);
        }

        if (!empty($requestData['columns'][4]['search']['value'])) {   //name
            $parameter["registrationNo"]=$requestData['columns'][4];
            $query->where('bikeinfo.registrationNo','=',$requestData['columns'][4]['search']['value']);
            $query2->where('bikeinfo.registrationNo','=',$requestData['columns'][4]['search']['value']);
        }
        if (!empty($requestData['columns'][5]['search']['value'])) {   //name
            $parameter["color"]=$requestData['columns'][5]['search']['value'];
            $query->where('bikeinfo.color','=',$requestData['columns'][5]['search']['value']);
            $query2->where('bikeinfo.color','=',$requestData['columns'][5]['search']['value']);
        }
       
        if (!empty($requestData['columns'][6]['search']['value'])) {   //name
            $parameter["dealerCode"]=$requestData['columns'][6]['search']['value'];
            $query->where('bikeinfo.dealerCode','=',$requestData['columns'][6]['search']['value']);
            $query2->where('bikeinfo.dealerCode','=',$requestData['columns'][6]['search']['value']);
        }
        if (!empty($requestData['columns'][7]['search']['value'])) {   //name
            $parameter["model"]=$requestData['columns'][7]['search']['value'];
            $query->where('bikeinfo.model','=',$requestData['columns'][7]['search']['value']);
            $query2->where('bikeinfo.model','=',$requestData['columns'][7]['search']['value']);
        }
        if (!empty($requestData['columns'][8]['search']['value'])) {   //name
            $parameter["zone"]=$requestData['columns'][8]['search']['value'];
            $query->where('dealerinfo.zone','=',$requestData['columns'][8]['search']['value']);
            $query2->where('dealerinfo.zone','=',$requestData['columns'][8]['search']['value']);
        }
        $parameter["tdate"]=$requestData['columns'][1]['search']['value'];
        $parameter["fdate"]=$requestData['columns'][0]['search']['value'];
        
        if( !empty($requestData['columns'][1]['search']['value']) )
        {
            $tdate=$requestData['columns'][1]['search']['value'];
        }  else {
            $tdate=date('Y-m-d  H:i:s');
        }
        if ((!empty($fdate))&&!empty($tdate)) 
             {
                
               $query->whereBetween('bikeinfo.updated_at',array($fdate,$tdate));
                $query2->whereBetween('bikeinfo.updated_at',array($fdate,$tdate));
             
        }else
        {
            $query->whereDate('bikeinfo.updated_at','<',$today);
            $query2->whereDate('bikeinfo.updated_at','<',$today);
             $fdate='';
        }


        $query->where('bikeinfo.status','=',1);
        $query2->where('bikeinfo.status','=',1);
        
        $query =$query->get();
        //$collection = collect($query);
        //var_dump($query);
        //$query = mysqli_query($con, $sql) or die("SQL Full: Error");
        $requestData['order'][0]['dir'] = 'DESC';
        $totalData = $query->count();
        $totalFiltered = $totalData;
//$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.
        //$queryData = base64_encode($parameter);
        //$sql.="ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ";  // adding length
        $query2->orderBy($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir']);
        $query2->offset($requestData['start']);
        $query2->limit($requestData['length']);
        //$query = mysqli_query($con, $sql) or die("error no data found: get data");
        $query =$query2->get()->toarray();
        // print_r($parameter);exit;
        $data = array();
        $j = 0;
        foreach($query as $row) {
           //print_r($row);exit;
            $nestedData = array();
            $nestedData[] = ucwords($row["doNo"]);
            $nestedData[] = ucwords($row["coNo"]);
            $nestedData[] = ucwords($row["model"]);
            $nestedData[] = ucwords($row["engineNo"]);
            $nestedData[] = ucwords($row["chesisNo"]);
            $nestedData[] = ucwords($row["color"]);
            $nestedData[] = ucwords($row["updated_at"]);
            $nestedData[] = ucwords($row["name"]);
            $nestedData[] = ucwords($row["cname"]);
             $nestedData[] =$row["cusomerContact"];
             $nestedData[] =$row["zone"];
            $nestedData[] = ucwords($row["registrationNo"]);
            $data[] = $nestedData;
        }
    $url= url('downloadExcel');
        //print_r($data);exit;
        $query = '<tbody class="employee-grid-error"><tr><th colspan="3"><a href="'.$url.'?queryinfo=' .  http_build_query($parameter) . '">Download CSV </a></th></tr></tbody>';
        //$query2 = '<tbody class="employee-grid-error"><tr><th colspan="3"><a href="download_sms_summaryLog.php?queryinfo=' . $queryData . '">Download Summary Report CSV </a></th></tr></tbody>';
        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data,
            "query"=>$query
           
                // total data array
        );

        echo json_encode($json_data);
    }
    
    public function downloadExcel()

	{
            $today=date('Y-m-d H:i:s');
            if (Input::has('tdate'))
            {
               $tdate=Input::get('tdate'); 
            }else
            {
                 $tdate=date('Y-m-d  H:i:s');
            }
            if (Input::has('fdate'))
            {
                $fdate=Input::get('fdate'); 
            }
            
            $query = BikeDetails::select('bikeinfo.*','customerinfo.cname','dealerinfo.name');
            $query->leftJoin('customerinfo', 'bikeinfo.id', '=', 'customerinfo.bikeInfo_id');
            $query->leftJoin('dealerinfo', 'bikeinfo.dealerCode', '=', 'dealerinfo.code');
            if (Input::has('engineNo'))
            {
                $engineNo= Input::get('engineNo');
               $query->where('bikeinfo.engineNo','=',$engineNo); 
            }
            if (Input::has('chesisNo'))
            {
              $chesisNo= Input::get('chesisNo');
             $query->where('bikeinfo.chesisNo','=',$chesisNo);    
            }
            if (Input::has('registrationNo'))
            {
              $registrationNo= Input::get('registrationNo');
              $query->where('bikeinfo.registrationNo','=',$registrationNo);  
            }
            if (Input::has('color'))
            {
              $color= Input::get('color');
              $query->where('bikeinfo.color','=',$color);  
            }
            if (Input::has('dealerCode'))
            {   
                $dealerCode= Input::get('dealerCode');
                $query->where('bikeinfo.dealerCode','=',$dealerCode);  
            }
            if (Input::has('model'))
            {
                $model= Input::get('model');
               $query->where('bikeinfo.model','=',$model);  
            }
            if (Input::has('zone'))
            {
                $zone= Input::get('zone');
               $query->where('dealerinfo.zone','=',$zone);  
            }
            
            if ((!empty($fdate))&&!empty($tdate)) 
             {
                
               $query->whereBetween('bikeinfo.updated_at',array($fdate,$tdate));
                
             
            }else
            {
                $query->whereDate('bikeinfo.updated_at','<',$today);
                
                 $fdate='';
            }
            $query->where('bikeinfo.status','=',1);
            $data = $query->get()->toArray();

            return Excel::create('BikeSellReport', function($excel) use ($data) {

                    $excel->sheet('Report', function($sheet) use ($data)

            {

                            $sheet->fromArray($data);

            });

            })->download('csv');

	}
}
