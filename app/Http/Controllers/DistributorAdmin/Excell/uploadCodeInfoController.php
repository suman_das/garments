<?php

namespace App\Http\Controllers\DistributorAdmin\Excell;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use App\uploadCode;
use DB;
use Validator;
use Redirect;
use Session;
use Entrust;

class uploadCodeInfoController extends Controller {

    private $view_path = "core.distributoradmin.excell.";
    private $route_path = "distributoradmin.product-upload.imeinformation.";

    public function __construct() {
        $this->middleware('role:superadmin|distributoradmin');
    }

    public function createBulkCode() {

        return view($this->view_path . 'codeinfo');
    }

    public function stoeBulkCode(Request $request) {
      
        if ($request->hasFile('file')) {
            $validation = Validator::make(
                            [
                                'file' => $request->file('file'),
                                'extension' => strtolower($request->file('file')->getClientOriginalExtension()),
                            ], [
                                'file' => 'required',
                                'extension' => 'required|in:xlsx,xls',
                            ]
            );
           
            if ($validation->fails()) {
                return Redirect::back()->withErrors($validation)->withInput();
            }

            $fileName = date('Y_m_d_H_i_s') . '.' . $request->file('file')->getClientOriginalExtension();

            $request->file('file')->move(
                    base_path() . '/public/uploads/excell/', $fileName
            );

            $filePath = base_path() . '/public/uploads/excell/' . $fileName;
            //$extension = $request->file('file')->getClientOriginalExtension();
            $rowPointer = 0;
            Excel::selectSheetsByIndex(0)->load($filePath, function ($reader) use(&$rowPointer)  {
                $data = $reader->toArray();
                //print_r($data);exit;
                     
                foreach ($data as $result) {

                    if ((empty($result["serial_no."])) || (empty($result["imei_1"])) ) {

                        continue;
                    } else {

                        $codecount = uploadCode::where('imei1', '=', $result["imei_1"])->first();
                        if (count($codecount)) {

                            continue;
                        } else {
                            $codeInfo = new uploadCode;
                            $codeInfo->sl = $result["serial_no."];
                            $codeInfo->r_date = $result["receiving_date"];
                            $codeInfo->model = $result["model_name"];
                            $codeInfo->color = $result["color"];
                            $codeInfo->imei1 = $result["imei_1"];
                            $codeInfo->imei2 = $result["imei_2"];
                            $codeInfo->box_id = $result["box_id"];
                            $codeInfo->remarks = $result["remarks"];
                            $codeInfo->save();
                            $rowPointer++;
                        }
                    }
                }
                
            });



            Session::flash('message', "Product $rowPointer information saved successfully");
            return redirect()->route($this->route_path.'index');
        } else {
            return Redirect::back()->withErrors(['error', 'File is required']);
            //Session::flash('message', "Special message goes here");
        }
    }

}
