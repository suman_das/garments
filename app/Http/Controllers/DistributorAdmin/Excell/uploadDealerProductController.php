<?php

namespace App\Http\Controllers\DistributorAdmin\Excell;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use App\uploadCode;
use DB;
use Validator;
use Redirect;
use Session;
use App\Role;
use App\User;
use App\UserType;
use Entrust;
use Carbon\Carbon;

class uploadDealerProductController extends Controller {

    private $view_path = "core.distributoradmin.excell.";
    private $route_path = "distributoradmin.product-upload.dealerimeiinformation.";

    public function __construct() {
        $this->middleware('role:superadmin|distributoradmin');
    }

    public function createBulkCode() {

        return view($this->view_path . 'dealercodeinfo');
    }

    public function storeBulkCode(Request $request) {
      
        if ($request->hasFile('file')) {
            $validation = Validator::make(
                            [
                                'file' => $request->file('file'),
                                'extension' => strtolower($request->file('file')->getClientOriginalExtension()),
                            ], [
                                'file' => 'required',
                                'extension' => 'required|in:xlsx,xls',
                            ]
            );
           
            if ($validation->fails()) {
                return Redirect::back()->withErrors($validation)->withInput();
            }

            $fileName = date('Y_m_d_H_i_s') . '.' . $request->file('file')->getClientOriginalExtension();

            $request->file('file')->move(
                    base_path() . '/public/uploads/excell/', $fileName
            );

            $filePath = base_path() . '/public/uploads/excell/' . $fileName;
            //$extension = $request->file('file')->getClientOriginalExtension();
            $rowPointer = 0;
            $misimei=0;
            $misdealer=0;
            $dcode=0;
            Excel::selectSheetsByIndex(0)->load($filePath, function ($reader) use(&$rowPointer,&$misimei,&$misdealer,&$dcode)  {
                $data = $reader->toArray();
                //print_r($data);exit;
                     
                foreach ($data as $result) {

                    if ((empty($result["imei"])) || (empty($result["retailer_code"])) ) {

                        continue;
                    } else {

                        $codecount = uploadCode::with('userinfo')
                                ->where(function($q) use ($result){
                                $q->where('imei1', '=', $result["imei"])
                                ->orWhere('imei2',$result["imei"]);
                                })
                                ->whereHas('userinfo', function ($quee){
                                    $quee->where('users.id', '=',\Auth::user()->id);
                                    })
                                ->first();
                                    
                      // print_r($codecount);exit;
                        //$usercount = user::where('dealer_code', '=', $result["dealer_code"])->first();
                        $adminUsers = User::whereHas('roles', function($q)
                        {
                            $q->where('name', 'retaileradmin');
                        })->where('email', '=', $result["retailer_code"])->first();
                       // print_r($adminUsers);exit;
                        $codecountd = uploadCode::where(function($q) use ($result){
                                $q->where('imei1', '=', $result["imei"])
                                ->orWhere('imei2',$result["imei"]);
                            })
                            ->where('dealer_stock_status', '=', 1)
                            ->first();
                          // print_r($codecountd);exit;
                        if (!count($codecount)) {
                            $misimei++;
                            continue;
                        }else if (!count($adminUsers)) {
                            $misdealer++;
                            continue;
                        }else if (count($codecountd)) {
                            $dcode++;
                            continue;
                        } else {
                                $product  = uploadCode::findOrFail($codecount->id);
                                //print_r($product);exit;
                                $product->dealer_stock_status = 1;
                                $product->retailer_id = $adminUsers->id;
                                $product->r_price = $result["price"];
                                $product->d_dispatch_date = Carbon::now();
                                $product->save();
                                $rowPointer++;
                        }
                    }
                }
                
            });



            Session::flash('message', "Total  $rowPointer information updated successfully,$misimei imei not found,$misdealer  retailer not match,$dcode found duplicate found");
            return redirect()->route($this->route_path.'index');
        } else {
            return Redirect::back()->withErrors(['error', 'File is required']);
            //Session::flash('message', "Special message goes here");
        }
    }

}
