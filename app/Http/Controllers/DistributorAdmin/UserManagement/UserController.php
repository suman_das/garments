<?php

namespace App\Http\Controllers\DistributorAdmin\UserManagement;

use App\Http\Controllers\Controller;

use App\Role;
use Validator;
use Redirect;
use App\User;
use App\UserType;
use Illuminate\Http\Request;

class UserController extends Controller
{
  private $view_path = "core.distributoradmin.user-management.";
  private $route_path = "distributoradmin.retail-management.";

  public function index(Request $request )
  {
    $roles = \DB::table('roles')->orderBy('display_name')->get();
    $users = "";
    $old = [ 'name' => ''];

    if($request->has('query'))
    {
      $old['query'] = $request->input('query');
      $old['name'] = $request->input('name');

      $users = User::where(function($query) use($request){

        if($request->has('role_id') && $request->role_id != ""){
          $role_id = $request->role_id;
          $query->whereHas('roles', function($query2) use($role_id) {
            $query2->where('roles.id', $role_id);
          });
        }

        if($request->has('name') && $request->name != ""){
          $query->where('name', 'LIKE', '%'.$request->name.'%');
        }

      })->where('id',auth()->user()->id)->paginate(15);

    }else{
      $users = User::with('roles')
              ->whereHas('roles', function($query2){
                $query2->where('roles.name','retaileradmin');
                })
              ->where('dealer_id',auth()->user()->id)->paginate(15);
      //dd($users);
    }
    return view($this->view_path.'index', compact('users', 'roles'));
  }

  public function create()
  {
    $roles = \DB::table('roles')->where('name','retaileradmin')->pluck('name','id')->toArray();
    $user = User::findOrFail(auth()->user()->id);
    //$districts = \DB::table('bd_info')->pluck('district','district')->toArray();
    $divisions = \DB::table('bd_info')->pluck('division','division')->toArray();
    return view($this->view_path.'create', compact('roles','divisions','user'));
  }

  public function store(Request $request)
  {
      
        $alldata=$request->all();
        
        $validation = Validator::make($request->all(), [
                'name' => 'required',
                'shop_name' => 'required',
                'designation' => 'required',
                'address' => 'required',
                'contact_no' => 'required',
                'district' => 'required',
            ]);

        if($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        }  
      
    $subdcode = strtoupper(auth()->user()->email);
    $subdcode=$subdcode.'R';
    $user_code = User::where('email','like', '' . $subdcode.'%')->count();
    if($user_code){
        $user_code +=$user_code;
    }  else {
        $user_code=1;
    }
    $dealer_code=$subdcode.($user_code+1);
    
    $user = new User;
    $user->fill($request->except('roles','query','_token'));
    
    $user->email = $dealer_code;
    $user->dealer_code = $dealer_code;
    $user->password = bcrypt('password');
    $user->created_by = auth()->user()->id;
    $user->updated_by = auth()->user()->id;
    $user->dealer_id=auth()->user()->id;

    $user->remember_token = str_random(60);
    $user->save();
    
    $user->attachRole(3);

    return redirect()->route($this->route_path.'index');
    
  }

  public function show(Request $request, $id)
  {
    $user = User::findOrFail($id);
    return view($this->view_path.'show', compact('user'));
  }

  public function edit(Request $request, $id)
  {
    $user = User::findOrFail($id);
    $cdivision=$user->division;
    $cdistrict=$user->district;
    
    $divisions = \DB::table('bd_info')->pluck('division','division')->toArray();
    $districts = \DB::table('bd_info')->where('division','=',$cdivision)->pluck('district','district')->toArray();
    $roles = \DB::table('roles')->pluck('name','id');
   
    return view($this->view_path.'edit', compact('roles','user','districts','divisions','cdivision','cdistrict'));
  }

  public function update(Request $request, $id)
  {
    $alldata=$request->all();
        
        $validation = Validator::make($request->all(), [
                'name' => 'required',
                'shop_name' => 'required',
                'designation' => 'required',
                'address' => 'required',
                'contact_no' => 'required',
                
            ]);

        if($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        } 
      
    $user              = User::findOrFail($id);
    $user->fill($request->except('roles','password','query','_token','remember_token'));
    $roles = $user->roles->first()->toArray();
        
    $rid=$roles["id"];
    
    //$user->password = bcrypt($request->password);
    //$user->created_by = auth()->user()->id;
    $user->updated_by = auth()->user()->id;
  
    $user->save();
    
    return redirect()->route($this->route_path.'index');
  }
}
