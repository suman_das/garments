<?php

namespace App\Http\Controllers\DistributorAdmin;

use App\Http\Controllers\Controller;
use Auth;
use App\uploadCode;
class DashboardController extends Controller {

  private $view_path = "core.distributoradmin.";

  public function dashboard()
  {
    $totalActivation=uploadCode::with('userinfo')
            ->whereHas('userinfo', function ($quee) {
                $quee->where('users.id', '=',Auth::user()->id);
            })
            ->where('sts_status',1)->count();
    $stockW=uploadCode::with('userinfo')
            ->whereHas('userinfo', function ($quee) {
                $quee->where('users.id', '=',Auth::user()->id);
            })->where('dealer_stock_status',0)->count();
    $totalSell=uploadCode::with('userinfo')
            ->whereHas('userinfo', function ($quee) {
                $quee->where('users.id', '=',Auth::user()->id);
            })->where('dealer_stock_status',1)->count();
    $totalModel=uploadCode::with('userinfo')
            ->whereHas('userinfo', function ($quee) {
                $quee->where('users.id', '=',Auth::user()->id);
            })->groupBy('model')->count();
    return view($this->view_path.'dashboard',compact('totalActivation','stockW','totalSell','totalModel'));
  }
}