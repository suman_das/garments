<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use JWTAuth;
use JWTAuthException;
use App\User;
use App\Product;
use App\Order;
use App\OrderSummary;
use App\Traders;
use DB;

class ApiController extends Controller
{

    public function __construct()
    {
        $this->user = new User;
    }
    
    public function login(Request $request){
        $credentials = $request->only('email', 'password');
		$rules = [
            'email' => 'required',
            'password' => 'required',
        ];
        $token = null;
		$validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            $content = [
        'data' => [],
        'error' => 'required_email_or_password',
        'status' => 'Fail',
        'status_code' => 400
      ];
		
            return response()->json($content);
        }
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                $content = [
        'data' => [],
        'error' =>'invalid_email_or_password' ,
        'status' => 'Fail',
        'status_code' => 400
      ];
                return response()->json($content);
            }
        } catch (JWTAuthException $e) {
            $content = [
        'data' => [],
        'error' =>'failed_to_create_token' ,
        'status' => 'Fail',
        'status_code' => 400
      ];
            return response()->json($content);
        }
		$user = JWTAuth::toUser($token); 
		$userId = $user->id;
		$totalTraders=$user->with('tradersinfo')->where('id',$userId)->count();
		$totalOrders=$user->with('ordersummaryinfo')->where('id',$userId)->whereHas('ordersummaryinfo', function($query)
		{
			
			$query->where('order_summary.type', 1);
		})->count();
		
		
		$totalRedeemed=$user->with('ordersummaryinfo')->where('id',$userId)->whereHas('ordersummaryinfo', function($query)
		{
			
			$query->where('order_summary.type', 0);
		})->count();
		
		$data["id"]= $user->id;
		$data["name"]= $user->name;
		$data["image"]= $user->image;
		$data["miisdn"]= $user->contact_number;
		$data["total_traders"]= $totalTraders;
		$data["total_order"]= $totalOrders;
		$data["total_redeemed"]= $totalRedeemed+$totalOrders;
		$data2[]=$data;
		$content = [
        'data' => $data2,
        'error' => '',
        'token' => $token,
        'status' => 'Succes',
        'status_code' => 200
      ];
		
        return response()->json(
            $content
        );
    }

    public function getAuthUser(Request $request){
        $user = JWTAuth::toUser($request->token);        
        return response()->json(['result' => $user]);
    }
	
	public function logout(Request $request) {
        $this->validate($request, ['token' => 'required']);
        try {
            JWTAuth::invalidate($request->input('token'));
            return response()->json(['success' => true]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
        }
    }
	Public function products(Request $request){
		$products = product::all();
		$data=[];
		$data2=[];
		foreach ($products as $product) {
			$data["id"]= $product->id;
			$data["sku_name"]= $product->sku_name;
			$data["img"]= \Config::get('app.url').'/ubl/'.$product->img;
			$data["trade_price"]= $product->trade_price;
			$data["mrp_price"]= $product->mrp_price;
			$data2[]=$data;
		}
		$content = [
        'data' => $data2,
        'error' => '',
        'message' => 'Product List',
        'status' => 'Succes',
        'status_code' => 200
      ];
	  return response()->json($content, 200);
		
	}
	Public function traders(Request $request){
		
		$user = JWTAuth::parseToken()->authenticate();

		//dd($user);
		
		//print_r($request->user()->get()->toArray());exit;
        $userId = $user->id;
		$data=$user->with('tradersinfo')->where('id',$userId)->first();
		//print_r($data);exit;
		//$products = product::all();
		$data1=[];
		$data2=[];
		foreach ($data->tradersinfo as $product) {
			//print_r($product);exit;
			$data1["id"]= $data->id;
			$data1["outlet_code"]= $product->outlet_code;
			$data1["outlet_name"]= $product->outlet_name;
                        $data1["location"]= $product->outlet_name;
                        $data1["territory"]= $product->territory;
                        $data1["phone"]= $product->outlet_contact;
                        $data1["route"]= $product->route;
                        $data1["channel_name"]= $product->channel_name;
			$data1["total_amount"]= $product->total_amount;
			$data1["total_redeemed"]= $product->total_redeemed;
			$data1["balance"]= $product->total_amount-$product->total_redeemed;
			$data2[]=$data1;
		}
		$content = [
        'data' => $data2,
        'error' => '',
        'message' => 'Traders List',
        'status' => 'Succes',
        'status_code' => 200
      ];
	  return response()->json($content, 200);
		
	}
	
	Public function saveinformation(Request $request){
		
		//$user = JWTAuth::invalidate($request->input('token'));
                //print_r($request->input('token'));    
                        //JWTAuth::parseToken()->authenticate();
            $user = JWTAuth::toUser($request->input('token')); 
		
		$content = [
		  'user_id' => $user->id,
		  'traders_info_id' => $request->input('traders_info_id'),
		  'price' => $request->input('price'),
		  'discount' => $request->input('discount'),
		  'total_price' => $request->input('price')-$request->input('discount'),
		  'type' => 1 
		];
		$dataInsertId = DB::table('order_summary')->insertGetId($content);
		
		Traders::find($request->input('traders_info_id'))->increment('total_redeemed', $request->input('discount'));

		
		$data = json_decode(json_encode($request->input('product_info')), true);
		$products = json_decode($data);
		//print_r($products);exit;
		
		foreach ($products as $product) {
			 Order::insert([
              'order_id' => $dataInsertId,
              'proudct_id'             => $product->id,
              'qty'                   => $product->qty,
			  'price'                   => $product->price,
              'treders_info_id'  => $request->input('traders_info_id'),
              'user_id' => $user->id
          ]);
		}
	if(!$dataInsertId)
    {
       $content = [
        'data' => [],
        'error' => '',
        'message' => 'Internal Server Error. Try Again',
        'status' => 'error',
        'status_code' => 500
      ];
    }
    else{
        $user = JWTAuth::toUser($request->input('token'));
        $userId=$user->id;
        $totalTraders=$user->with('tradersinfo')->where('id',$userId)->count();
        $totalOrders=$user->with('ordersummaryinfo')->where('id',$userId)->whereHas('ordersummaryinfo', function($query)
        {
                $query->where('order_summary.type', 1);
        })->count();

        $totalRedeemed=$user->with('ordersummaryinfo')->where('id',$userId)->whereHas('ordersummaryinfo', function($query)
        {
                $query->where('order_summary.type', 0);
        })->count();
        
        $tcount = DB::table('treders_info')->where('id', $request->input('traders_info_id'))->first();
        $redmed=$request->input('discount');
        $cbalance=($tcount->total_amount-$tcount->total_redeemed);
        $replytext="Dear $tcount->outlet_name, you have successfully redeemed BDT $redmed from your account by purchasing Pureit SKUs. Your current balance is BDT $cbalance. Please call 09613105105 for details. Thanks.";
        $msisdnt=$tcount->outlet_contact;
        $user = "pureitapp";
        $pass = "53L013i?";
        $sid = "pureitapp";
        $url = "http://192.168.92.138/pushapi/dynamic/server.php";
        $unique_id = "pureit";
        $param = "user=$user&pass=$pass&sid=$sid&";
        $sms = "sms[0][0]=$msisdnt&sms[0][1]=" . urlencode($replytext) . "&sms[0][2]=$unique_id";
        $data = $param . $sms . $sid;
        $crl = curl_init();
        curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($crl, CURLOPT_URL, $url);
        curl_setopt($crl, CURLOPT_HEADER, 0);
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($crl, CURLOPT_POST, 1);
        curl_setopt($crl, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($crl);
        
        curl_close($crl);
        
        $data=[];
        $data["total_traders"]= $totalTraders;
        $data["total_order"]= $totalOrders;
        $data["total_redeemed"]= $totalRedeemed+$totalOrders;
        $data2[]=$data;
        
      $content = [
        'data' => $data2,
        'error' => '',
        'message' => 'Data Uploaded to Server.',
        'status' => 'success',
        'status_code' => 200
      ];
    }
	  return response()->json($content, 200);
		
	}
	Public function saveRedeemInformation(Request $request){
		
		$user = JWTAuth::parseToken()->authenticate();
		
		$content = [
		  'user_id' => $user->id,
		  'traders_info_id' => $request->input('traders_info_id'),
		  'price' => 0,
		  'discount' => $request->input('discount'),
		  'total_price' => 0,
		  'type' => 0 
		];
		$dataInsertId = DB::table('order_summary')->insertGetId($content);
		
		Traders::find($request->input('traders_info_id'))->increment('total_redeemed', $request->input('discount'));

		
		
	if(!$dataInsertId)
    {
       $content = [
        'data' => [],
        'error' => '',
        'message' => 'Internal Server Error. Try Again',
        'status' => 'error',
        'status_code' => 500
      ];
    }
    else{
      $content = [
        'data' => [],
        'error' => '',
        'message' => 'Data Uploaded to Server.',
        'status' => 'success',
        'status_code' => 200
      ];
    }
	  return response()->json($content, 200);
		
	}
	Public function pushpullsms(Request $request){
		$data=$request->all();
		$sms=$data["sms"];
                $sms2=$data["sms"];
		$pieces = explode(" ", $sms);
		$traders_code=@$pieces[1];
		$product_code=@$pieces[2];
		$msisdn=$data["msisdn"];
                //->whereIn('id', [1, 2, 3])
                $product_code2='';
                if(($product_code==1)||($product_code==2)){
                    $product_code2=1;
                }
                if(($product_code==3)||($product_code==4)){
                    $product_code2=2;
                }
                
		$tcount = DB::table('treders_info')->where('outlet_code', $traders_code)->first();
		$pcount = DB::table('product')->where('product_code', $product_code)->first();
		$count = DB::table('sms_log')->where('status', 'Success')->where('msisdn', $msisdn)->first();
		$smssendstatus=0;
                if(!$tcount){
                        $smssendstatus=1;
			$status="Invalid Traders";
			$reply="Sorry Invalid Traders Code";
		}else if(!$pcount){
                        $smssendstatus=1;
			$status="Invalid Traders";
			$reply="Sorry Invalid Product Code";
		}
		else if(!$count){
			$status="Success";
			$customer_amount=$pcount->discount;
			$traders_amount=(int)$pcount->discount+50;
			Traders::find($tcount->id)->increment('total_amount', $traders_amount);
			$reply="Thanks for your request. You are now eligible for $customer_amount  Taka discount. You will get a call from Pureit Careline shortly.";
                        
                        $replytext="Congratulation! Your request for New Consumer Registration $msisdn has been accepted and BDT $traders_amount has been added to your account. Please send SMS to 6969 for details about your account. Thanks ";
                        $msisdnt=$tcount->outlet_contact;
                        $user = "pureitapp";
                        $pass = "53L013i?";
                        $sid = "pureitapp";
                        $url = "http://192.168.92.138/pushapi/dynamic/server.php";
                        $unique_id = "pureit";
                        $param = "user=$user&pass=$pass&sid=$sid&";
                        $sms = "sms[0][0]=$msisdnt&sms[0][1]=" . urlencode($replytext) . "&sms[0][2]=$unique_id";
                        $data = $param . $sms . $sid;
                        $crl = curl_init();
                        curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, FALSE);
                        curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 2);
                        curl_setopt($crl, CURLOPT_URL, $url);
                        curl_setopt($crl, CURLOPT_HEADER, 0);
                        curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($crl, CURLOPT_POST, 1);
                        curl_setopt($crl, CURLOPT_POSTFIELDS, $data);
                        $response = curl_exec($crl);
                        curl_close($crl);
                        
                        
                }else{
                        $smssendstatus=1;
			$decline="Invalid Traders";
			$reply="Dear Consumer, your request has been declined. For details please call to our Pureit Careline at 09613105105.";
		}
                if($smssendstatus==1){
                    $replytext="Dear Trader, your request has been declined. For details-please call to our Pureit Careline at 09613105105 ";
                    $msisdnt=@$tcount->outlet_contact;
                    $user = "pureitapp";
                    $pass = "53L013i?";
                    $sid = "pureitapp";
                    $url = "http://192.168.92.138/pushapi/dynamic/server.php";
                    $unique_id = "pureit";
                    $param = "user=$user&pass=$pass&sid=$sid&";
                    $sms = "sms[0][0]=$msisdnt&sms[0][1]=" . urlencode($replytext) . "&sms[0][2]=$unique_id";
                    $data = $param . $sms . $sid;
                    $crl = curl_init();
                    curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, FALSE);
                    curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 2);
                    curl_setopt($crl, CURLOPT_URL, $url);
                    curl_setopt($crl, CURLOPT_HEADER, 0);
                    curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($crl, CURLOPT_POST, 1);
                    curl_setopt($crl, CURLOPT_POSTFIELDS, $data);
                    $response = curl_exec($crl);
                    curl_close($crl); 
                }
		
		$content = [
		  'msisdn' => @$msisdn,
		  'sms' => @$sms2,
		  'reply' => @$reply,
		  'traders_code' => @$traders_code,
		  'traders_amount' => @$traders_amount,
		  'customer_amount' => @$customer_amount,
                  'product_type'=> @(int)$product_code2,
		  'product_code' => @$product_code,
		  'status' => @$status
		];
		$dataInsertId = DB::table('sms_log')->insertGetId($content);
		if($dataInsertId){
			echo $reply;
		}
		
		
	}
        
        Public function getpoint(Request $request){
		$data=$request->all();
		$sms=$data["sms"];
		$pieces = explode(" ", $sms);
		$traders_code=@$pieces[1];
		$msisdn=$data["msisdn"];
              
                
		$tcount = DB::table('treders_info')->where('outlet_contact', $msisdn)->first();
		//$pcount = DB::table('product')->where('product_code', $product_code)->first();
		//$count = DB::table('sms_log')->where('status', 'Success')->where('msisdn', $msisdn)->where('product_type', $product_code2)->first();
		if(!$tcount){
			$status="Invalid Traders";
			$reply="Please try from your registered mobile number.";
		}else{
			$decline="Success";
                        $amount=$tcount->total_amount-$tcount->total_redeemed;
			$reply="Dear $tcount->outlet_name, your current balance is BDT $amount. Keep registering more new consumers to earn more. Thanks .";
		}
		
			echo $reply;
		
		
		
	}
	

}
