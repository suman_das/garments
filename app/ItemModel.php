<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemModel extends Model
{
   protected $table = 'models';
   protected $guarded = [
      'id','created_at', 'updated_at'
   ];
  
}