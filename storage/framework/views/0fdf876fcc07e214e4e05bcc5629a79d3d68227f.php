<?php $__env->startSection('content'); ?>

    <link href="<?php echo e(URL::asset('assets/pages/css/profile.min.css')); ?>" rel="stylesheet" type="text/css" />

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<?php echo e(URL::to('home')); ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="<?php echo e(URL::to('user')); ?>">User</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>View</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> User
        <small> view</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PROFILE SIDEBAR -->
            <div class="profile-sidebar">

                <!-- PORTLET MAIN -->
                <div class="portlet light profile-sidebar-portlet ">
                    <!-- SIDEBAR USERPIC -->
                   
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name"> <?php echo e($user->name); ?> </div>
                        <div class="profile-usertitle-job"> <?php echo e($user->roleName); ?> </div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                    <div class="profile-userbuttons">
                        <?php if($user->status == 1): ?>
                            <p class="btn btn-circle green btn-sm">Active</p>
                        <?php else: ?>
                            <p class="btn btn-circle red btn-sm">Inactive</p>
                        <?php endif; ?>
                    </div>
                    <!-- END SIDEBAR BUTTONS -->
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li>
                                <a href="<?php echo e(URL::to('user/'.$user->id.'/edit')); ?>">
                                <i class="icon-pencil"></i> Update </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
                <!-- END PORTLET MAIN -->

            </div>

           

            <!-- PORTLET MAIN -->
            <div class="profile-sidebar">
            
                <div class="portlet light ">
                    <div>
                        <h4 class="profile-desc-title">Contact</h4>
                        <div class="margin-top-20 profile-desc-link">
                            <i class="fa fa-envelope"></i>
                            <a href="mailto:<?php echo e($user->email); ?>"><?php echo e($user->email); ?></a>
                        </div>
                        <div class="margin-top-20 profile-desc-link">
                            <i class="fa fa-mobile"></i>
                            <a href="tel:<?php echo e($user->msisdn); ?>"><?php echo e($user->msisdn); ?></a>
                        </div>
                       
                    </div>
                </div>
                <!-- END PORTLET MAIN -->
            </div>

        </div>
    </div>

    <script type="text/javascript">
        $(document ).ready(function() {
            // Navigation Highlight
            highlight_nav('user-manage', 'users');
        });
    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.appinside', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>