<?php $__env->startSection('content'); ?>

  <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(route('superadmin.dashboard')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('superadmin.machinedata.index')); ?>">Machine data</a></li>
    <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo e(route('superadmin.machinedata.create')); ?>">New Machine Data</a></li>
  </ol>
</nav>

  <?php echo $__env->make('partials.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  <h1 class="page-title"> Order List <small></small> </h1>

  <div class="row my-3 card no-b shadow">
      <div class="col-md-12">
          <div class="card-header white p-0 pt-4">
              <h6 class="text-uppercase d-inline-block"> Daily Machine Data Report</h6>
              <?php if(Session::has('success')): ?>
            <div class="alert alert-warning alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?php echo e(Session::get('success')); ?>

</div>
<?php endif; ?>
              <div class="float-right d-inline-block table-filter">
                  <div class="input-group mb-2 mr-sm-2">
                      <button type="submit" class="btn btn-default mr-2 rounded-0" id="dashboardFilter"><span
                              class="s-18 icon-search-plus2 mr-2"></span>Filter
                      </button>
                     
                  </div>
              </div>
              <div class="mt-4">
                  <form action="<?php echo e(route('superadmin.machinedata.index')); ?>" class="" method="GET" id="createForm">
                      <input type="hidden" name="query" value="<?php echo e(time()); ?>" />
                  <div class="filterTable mb-4 white">
                      <div class="col-md-12">
                          <div class="p-0 pt-4">
                              <div class="row">
                                  <div class="col-md-5 pr-1 pl-0">
                                      <?php echo Form::select('unit', array('CGL' => 'CGL','LILI' => 'LILY'), null, ['class' => 'form-control js-example-basic-single', 'required' => 'required','id'=>'unit']); ?>

                                  </div>
                                  
                                   <div class="col-md-5 p-0 pr-1">
              
                                    <?php echo Form::text('date', null, ['class' => 'form-control', 'placeholder' => 'Start date:', 'required' => 'required','id'=>'date']); ?>

                                    </div>
                                  <div class="col-md-2 p-0 pr-1">
                                      <button type="submit" class="btn btn-outline-primary btn-sm rounded-0 filter-btn w-100"><i class="icon-arrow_forward mr-2"></i>Search</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <?php echo Form::close(); ?>

              </div>
          </div>
      </div>
  </div>
  <?php if(count($manpowers)): ?>

  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
      <div class="portlet-body util-btn-margin-bottom-5">

        <div class="table-responsive">
          <style>
            #example0 td {
              white-space: nowrap;
              font-size: 10px;
            }
            #example0 th {
              font-size: 10px;
            }
          </style>
        <div class="card-body table-responsive white px-0">
          <table class="table table-striped table-hover r-0" id="example0">
            <thead >
              <tr>
              <th>Unit</th>
              <th>Rented Machine in Factory :</th>
              <th>Idle Machine Outside Line :</th>
              <th>Used Machine in Sewing Line:</th>
              <th>Sample + Others:</th>
              <th>Total MCs in Factory:</th>
             <th>Action</th>
              </tr>
            </thead>

            <tbody>
              <?php $__currentLoopData = $manpowers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $manpower): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
             
                <tr>

                  <td><?php echo e($manpower->unit); ?></td>
                  <td><?php echo e($manpower->rented_machine); ?></td>
                  <td><?php echo e($manpower->idle_machine); ?> </td>
                  <td><?php echo e($manpower->used_machine); ?></td>
                  <td><?php echo e($manpower->sample_section +$manpower->finishing_section+$manpower->cutting_section+$manpower->power+$manpower->mvc_godwon); ?> </td>
                  <td><?php echo e($manpower->used_machine+$manpower->idle_machine+$manpower->sample_section +$manpower->finishing_section+$manpower->cutting_section+$manpower->power+$manpower->mvc_godwon); ?></td>
                  
                  
                  <td>
                       <p>
        
        <a href="<?php echo e(route('superadmin.machinedata.edit', $manpower->id)); ?>" class="btn btn-primary">Edit Task</a>
    </p>
                  </td>

                 
                </tr>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>

            <tfoot></tfoot>
          </table>
        </div>
        </div>
          <?php echo e($manpowers->appends($_GET)->links()); ?>

      </div>
    </div>
  </div>



  <?php else: ?>
  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
      <p>
        No Data Found
      </p>
    </div>
  </div>
  
  <?php endif; ?>
  <script type='text/javascript'>
    $('#date').datepicker({
    format: 'yyyy-mm-dd',
});
</script>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('my_js'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.appinside', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>