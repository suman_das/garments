<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/img/basic/favicon.ico" type="image/x-icon">
    <title>Concorde Group</title>
    <!-- CSS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="<?php echo e(asset('assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')); ?>" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/app.css')); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo e(asset('assets/js/bootstrap-daterangepicker/daterangepicker.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css" rel="stylesheet" type="text/css" />

    <!-- BEGIN DATATABLE -->
    <!--      <script src="<?php echo e(asset('theme/assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script> -->
    <script src="<?php echo e(asset('assets/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <!-- <script src="<?php echo e(asset('theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script> --><!-- 
    <script src="<?php echo e(asset('theme/assets/pages/scripts/table-datatables-ajax.min.js')); ?>" type="text/javascript"></script> -->
    <!-- END DATATABLE -->

    <!-- Datetimepicker JS : http://t1m0n.name/air-datepicker/docs/ -->
    

    <?php echo $__env->yieldContent('select2CSS'); ?>
</head>
<body class="light">
<!-- Pre loader -->
<div id="loader" class="loader">
    <div class="plane-container">
        <div class="preloader-wrapper big active">
            <div class="spinner-layer spinner-blue-only">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>
        </div>
    </div>
</div>
<div id="app">
    <div class="page container-fluid">
        <div class="row navbar navbar-expand d-flex justify-content-between blue accent-3 shadow">
            <div class="relative">
                <div class="d-flex">
                    <div>
                        <h1 class="nav-title text-white">Dashboard</h1>
                    </div>
                </div>
            </div>
            <!--Top Menu Start -->
            <div class="navbar-custom-menu p-t-10">
                <div class="d-block d-sm-block d-md-none">
                    <a class="btn-fab fab-right btn-primary btn-mob-nav" data-toggle="control-sidebar">
                        <i class="icon icon-menu"></i>
                    </a>
                    <div class="nav navbar-nav">
                        <div class="dropdown custom-dropdown user user-menu mobile-user-nav">
                            <a href="#" class="nav-link" data-toggle="dropdown">
                                <img src="assets/img/basic/u8.png" class="user-image" alt="User Image">
                            </a>
                            <!--<div class="dropdown-menu">
                                <div class="list-group mt-3" id="userSettingsCollapse">
                                    <a href="index.html" class="list-group-item list-group-item-action ">
                                        <i class="mr-2 icon-umbrella text-blue"></i>Profile
                                    </a>
                                    <a href="#" class="list-group-item list-group-item-action"><i
                                            class="mr-2 icon-cogs text-yellow"></i>Settings</a>
                                    <a href="#" class="list-group-item list-group-item-action"><i
                                            class="mr-2 icon-security text-purple"></i>Change Password</a>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
                <ul class="nav navbar-nav d-none d-md-block">
                    <?php
                    $role = \Auth::user()->roles->first()->name;
                ?>

                <?php if($role == 'superadmin'): ?>
                    <?php echo $__env->make('layouts.nav.superadministrator', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php endif; ?>
               
                   
                </ul>
            </div>
        </div>
         <?php echo $__env->yieldContent('content'); ?>
    </div>
</div>

<footer class="my-5 text-center">
    <div class="w-100 mb-3">
        <img class="w-80px" src="<?php echo e(asset('assets/img/basic/logo.png')); ?>">
    </div>
    <div class="w-100">
        Copyright &copy; 2018 <a href="#">Concorde Garments Group</a>
    </div>
</footer>
<!--/#app -->
<!-- Mobile Navigatio  -->
<aside class="control-sidebar fixed mobile-nav">
    <div class="sidebar-header">
        <h4>Navigation</h4>
        <a href="#" data-toggle="control-sidebar" class="paper-nav-toggle  active"><i></i></a>
    </div>
    <div class="p-2">
        <ul class="sidebar-menu">
            <li class="treeview">
                <a href="dashboard.html">Dashboard</a>
            </li>
            <li class="treeview">
                <a href="report-overview.html">Report Overview</a>
            </li>
            <li class="treeview">
                <a href="line-report.html">Line Report</a>
            </li>
        </ul>
    </div>
</aside>
<script src="<?php echo e(asset('assets/js/app.js')); ?>" type="text/javascript"></script>
<?php echo $__env->yieldContent('select2JS'); ?>
<?php echo $__env->yieldContent('my_js'); ?>
    
</body>
</html>