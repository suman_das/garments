<?php $__env->startSection('content'); ?>

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<?php echo e(URL::to('home')); ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Users</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Users
        <small>view & update</small>
    </h1>
    
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->

    

    <div class="row">
        <div class="col-md-12">
            
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-users font-dark"></i>
                        <span class="caption-subject bold uppercase">Users</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="example0">
                        <thead>
                            <tr role="row" class="heading">
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>User Group</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php if(count($users) > 0): ?>
                              <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                 <tr>
                                    <td><?php echo e($user->name); ?></td>
                                    <td><?php echo e($user->email); ?></td>
                                    <td><?php echo e($user->msisdn); ?></td>
                                    <td><?php echo e($user->roles->first()->display_name); ?></td>
                                    <td><?php echo $user->status == 1 ? '<span class="label label-success"> Active </span>' : '<span class="label label-danger"> Inactive </span>'; ?></td>
                                    <td>
                                       <div class="btn-group pull-right">
                                           <button class="btn green btn-xs btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                               <i class="fa fa-angle-down"></i>
                                           </button>
                                           <ul class="dropdown-menu pull-right">
                                               <li>
                                                   <a href="user-management/show/<?php echo e($user->id); ?>">
                                                       <i class="fa fa-file-o"></i> View </a>
                                               </li>
                                               <li>
                                                   <a href="user-management/edit/<?php echo e($user->id); ?>">
                                                       <i class="fa fa-pencil"></i> Update </a>
                                               </li>
                                           </ul>
                                       </div>
                                    </td>
                                 </tr>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           <?php endif; ?>
                        </tbody>
                    </table>
                    <div class="pagination">
                        <?php echo $users->links(); ?>

                    </div>
                </div>
            </div>
            
        </div>
    </div>

    <script type="text/javascript">
        $(document ).ready(function() {
            // Navigation Highlight
            highlight_nav('user-manage', 'users');

          

            <?php 
               if( !empty($req) )
               {
                  foreach($req as $key => $val)
                  {
                     echo "document.getElementById('".$key."').value = '".$val."';";
                  }
               }
             ?>

        });

     
    </script>
</script>
<style media="screen">
.table-filtter .btn{ width: 100%;}
.table-filtter {
   margin: 20px 0;
}
</style>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.appinside', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>