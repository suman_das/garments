<?php $__env->startSection('content'); ?>

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?php echo e(route('superadmin.dashboard')); ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="<?php echo e(route('superadmin.retailer-management.index')); ?>">Traders Management</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Add New</span>
        </li>
    </ul>
</div>

<?php echo $__env->make('partials.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<h1 class="page-title"> Traders Management <small></small> </h1>

<div class="col-md-12">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Add New Traders </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

        <div class="portlet-body form">
            <?php echo Form::open(array('route' => 'superadmin.retailer-management.store', 'method' => 'post','class'=>'form-horizontal','id'=>'createForm')); ?>


            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="query" value="<?php echo e(time()); ?>" />

            <div class="form-body">

                <div class="row">
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">PPO</label>
                                <div class="col-md-4">
                                    <?php echo Form::select('dealer_id',array('' => 'Add Distributor')+$distributors, [], ['class' => 'form-control', 'required' => 'required', 'id' => 'distributor']); ?>


                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Outlet Code</label>
                                <div class="col-md-4">
                                    <?php echo Form::text('outlet_code', null, ['class' => 'form-control', 'placeholder' => 'Outlet Code', 'required' => 'required']); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Outlet Name</label>
                                <div class="col-md-4">
                                    <?php echo Form::text('outlet_name', null, ['class' => 'form-control', 'placeholder' => 'Outlet name', 'required' => 'required']); ?>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Adress</label>
                                <div class="col-md-4">
                                    <?php echo Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Address', 'required' => 'required']); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Outlet Contact</label>
                                <div class="col-md-4">
                                    <?php echo Form::text('outlet_contact', null, ['class' => 'form-control', 'placeholder' => 'Outlet Contact', 'required' => 'required']); ?>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Channel Name</label>
                                <div class="col-md-4">
                                    <?php echo Form::text('channel_name', null, ['class' => 'form-control', 'placeholder' => 'Channel Name', 'required' => 'required']); ?>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">Route</label>
                            <div class="col-md-4">
                                <?php echo Form::text('route', null, ['class' => 'form-control', 'placeholder' => 'Route', 'required' => 'required']); ?>


                            </div>
                        </div>
                    </div>
                </div>


                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">Territory</label>
                            <div class="col-md-4">
                                <?php echo Form::text('territory', null, ['class' => 'form-control', 'placeholder' => 'Territory', 'required' => 'required']); ?>

                            </div>

                        </div>
                    </div>
                </div>

                

                <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-6">
                            <input type="submit" class="btn green" value="Add New Traders" />
                        </div>
                    </div>
                </div>


            </div>
            <?php echo Form::close(); ?>


        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('my_js'); ?>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.appinside', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>