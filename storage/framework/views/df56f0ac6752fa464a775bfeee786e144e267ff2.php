<?php $__env->startSection('content'); ?>

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="<?php echo e(URL::to('home')); ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="<?php echo e(URL::to('user')); ?>">User</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Update</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Users
        <small>update user</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->

    <div class="mt-element-step">
        <div class="row step-background-thin">
            <a href="<?php echo e(URL::to('user-management/'.$user->id.'/edit')); ?>">
                <div class="col-md-12 bg-grey-steel mt-step-col active">
                    <div class="mt-step-number"></div>
                    <div class="mt-step-title uppercase font-grey-cascade">Edit Basic</div>
                    <div class="mt-step-content font-grey-cascade">Personal information</div>
                </div>
            </a>

        </div>

            <?php echo $__env->make('core.superadmin.user-management.edit.1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


    </div>

    <script type="text/javascript">

        $(document ).ready(function() {
            // Navigation Highlight
            highlight_nav('user-manage', 'users');
        });

    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.appinside', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>