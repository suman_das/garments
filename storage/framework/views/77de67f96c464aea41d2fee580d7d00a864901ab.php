<?php echo Form::model($user, array('url' => '/user/'.$user->id, 'method' => 'put')); ?>


    <div class="row">

        <?php echo $__env->make('partials.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <div class="col-md-6 col-md-offset-3">
            <div class="row padding-top-20"></div>
            <div class="form-group">
                <label class="control-label">Name</label>
                <?php echo Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name', 'required' => 'required']); ?>

            </div>

            <div class="form-group">
                <label class="control-label">Email</label>
                <?php echo Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required' => 'required']); ?>

            </div>

            <div class="form-group">
                <label class="control-label">Mobile Number</label>
                
                
                    <?php echo Form::text('msisdn', null, ['class' => 'form-control', 'placeholder' => 'Mobile Number',  'required' => 'required']); ?>

                
            </div>

            

            <div class="form-group">
                <label class="control-label">Select Status</label>
                <?php echo Form::select('status', array('1' => 'Active','0' => 'Inactive'), null, ['class' => 'form-control js-example-basic-single', 'required' => 'required']); ?>

            </div>
            <div class="form-group"> 
                <label class="control-label">Select User Group</label>
                <?php echo Form::select('roles',$roles,$role_type, ['class' => 'form-control js-example-basic-single js-country', 'required' => 'required', 'id' => 'id']); ?>

            </div>

        </div>

        

    </div>

    &nbsp;
    <div class="row padding-top-10">
        <a href="javascript:history.back()" class="btn default"> Back </a>
        <?php echo Form::submit('Update', ['class' => 'btn green center-block']); ?>

    </div>

<?php echo Form::close(); ?>


<script type="text/javascript">
    $(document ).ready(function() {
        // Get State list
        // var country_id = $('#country_id').val();
    });

    // Get State list On Country Change
//    $('#country_id').on('change', function() {
//        if($(this).val() != <?php echo e($user->country_id); ?>){
//            get_states($(this).val());
//        }
//    });
//
//    // Get City list On State Change
//    $('#state_id').on('change', function() {
//        if($(this).val() != <?php echo e($user->state_id); ?>){
//            get_cities($(this).val());
//        }
//    });
//
//    // Get Zone list On City Change
//    $('#city_id').on('change', function() {
//        if($(this).val() != <?php echo e($user->city_id); ?>){
//            get_zones($(this).val());
//        }
//    });
</script>