<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo e(route('superadmin.dashboard')); ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('superadmin.machinedata.index')); ?>">Machine Data</a></li>
        <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo e(route('superadmin.machinedata.create')); ?>">New Machine Data</a></li>
    </ol>
</nav>


<?php echo $__env->make('partials.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<h1 class="page-title d-flex justify-content-center"> Target Data <small></small> </h1>
<div class="portlet mb-3">
    <div class="portlet-title">
        <div class="caption" style="text-align: center">
            <i class="fa fa-gift"></i>Add New Target</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
</div>
<div class="col-md-12 d-flex ">
<div class="col-md-3">
    <?php echo Form::open(array('route' => 'superadmin.target.store', 'method' => 'post','class'=>'needs-validation','id'=>'createForm')); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="date">DATE</label>
                <input type="text" class="form-control datetime-picker" name="date" id="date" value="<?php echo e(date('Y-m-d')); ?>"
                       placeholder="Ex. YYYY-MM-DD" readonly>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="buyer">Order</label>
                <?php echo Form::select('order_id', $buyer, null, [
                'class' => 'form-control',
                'id'    => 'order'
                ]); ?>


            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Line Number </label>
                    <select class="form-control" name="line_no">
                        <option value="1">LINE 01</option>
                        <option value="2">LINE 02</option>
                        <option value="3">LINE 03</option>
                        <option value="4">LINE 04</option>
                        <option value="5">LINE 05</option>
                        <option value="6">LINE 06</option>
                        <option value="7">LINE 07</option>
                        <option value="8">LINE 08</option>
                        <option value="9">LINE 09</option>
                        <option value="10">LINE 10</option>
                        <option value="11">LINE 11</option>
                        <option value="12">LINE 12</option>
                        <option value="13">LINE 13</option>
                        <option value="14">LINE 14</option>
                        <option value="15">LINE 15</option>
                        <option value="16">LINE 16</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="sam">SAM</label>
                    <input type="text" class="form-control datetime-picker" name="sam" id="sam" value="" placeholder="SAM" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="a_machine">Allocated Machine</label>
                    <input type="text" class="form-control datetime-picker" name="a_machine" id="a_machine" value="" placeholder="Allocated Machine" >
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="a_operator">Allocated Operator</label>
                    <input type="text" class="form-control datetime-picker" name="a_operator" id="a_operator" value="" placeholder="Allocated Operator" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="p_operator">Present Operator</label>
                    <input type="text" class="form-control datetime-picker" name="p_operator" id="p_operator" value="" placeholder="Present Operator" >
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="s_helper_ironman">Sewing Helper + Iron Man</label>
                    <input type="text" class="form-control datetime-picker" name="s_helper_ironman" id="s_helper_ironman" value="" placeholder="Sewing Helper + Iron Man" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="li_sv">L/I & S/V</label>
                    <input type="text" class="form-control datetime-picker" name="li_sv" id="li_sv" value="" placeholder="L/I & S/V" >
                </div>
            </div>


            <div class="col-md-6">
                <div class="form-group">
                    <label for="todays_target">Todays Target</label>
                    <input type="text" class="form-control" id="todays_target" name="todays_target"
                           placeholder="Todays Target"  required="">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Hours </label>
                <select class="form-control"  onchange="updateCheckBox(this)" id="vatiation">
                    <option value="1">(8am-9pm)</option>
                    <option value="2">(8am-1pm)</option>
                    <option value="3">(2pm-9pm)</option>
                    <option value="4">(8pm-11pm)</option>
                </select>
            </div>
        </div>


        <div class="col-md-12">
            <label>Select Hours</label>
        </div>
        <div class="col-md-3">
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours0" value="0"> 00<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours4" value="4"> 04<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours8" checked value="8"> 08<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours12" checked value="12"> 12<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours16" checked value="16"> 16<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours20" value="20"> 20<br>
        </div>
        <div class="col-md-3">
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours1"  value="1"> 01<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours5" value="5"> 05<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours9" checked value="9"> 09<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours13" value="13"> 13<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours17" checked value="17"> 17<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours21" value="21"> 21<br>
        </div>
        <div class="col-md-3">
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours2" value="2"> 02<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours6" value="6"> 06<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours10" checked value="10"> 10<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours14" checked value="14"> 14<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours18" checked value="18"> 18<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours22" value="22"> 22<br>
        </div>
        <div class="col-md-3">
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours3" value="3"> 03<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours7" value="7"> 07<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours11" checked value="11"> 11<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours15" checked value="15"> 15<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours19" checked value="19"> 19<br>
            <input type="checkbox" name="num_of_hours[]" id="num_of_hours17" value="23"> 23<br>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="remarks">Remarks</label>
                <textarea class="form-control" id="remarks" rows="3" name="remarks"></textarea>
            </div>
        </div>


        <!--                                                      <div class="col-md-12">
                                                                    <button class="btn btn-info btn-flat">SUBMIT</button>
                                                              </div>-->
    </div>

    <hr class="mb-4">
    <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to create</button>
    <?php echo e(csrf_field()); ?>

    

    <?php echo Form::close(); ?>

</div>
   
    <div class="col-md-8">
          <table id="example2" class="table table-bordered table-hover dataTable"
                role="grid" aria-describedby="example2_info">
                <thead>
                      <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example2"
                                  rowspan="1" colspan="1" aria-sort="ascending" aria-label="LINE NUMBER">LINE
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                  colspan="1" aria-label="BUYER NAME">BUYER
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                  colspan="1" aria-label="STYLE NAME">STYLE
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                  colspan="1" aria-label="Hours">Hours
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                  colspan="1" aria-label="TODAYS TARGET">TODAY'S TARGET
                            </th>
                            <th>ACTION</th>
                      </tr>
                </thead>
                <tbody>
                      <?php if(count($totalTarget) && !empty($totalTarget)): ?>
                            <?php $__currentLoopData = $totalTarget; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <tr role="row">
                                        <td class="sorting_1">LINE <?php echo e($tt->line_no); ?></td>
                                        <td><?php echo e(@$tt->buyer_name); ?></td>
                                        <td><?php echo e(@$tt->style_name); ?></td>
                                        <td><?php echo e($tt->num_of_hours); ?></td>
                                        <td><?php echo e($tt->todays_target); ?></td>
                                        <td class="text-center">
                                            <a href="<?php echo e(route('superadmin.target.addHour', $tt->id)); ?>">
                                                 Add Hour
                                            </a>
                                            &nbsp;&nbsp;&nbsp;
                                            <a href="<?php echo e(route('superadmin.target.removeHour', $tt->id)); ?>">
                                                  Update hour
                                            </a>
                                        </td>

                                  </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>
                </tbody>
          </table>
    </div>
                                              


</div>

<script type='text/javascript'>
    $('#date').datepicker({
        format: 'yyyy-mm-dd',
    });


    function updateCheckBox(opts) {
        var chks = document.getElementsByName("num_of_hours[]");
        for (var i = 0; i <= chks.length - 1; i++) {
            chks[i].checked = false;

        }

        if (opts.value === '1') {
            for (var i = 8; i <= 21; i++) {
                $('#num_of_hours' + i).prop('checked', true);

            }
            $('#num_of_hours13').prop('checked', false);
        } else if (opts.value === '2') {
            for (var i = 8; i <= 13; i++) {
                $('#num_of_hours' + i).prop('checked', true);

            }
        } else if (opts.value === '3') {
            for (var i = 14; i <= 21; i++) {
                $('#num_of_hours' + i).prop('checked', true);

            }
        } else if (opts.value === '4') {
            for (var i = 20; i <= 21; i++) {
                $('#num_of_hours' + i).prop('checked', true);

            }
        }

    }
</script>


<style>
    input[type="checkbox"], input[type="radio"] {
        margin-right: 3px !important;
    }
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('my_js'); ?>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.appinside', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>