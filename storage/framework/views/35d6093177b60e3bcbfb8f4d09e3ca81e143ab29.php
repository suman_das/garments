<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(route('superadmin.dashboard')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('superadmin.machinedata.index')); ?>">Machine Data</a></li>
    <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo e(route('superadmin.machinedata.create')); ?>">New Machine Data</a></li>
  </ol>
</nav>


<?php echo $__env->make('partials.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<h1 class="page-title d-flex justify-content-center"> Machine Data <small></small> </h1>
<div class="portlet mb-3">
        <div class="portlet-title">
            <div class="caption" style="text-align: center">
                <i class="fa fa-gift"></i>Add New Machine record </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
    </div>
<div class="col-md-12 d-flex justify-content-center">
    
<?php echo Form::open(array('route' => 'superadmin.machinedata.store', 'method' => 'post','class'=>'needs-validation col-md-9','id'=>'createForm')); ?>

            <div class="portlet mb-3">
        <div class="portlet-title">
            <div class="caption" style="text-align: center">
                <i class="fa fa-gift"></i>Add New Machine Record </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
    </div>

            <div class="mb-3">
              <label for="rented_machine">Rented Machine in Factory :</label>
              <?php echo Form::text('rented_machine', 0, ['class' => 'form-control', 'required' => 'required' ,'id'=>'rented_machine']); ?>

            </div>
            <div class="mb-3">
              <label for="idle_machine">Idle Machine Outside Line :</label>
              <?php echo Form::text('idle_machine', 0, ['class' => 'form-control', 'placeholder' => 'Marketing :', 'required' => 'required','id'=>'idle_machine']); ?>

            </div>
            <div class="mb-3">
              <label for="used_machine">Used Machine in Sewing Line :</label>
              <?php echo Form::text('used_machine', 0, ['class' => 'form-control', 'placeholder' => 'MERCHANDISER :', 'required' => 'required','id'=>'used_machine']); ?>

            </div>
           
<div class="row clearfix">
    <div class="col-md-12 mb-3 pr-3">
              <label for="pl">Sample + Others:</label>
             
            </div>
    
            <div class="col-md-2 mb-3 pr-5">
              <label for="sample_section">Sample Section :</label>
              <?php echo Form::text('sample_section', 0, ['class' => 'form-control', 'placeholder' => 'Sample Section', 'required' => 'required','id'=>'sample_section']); ?>

            </div>
            <div class="col-md-2 mb-3 pr-5">
              <label for="finishing_section">Finishing Section :</label>
              <?php echo Form::text('finishing_section', 0, ['class' => 'form-control', 'placeholder' => 'Finishing Section', 'required' => 'required','id'=>'finishing_section']); ?>

            </div>
            <div class="col-md-2 mb-3 pr-5">
              <label for="cutting_section">Cutting Section :</label>
              <?php echo Form::text('cutting_section', 0, ['class' => 'form-control', 'placeholder' => 'Cutting Section', 'required' => 'required','id'=>'cutting_section']); ?>

            </div>
            <div class="col-md-2 mb-3 pr-5">
              <label for="power">Power :</label>
              <?php echo Form::text('power', 0, ['class' => 'form-control', 'placeholder' => 'power', 'required' => 'required','id'=>'power']); ?>

            </div>
            <div class="col-md-2 mb-3 pr-5">
              <label for="mvc_godwon">MVC Godown :</label>
              <?php echo Form::text('mvc_godwon', 0, ['class' => 'form-control', 'placeholder' => 'mvc_godwon', 'required' => 'required','id'=>'mvc_godwon']); ?>

            </div>
</div>

            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to create</button>
            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="query" value="<?php echo e(time()); ?>" />
            
            <?php echo Form::close(); ?>


        
    </div>

<script type='text/javascript'>
    $('#date').datepicker({
    format: 'yyyy-mm-dd',
});
</script>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('my_js'); ?>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.appinside', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>