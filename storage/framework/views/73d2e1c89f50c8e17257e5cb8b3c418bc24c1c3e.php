<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(route('superadmin.dashboard')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('superadmin.manpower.index')); ?>">Library</a></li>
    <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo e(route('superadmin.manpower.edit', $manpower->id)); ?>">New Manpower</a></li>
  </ol>
</nav>


<?php echo $__env->make('partials.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<h1 class="page-title d-flex justify-content-center"> Traders Management <small></small> </h1>
<div class="portlet mb-3">
        <div class="portlet-title">
            <div class="caption" style="text-align: center">
                <i class="fa fa-gift"></i>Add New Traders </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
    </div>
<div class="col-md-12 d-flex justify-content-center">
    
<?php echo Form::model($manpower,array('route' => ['superadmin.manpower.update',$manpower->id], 'method' => 'post','class'=>'needs-validation col-md-8','id'=>'createForm')); ?>

            <div class="portlet mb-3">
        <div class="portlet-title">
            <div class="caption" style="text-align: center">
                <i class="fa fa-gift"></i>update manpower </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
    </div>
            <div class="mb-3">
              <label for="p_operator">Present Operator :</label>
              <?php echo Form::text('p_operator', null, ['class' => 'form-control', 'placeholder' => 'Present Operator', 'required' => 'required' ,'id'=>'p_operator']); ?>

            </div>
            <div class="mb-3">
              <label for="p_swing_iron">Present Sewing Helper & Iron Man :</label>
              <?php echo Form::text('p_swing_iron', null, ['class' => 'form-control', 'placeholder' => 'Present Sewing Helper & Iron Man :', 'required' => 'required','id'=>'p_swing_iron']); ?>

            </div>
            <div class="mb-3">
              <label for="p_lc_sv">Present L/C & S/V :</label>
              <?php echo Form::text('p_lc_sv', null, ['class' => 'form-control', 'placeholder' => 'Present L/C & S/V :', 'required' => 'required','id'=>'p_lc_sv']); ?>

            </div>
            <div class="mb-3">
              <label for="username">Present L/C & S/V :</label>
              <?php echo Form::text('p_lc_sv', null, ['class' => 'form-control', 'placeholder' => 'Present L/C & S/V :', 'required' => 'required','id'=>'p_operator']); ?>

            </div>
            <div class="mb-3">
              <label for="unit">Unit:</label>
              <?php echo Form::select('unit', array('CGL' => 'CGL','LILI' => 'LILY'), null, ['class' => 'form-control js-example-basic-single', 'required' => 'required','id'=>'unit']); ?>

            </div>
            <div class="mb-3">
              <label for="date">Date:</label>
              <?php echo Form::text('date', null, ['class' => 'form-control', 'placeholder' => 'date:', 'required' => 'required','id'=>'date']); ?>

            </div>
            
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to update</button>
            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="query" value="<?php echo e(time()); ?>" />
            
            <?php echo Form::close(); ?>


        
    </div>

<script type='text/javascript'>
    $('#date').datepicker({
    format: 'yyyy-mm-dd',
});
</script>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('my_js'); ?>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.appinside', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>