<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(route('superadmin.dashboard')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('superadmin.order.index')); ?>">Order</a></li>
    <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo e(route('superadmin.order.edit', $manpower->id)); ?>">Update Order</a></li>
  </ol>
</nav>


<?php echo $__env->make('partials.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<h1 class="page-title d-flex justify-content-center"> Order Management <small></small> </h1>
<div class="portlet mb-3">
        <div class="portlet-title">
            <div class="caption" style="text-align: center">
                <i class="fa fa-gift"></i>Update New Order </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
    </div>
<div class="col-md-12 d-flex justify-content-center">
    
<?php echo Form::model($manpower,array('route' => ['superadmin.order.update',$manpower->id], 'method' => 'post','class'=>'needs-validation col-md-8','id'=>'createForm')); ?>

            <div class="portlet mb-3">
        <div class="portlet-title">
            <div class="caption" style="text-align: center">
                <i class="fa fa-gift"></i>update manpower </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
    </div>
            <div class="mb-3">
              <label for="buyer_name">Buyer :</label>
              <?php echo Form::text('buyer_name', null, ['class' => 'form-control', 'placeholder' => 'Buyer Name', 'required' => 'required' ,'id'=>'buyer_name']); ?>

            </div>
            <div class="mb-3">
              <label for="style">Style/ Buyer Refference :</label>
              <?php echo Form::text('style', null, ['class' => 'form-control', 'placeholder' => 'Style/ Buyer Refference :', 'required' => 'required','id'=>'style']); ?>

            </div>
            <div class="mb-3">
              <label for="model">Model :</label>
              <?php echo Form::text('model', null, ['class' => 'form-control', 'placeholder' => 'Model :', 'required' => 'required','id'=>'model']); ?>

            </div>
            <div class="mb-3">
              <label for="item">Item :</label>
              <?php echo Form::text('item', null, ['class' => 'form-control', 'placeholder' => 'Item', 'required' => 'required','id'=>'item']); ?>

            </div>
            <div class="mb-3">
              <label for="unit">Unit:</label>
              <?php echo Form::select('unit', array('CGL' => 'CGL','LILI' => 'LILY'), null, ['class' => 'form-control js-example-basic-single', 'required' => 'required','id'=>'unit']); ?>

            </div>
            <div class="mb-3">
              <label for="date">Start Date:</label>
              <?php echo Form::text('start_date', null, ['class' => 'form-control', 'placeholder' => 'date:', 'required' => 'required','id'=>'date']); ?>

            </div>
            
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to update</button>
            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="query" value="<?php echo e(time()); ?>" />
            
            <?php echo Form::close(); ?>


        
    </div>

<script type='text/javascript'>
    $('#date').datepicker({
    format: 'yyyy-mm-dd',
});
</script>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('my_js'); ?>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.appinside', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>