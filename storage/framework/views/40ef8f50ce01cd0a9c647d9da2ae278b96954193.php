<?php $__env->startSection('content'); ?>

<div id="app">
<div class="page parallel">
    <div class="d-flex row">
        <div class="col-md-4 white d-table h-100vh">
            <div class="d-table-cell v-middle">
                <div class="p-5 mt-5">
                    <img src="<?php echo e(asset('assets/img/basic/logo.png')); ?>" alt=""/>
                </div>
                <div class="p-5">
                    <h3>Welcome Back</h3>
                    <p>Hey, welcome back signin now there is lot of
                        new stuff waiting
                        for you</p>
                    <form action="<?php echo e(url('/login')); ?>" class="login-form" method="post">
                        <?php echo e(csrf_field()); ?>

                        
                        <?php if($errors->has('email')): ?>
                        <div class="alert alert-danger">
                            <button class="close" data-close="alert"></button>
                            <span><?php echo e($errors->first('email')); ?></span>
                        </div>
                        <?php endif; ?>
                        <?php if($errors->has('password')): ?>
                        <div class="alert alert-danger">
                            <button class="close" data-close="alert"></button>
                            <span><?php echo e($errors->first('password')); ?></span>
                        </div>
                        <?php endif; ?>
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Username</label>
                            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" value="<?php echo e(old('email')); ?>" required /> </div>
                            <div class="form-group">
                                <label class="control-label visible-ie8 visible-ie9">Password</label>
                                <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" value="<?php echo e(old('password')); ?>" required /> </div>

                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Login</button>
                                    <label class="rememberme check mt-checkbox mt-checkbox-outline">
                                        
                                    </label>
                                    
                                </div>

                        </form>
                </div>
            </div>
        </div>
        <div class="col-md-8  height-full blue accent-3 align-self-center text-center" data-bg-repeat="false"
             data-bg-possition="center"
             style="background: url('assets/img/basic/bg.jpeg') #FFEFE4">
        </div>
    </div>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>