@extends('master')
@section('content')

<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Tables</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Datatables</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <!-- <h1 class="page-title"> Ajax Datatables
        <small>basic datatable samples</small>
    </h1> -->
    
    

    @include('simpleRest.'.$dt['theme'].'.messages')

    <div class="row"> 
        <div class="col-md-12">
            <?php 
            $dt['links']=array_reverse($dt['links']);   // as we are using pull-right class below
            foreach($dt['links'] as $link){
                echo '<a class="btn btn-success pull-right" style="margin:15px;" href="'.$link[1].'"><i class="'.$link[2].'"></i> '.$link[0].'</a>';
            }
            ?>
        </div>
    </div>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->

    <div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">{{ $dt['title'] }} List</span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                </div>
                <div class="btn-group">
                </div>
            </div>
        </div>
        
    @include('simpleRest.'.$dt['theme'].'.datatable')

    


        <!-- /.col-sm-6 -->
        </div>
        <!-- /.row -->
     </div>
    </div>
</div>
 @endsection
