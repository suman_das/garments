<style>
.portlet.light .dataTables_wrapper .dt-buttons {
    margin-top:0px;
}
.searchZone{
	padding-top:15px;
	padding-left:15px;
	margin-bottom:15px;
    border-bottom:1px dashed #DDD;
}
#col_action{
    min-width:190px;
}
</style>


<!-- Datatable JS -->
<script type="text/javascript">
    $(function() {
        var dtUrl='datatableJsonData/{{$resourceBase}}';
        <?php
            $dtColumns=array();
            foreach($dt['fields'] as $f){
                if($f['fieldType']=="fileUpload" || $f['fieldType']=="checkbox"){
                    $tempArray=array();
                    $tempArray['data']=$f['fieldId'];
                    $tempArray['visible']=false;
                    $dtColumns[]=$tempArray;
                }
                else{
                    $dtColumns[]['data']=$f['fieldId'];
                }
            }
        ?>
        var dtColumns=<?php echo json_encode($dtColumns); ?>;

        var dtTable=$('#dt-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                    "url": dtUrl, // http://localhost/.../public/datatableJsonData/...
                    //"contentType": "application/json",
                    "type": "POST",
                    "data": function( d ){
                        var frm_data=$('#searchZoneForm').serializeArray();
                        $.each(frm_data, function(key, val){
                            d[val.name] = val.value;
                        });
                    }
            },
            columns: dtColumns,
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: ''
                }
            },

            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    dtColumnsCurrent=dtColumns[column.index()];	//console.log('XXX');
                    // var input = document.createElement("input");
                    // // $(input).appendTo($(column.header()).empty())
                    // $(input).appendTo($(column.footer()).empty())
                    // .on('change', function () {
                    //     column.search($(this).val(), false, false, true).draw();
                    // });
                    // --- Auto Load for select ---
					if( dtColumnsCurrent['fieldType']=="select" ){
						column.data().unique().sort().each( function ( d, j ) {//console.log('d:'+d+',j:'+j);
							$('#'+dtColumnsCurrent['data']).append( '<option value="'+d+'">'+d+'</option>' )
						} );
					}
                    // --- Filter On Change ---
                        // $('#'+dtColumnsCurrent['data']).on( 'change', function () {
                        //     val = $.fn.dataTable.util.escapeRegex(
                        //         $(this).val()
                        //     );console.log('X'+val+'X');
                        //     column.search( val ? ''+val+'' : '', true, false );//.draw();
                        // });	
                    // ----------- Draw Table On Click ----------
                    // $('#dtSearch').on('click',function(){
                    //     dtTable.draw();
                    // });
                });
                // ----------- Draw Table On Click ----------
                $('#dtSearch').on('click',function(){
                    dtTable.draw();
                });
            },
            "dom": 'Blrtip',
            buttons: [
                'excel',
                'pdf',
                'print'
            ],
        });
        
    });
 </script>


<div class="row">
    <div class="col-md-12">
        <!-- <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">{{ $dt['title'] }} List</span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                </div>
                <div class="btn-group">
                </div>
            </div>
        </div> -->
        

        <!-- Datatable Search HTML -->
        <form id="searchZoneForm">{{ csrf_field() }}
        <div class="searchZone">
        @php $i=0;	$itemCount=0; @endphp
        @foreach($dt['fields'] as $th)
            @php
            if(@$th['fieldType']=="" || $th['fieldType']=="fileUpload"){
                // skip, do nothing
            }
            else {
                if($itemCount%2==0){
                    @endphp
                    <div class="form-group row">
                    @php
                }
                @endphp
                        <div class="col-sm-1" id="s_t_{{$i}}">{{$th['label']}}</div>
                        <div class="col-md-4" id="s_v_{{$i}}">
                            @if ( @$th['fieldType']=="select" || @$th['fieldType']=="radio" )
                                <select  name="{{$th['fieldId']}}" id="{{$th['fieldId']}}" class="form-control form-control-lg">
                                    <?php 
                                        echo '<option value=""></option>';
                                        foreach($selectArray[ $th['fieldId'] ] as $s){
                                            echo '<option value="'.$s->id.'">'.$s->optionText.'</option>';
                                            // echo '<option value="'.$s->optionText.'">'.$s->optionText.'</option>';
                                        }
                                    ?>
                                </select>
                            @elseif ( @$th['fieldType']=="datepicker" )
                                <input type="text" name="{{$th['fieldId']}}" id="{{$th['fieldId']}}" class="form-control datepicker-here" data-language="en" data-date-format="yyyy-mm-dd" data-position="top left"  />
                            @elseif ( @$th['fieldType']=="datetimepicker" )
                                <input type="text" name="{{$th['fieldId']}}" id="{{$th['fieldId']}}" class="form-control datepicker-here" data-language="en" data-timepicker="true" data-date-format="yyyy-mm-dd" data-time-format="hh:ii:00" data-position="top left"  />
                            @elseif ( @$th['fieldType']=="checkboxSingle" )
                                <select  name="{{$th['fieldId']}}" id="{{$th['fieldId']}}" class="form-control form-control-lg">
                                    <option value=""></option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            @else
                                <input type="text" name="{{$th['fieldId']}}" id="{{$th['fieldId']}}" class="form-control"  />
                            @endif
                        </div>
                        <div class="col-sm-1"></div>
                @php
                if($itemCount%2==1){
                    @endphp
                    </div>
                    @php
                }
                $itemCount++;
            }	
            $i++; 
            @endphp
        @endforeach
        @if($itemCount>0)
            @if($itemCount%2==1)
                <div class="col-md-1" id="s_t_{{$i}}">&nbsp;</div>
                <div class="col-md-1" id="s_v_{{$i}}">
                    <input type="button" value="Submit" name="dtSearch" id="dtSearch" class="dtSearch btn btn-success" />
                </div>
                <div class="col-md-4" id="">&nbsp;</div>
            </div>
            @else
            <div class="form-group row">
                <div class="col-md-7" id="">&nbsp;</div>
                <div class="col-md-1" id="">
                    <input type="button" value="Submit" name="dtSearch" id="dtSearch" class="dtSearch btn btn-success" />
                </div>
                <div class="col-md-4" id="">&nbsp;</div>
            </div>
            @endif
        @else
            <style>
                .searchZone{display:none;}
            </style>
        @endif
            <div class="form-group row">
                <div class="col-md-12" id="">&nbsp;</div>
            </div>
        <!-- /class="searchZone" -->
        <!-- <div class="clear"></div> -->
        </div>
        </form>
        
        <!-- Datatable Table HTML -->
        <!--<table class="table table-striped table-bordered table-hover table-checkable" id="users-table"> -->
        <!--     <table class="table  table-condensed" id="users-table"> -->
        <table id='dt-table' class="table table-bordered table-striped">
            <thead>
                <tr>
                    @foreach($dt['fields'] as $th)
                        <th id="col_{{$th['fieldId']}}">{{$th['label']}}</th>
                    @endforeach
                </tr>
            </thead>
            <tfoot>
                <tr>
                    @foreach($dt['fields'] as $th)
                        <th>{{$th['label']}}</th>
                    @endforeach
                </tr>
            </tfoot>
        </table>

    </div>
</div>