@extends('layouts.appinside')

@section('content')

@php
    $form_id="form_create";
@endphp

<script>
var FormValidation = function () {

    // basic validation
    var handleValidation_{{ $form_id }} = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var {{ $form_id }} = $('#{{ $form_id }}');
        var error1 = $('.alert-danger', {{ $form_id }} );
        var success1 = $('.alert-success', {{ $form_id }} );

        {{ $form_id }}.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            messages: {
                <?php
                foreach($dt['fields'] as $currentField){
                    if(isset($currentField['validation'])){
                        echo $currentField['fieldId'].': {';
                        $currentField['validation']=str_replace(" ","",$currentField['validation']);
                        $validation_array=explode('|',$currentField['validation']);
                        foreach($validation_array as $v){//echo "[$v]";
                            if($v=="required"){   
                                echo 'required: "This field is required" ,';  
                            }
                            else if(strpos($v,"min:")!==false)   { 
                                echo 'minlength: jQuery.validator.format("Min Length {0} ") ,'; 
                            }
                            else if(strpos($v,"max:")!==false){ 
                                echo 'maxlength: jQuery.validator.format("MAX Length {0} ") ,';  
                            }
                            else if($v=="integer"){   
                                echo 'number: "Please use full numbers only" ,';  
                            }
                            else {}
                        }
                        echo '},';
                    }
                } 
                ?>
            },
            rules: {
                <?php
                foreach($dt['fields'] as $currentField){
                    if(isset($currentField['validation'])){
                        echo $currentField['fieldId'].': {';
                        $currentField['validation']=str_replace(" ","",$currentField['validation']);
                        $validation_array=explode('|',$currentField['validation']);
                        foreach($validation_array as $v){//echo "[$v]";
                            if($v=="required")              {   echo "required: true,";  }
                            else if(strpos($v,"min:")!==false)   { 
                                echo "minlength: ".substr($v,strpos($v,":")+1).", "; 
                            }
                            else if(strpos($v,"max:")!==false){ 
                                echo "maxlength: ".substr($v,strpos($v,":")+1).", "; 
                            }
                            else if($v=="integer") { 
                                echo "number: true, "; 
                            }
                            else                    {}
                        }
                        echo '},';
                    }
                } 
                ?>
                // name: {
                //     minlength: 2,
                //     required: true
                // },
                // input_group: {
                //     email: true,
                //     required: true
                // },
                // email: {
                //     required: true,
                //     email: true
                // },
                // url: {
                //     required: true,
                //     url: true
                // },
                // number: {
                //     required: true,
                //     number: true
                // },
                // digits: {
                //     required: true,
                //     digits: true
                // },
                // creditcard: {
                //     required: true,
                //     creditcard: true
                // },
                // occupation: {
                //     minlength: 5,
                // },
                // select: {
                //     required: true
                // },
                // select_multi: {
                //     required: true,
                //     minlength: 1,
                //     maxlength: 3
                // }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            // errorPlacement: function (error, element) { // render error placement for each input type
            //     var cont = $(element).parent('.input-group');
            //     if (cont) {
            //         cont.after(error);
            //     } else {
            //         element.after(error);
            //     }
            // },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                //success1.show();
                //error1.hide();
                form.submit();
            }
        });

    }

    return {
        //main function to initiate the module
        init: function () {
            handleValidation_{{ $form_id }}();
        }

    };
}();

jQuery(document).ready(function() {
    FormValidation.init();
});
</script>



<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <!-- <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Edit {{ $dt['title'] }}</span>
            </li>
        </ul>
    </div> -->

    <div class="row">
        <div class="col-md-6 py-3 blue accent-4 text-white float-left">
            <h6 class="text-uppercase font-weight-light"><i class="icon-list font-dark"></i>Edit {{ $dt['title'] }} #{{ $row->id }}</h6>
        </div>
        <div class="col-md-6 py-2 blue accent-4 text-white text-md-right report-date_filter pr-3">
            &nbsp;
        </div>
    </div>

    @include('simpleRest.'.$dt['theme'].'.messages')

    <div class="row"> 
        <a class="btn btn-success float-sm-right" style="margin:15px;" href="{{ url($resourceBase) }}"><i class="fa fa-angle-double-left fa-lg"></i> Back</a>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <!--   <h1 class="page-title"> Material Design Form Validation
        <small>material design form validation</small>
    </h1> -->
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <!--  <div class="note note-info">
        <p> A black page template with a minimal dependency assets to use as a base for any custom page you create </p>
    </div> -->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered">
                <div class="portlet-title">
                    <!-- <div class="caption">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">Edit {{ $dt['title'] }} #{{ $row->id }}</span>
                    </div> -->
                    <!-- <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-cloud-upload"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-wrench"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-trash"></i>
                        </a>
                    </div> -->
                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{ url($resourceBase.'/'.$row['id']) }}" class="form-horizontal" id="{{ $form_id }}"  enctype="multipart/form-data" method="POST">
                    {{csrf_field()}}
                    <input name="_method" type="hidden" value="PATCH">

                        <div class="form-body">
                            <div class="alert alert-danger d-none">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                            <div class="alert alert-success d-none">
                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                            <?php
                            foreach($dt['fields'] as $currentField){
                                if($currentField['fieldType']=="text"){
                                    echo '<div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="form_control_1">'.$currentField['label']
                                        .(in_array("required",explode('|',$currentField['validation'])) ? '<span class="required">*</span>' : '')
                                        .'</label>
                                        <div class="col-md-9">
                                            <input type="text" placeholder="" name="'.$currentField['fieldId'].'" value="'.$row[ $currentField['fieldId'] ].'" class="form-control" >
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block">Please enter '.$currentField['label'].'</span>
                                        </div>
                                    </div>';
                                }
                                else if($currentField['fieldType']=="textArea"){
                                    echo '<div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="form_control_1">'.$currentField['label']
                                        .(in_array("required",explode('|',$currentField['validation'])) ? '<span class="required">*</span>' : '')
                                        .'</label>
                                        <div class="col-md-9">
                                            <textarea class="form-control" placeholder="" name="'.$currentField['fieldId'].'" rows="3">'.$row[ $currentField['fieldId'] ].'</textarea>
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block">Please enter '.$currentField['label'].'</span>
                                        </div>
                                    </div>';
                                }
                                else if($currentField['fieldType']=="select"){
                                    echo '<div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="form_control_1">'.$currentField['label']
                                        .(in_array("required",explode('|',$currentField['validation'])) ? '<span class="required">*</span>' : '')
                                        .'</label>
                                        <div class="col-md-9">
                                            <select class="form-control" name="'.$currentField['fieldId'].'" >';
                                            foreach($selectArray[$currentField['fieldId']] as $s){
                                                // echo '<option value="'.$s->id.'" '.( $row[ $currentField['data'] ]==$s->optionText ?' selected ':'').' >'.$s->optionText.'</option>';
                                                echo '<option value="'.$s->id.'" '.( $row[ $currentField['fieldId'] ]==$s->optionText ?' selected ':'').' >'.$s->optionText.'</option>';
                                            }
                                            echo '</select>
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>';
                                }
                                else if($currentField['fieldType']=="radio"){
                                    echo '<div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="form_control_1">'.$currentField['label']
                                        .(in_array("required",explode('|',$currentField['validation'])) ? '<span class="required">*</span>' : '')
                                        .'</label>
                                        <div class="col-md-9">
                                            <div class="md-radio-list">';
                                            foreach($selectArray[$currentField['fieldId']] as $s){
                                                echo '<div class="md-radio">
                                                        <input type="radio" id="'.$currentField['fieldId'].'_'.$s->id.'" name="'.$currentField['fieldId'].'" value="'.$s->id.'" '.( $row[ $currentField['fieldId'] ]==$s->optionText ?' checked ':'').' class="md-radiobtn">
                                                        <label for="'.$currentField['fieldId'].'_'.$s->id.'">
                                                            <span></span>
                                                            <span class="check"></span>
                                                            <span class="box"></span> '.$s->optionText.'</label>
                                                    </div>';
                                            }
                                            echo '</div>
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>';
                                }
                                else if($currentField['fieldType']=="checkboxSingle"){
                                    echo '<div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="form_control_1">'.$currentField['label']
                                        .(in_array("required",explode('|',$currentField['validation'])) ? '<span class="required">*</span>' : '')
                                        .'</label>
                                        <div class="col-md-9">
                                            <div class="md-checkbox-list">
                                                <div class="md-checkbox">
                                                    <input type="checkbox" 
                                                        name="'.$currentField['fieldId'].'" 
                                                        id="'.$currentField['fieldId'].'" 
                                                        value="'.($row[ $currentField['fieldId'] ]=='yes'?1:0).'" '
                                                        .($row[ $currentField['fieldId'] ]=='yes'?' checked':' ')
                                                        .'  class="md-check" />
                                                    <label for="'.$currentField['fieldId'].'">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> &nbsp; </label>
                                                </div>
                                            </div>
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>';
                                }                                
                                else if($currentField['fieldType']=="checkbox"){
                                    $checkboxArray=explode(',',$row->{$currentField['fieldId']});
                                    echo '<div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="form_control_1">'.$currentField['label']
                                        .(in_array("required",explode('|',$currentField['validation'])) ? '<span class="required">*</span>' : '')
                                        .'</label>
                                        <div class="col-md-9">
                                            <div class="md-checkbox-list">';
                                            foreach($selectArray[$currentField['fieldId']] as $s){
                                                echo '<div class="md-checkbox">
                                                    <input type="checkbox" 
                                                        name="'.$currentField['fieldId'].'[]" 
                                                        id="'.$currentField['fieldId'].'_'.$s->id.'" 
                                                        value="'.$s->id.'" '
                                                        .( in_array($s->id,$checkboxArray) ?' checked ':'')
                                                        .'  class="md-check" />
                                                    <label for="'.$currentField['fieldId'].'_'.$s->id.'">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>'.$s->optionText.'</label>
                                                </div>';
                                            }
                                            echo '</div>
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>';
                                }
                                else if($currentField['fieldType']=="datepicker"){
                                    echo '<div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="form_control_1">'.$currentField['label']
                                        .(in_array("required",explode('|',$currentField['validation'])) ? '<span class="required">*</span>' : '')
                                        .'</label>
                                        <div class="col-md-9">
                                            <input type="text" placeholder="" name="'.$currentField['fieldId'].'" value="'.$row[ $currentField['fieldId'] ].'" class="form-control datepicker-here" data-language="en" data-date-format="yyyy-mm-dd" data-position="top left" >
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block">Please enter '.$currentField['label'].'</span>
                                        </div>
                                    </div>';
                                }
                                else if($currentField['fieldType']=="datetimepicker"){
                                    echo '<div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="form_control_1">'.$currentField['label']
                                        .(in_array("required",explode('|',$currentField['validation'])) ? '<span class="required">*</span>' : '')
                                        .'</label>
                                        <div class="col-md-9">
                                            <input type="text" placeholder="" name="'.$currentField['fieldId'].'" value="'.$row[ $currentField['fieldId'] ].'" class="form-control datepicker-here"  data-language="en" data-timepicker="true" data-date-format="yyyy-mm-dd" data-time-format="hh:ii:00" data-position="top left" >
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block">Please enter '.$currentField['label'].'</span>
                                        </div>
                                    </div>';
                                }
                                else if($currentField['fieldType']=="fileUpload"){
                                    echo '<div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="form_control_1">'.$currentField['label']
                                        .(in_array("required",explode('|',$currentField['validation'])) ? '<span class="required">*</span>' : '')
                                        .'</label>
                                        <div class="col-md-9">
                                            <input type="file" placeholder="" name="'.$currentField['fieldId'].'" value="'.$row[ $currentField['fieldId'] ].'" class="form-control" >
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block">Please select '.$currentField['label'].'</span>
                                        </div>
                                    </div>';
                                }
                                else{
                                    // die('<div class="row">
                                    //     <div class="col-md-12 simpleRestMessage alert alert-danger"> 
                                    //         <i class="fa fa-bell simpleRestMessageIcon"></i> Unknown fieldType '.$currentField['fieldType'].' in simpleRestConfig()
                                    //     </div>
                                    // </div>');
                                }
                            }
                            ?>

                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <Input type="submit" class="btn green" value="SUBMIT" />
                                    <button type="reset" class="btn default">Reset</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>

    </div>
    
</div>


@endsection