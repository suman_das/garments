<style type="text/css">
.simpleRestMessage{
    margin:10px 22px;
    padding: 10px 5px;
}
.simpleRestMessageIcon{
    font-size: 24px;
    vertical-align: middle;
}
</style>

<script>
$(document).ready(function(){
    $(".flashSuccess, .flashError").hide(0).fadeIn(500);
});
</script>

@if(Session::has('flashSuccess'))
    <div class="row">
        <div class="col-md-12 simpleRestMessage alert alert-success">
            <i class="fa fa-check-circle simpleRestMessageIcon"></i> {{ Session::get('flashSuccess') }}
        </div>
    </div>
@endif
@if(Session::has('flashError'))
    <div class="row">
        <div class="col-md-12 simpleRestMessage alert alert-danger"> 
            <i class="fa fa-bell simpleRestMessageIcon"></i> {{ Session::get('flashError') }}
        </div>
    </div>
@endif

@if (count($errors)>0)
    <div class="row">
        <div class="col-md-12 simpleRestMessage alert alert-danger">
            <ul id="errors">
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif