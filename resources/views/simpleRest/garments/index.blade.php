@extends('layouts.appinside')

@section('content')
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <!-- <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Tables</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Datatables</span>
            </li>
        </ul>
    </div> -->
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <!-- <h1 class="page-title"> Ajax Datatables
        <small>basic datatable samples</small>
    </h1> -->

    <div class="row">
        <div class="col-md-6 py-3 blue accent-4 text-white float-left">
            <h6 class="text-uppercase font-weight-light"><i class="icon-list font-dark"></i>{{ $dt['title'] }} List</h6>
        </div>
        <div class="col-md-6 py-2 blue accent-4 text-white text-md-right report-date_filter pr-3">
            &nbsp;
        </div>
    </div>
    
    @include('simpleRest.'.$dt['theme'].'.messages')

    <div class="row"> 
        <div class="col-md-12">
            <?php 
            $dt['links']=array_reverse($dt['links']);   // as we are using float-sm-right class below
            foreach($dt['links'] as $link){
                echo '<a class="btn btn-success float-sm-right" style="margin:15px;" href="'.$link[1].'"><i class="'.$link[2].'"></i> '.$link[0].'</a>';
            }
            ?>
        </div>
    </div>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->

    @include('simpleRest.'.$dt['theme'].'.datatable')

    


        <!-- /.col-sm-6 -->
        </div>
        <!-- /.row -->
     </div>
    </div>
</div>
 @endsection
