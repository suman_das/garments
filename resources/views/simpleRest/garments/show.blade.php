@extends('layouts.appinside')

@section('content')

<div class="page-content">
    <!-- BEGIN PAGE BAR -->
    <!-- <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
            <span>{{ $dt['title'] }} Details</span>
            </li>
        </ul>
    </div> -->

    <div class="row">
        <div class="col-md-6 py-3 blue accent-4 text-white float-left">
            <h6 class="text-uppercase font-weight-light"><i class="icon-list font-dark"></i>{{ $dt['title'] }} Details #{{ $row->id }}</h6>
        </div>
        <div class="col-md-6 py-2 blue accent-4 text-white text-md-right report-date_filter pr-3">
            &nbsp;
        </div>
    </div>

    @include('simpleRest.'.$dt['theme'].'.messages')
    
    <div class="row"> 
        <a class="btn btn-success float-sm-right" style="margin:15px;" href="{{ url($resourceBase) }}"><i class="fa fa-angle-double-left fa-lg"></i> Back</a>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <!--   <h1 class="page-title"> Material Design Form Validation
        <small>material design form validation</small>
    </h1> -->
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <!--  <div class="note note-info">
        <p> A black page template with a minimal dependency assets to use as a base for any custom page you create </p>
    </div> -->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered">
                <div class="portlet-title">
                    <!-- <div class="caption">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{ $dt['title'] }} Details #{{ $row->id }}</span>
                    </div> -->
                    <!-- <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-cloud-upload"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-wrench"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-trash"></i>
                        </a>
                    </div> -->
                </div>
                <div class="portlet-body">
                        <div class="form-body">
                            <div class="alert alert-danger d-none">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                            <div class="alert alert-success d-none">
                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>

                            <?php //dd($row);
                            foreach($dt['fields'] as $currentField){
                                if($currentField['fieldType']==""){
                                    /* do nothing */
                                }
                                else if($currentField['fieldType']=="fileUpload"){
                                    $ext=substr( $row->{$currentField['fieldId']}, strrpos($row->{$currentField['fieldId']},'.')+1 );
                                    if(in_array($ext,array('jpg','jpeg','gif','bmp'))){
                                        echo '<div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label" for="form_control_1">'.ucwords(str_replace('_',' ',$currentField['label']))
                                            .(in_array("required",explode('|',$currentField['validation'])) ? '<span class="required">*</span>' : '')
                                            .'</label>
                                            <div class="col-md-9">'
                                                .'<img src="../'.$row->{$currentField['fieldId']}.'" style="height:50px" alt="'.$row->{$currentField['fieldId']}.'" title="'.$row->{$currentField['fieldId']}.'" /> '.$row->{$currentField['fieldId']}.'
                                            </div>
                                        </div>';
                                    }
                                    else{
                                        echo '<div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="form_control_1">'.ucwords(str_replace('_',' ',$currentField['label']))
                                        .(in_array("required",explode('|',$currentField['validation'])) ? '<span class="required">*</span>' : '')
                                        .'</label>
                                        <div class="col-md-9">
                                            <div style="border-bottom: 1px dashed #c2cad8; padding-bottom:5px;">
                                                '.$row->{$currentField['fieldId']}.' &nbsp; <a href="../'.$row->{$currentField['fieldId']}.'" target="_blank" style="border: 0px solid"><i class="fa fa-lg fa-external-link"></i></a>
                                            </div>
                                        </div>
                                    </div>';
                                    }
                                }
                                else if($currentField['fieldType']=="checkbox"){
                                    $checkboxArray=explode(',',$row->{$currentField['fieldId']});
                                    $value='';
                                    foreach($selectArray[$currentField['fieldId']] as $s){
                                        if(in_array($s->id,$checkboxArray)){
                                            $value.=','.$s->optionText;
                                        }
                                    }
                                    $value=trim($value,',');
                                    echo '<div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="form_control_1">'.ucwords(str_replace('_',' ',$currentField['label']))
                                        .(in_array("required",explode('|',$currentField['validation'])) ? '<span class="required">*</span>' : '')
                                        .'</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" placeholder="" value="'.$value.'" readonly >
                                        </div>
                                    </div>';
                                }
                                else{
                                    echo '<div class="form-group form-md-line-input">
                                        <label class="col-md-3 control-label" for="form_control_1">'.ucwords(str_replace('_',' ',$currentField['label']))
                                        .(in_array("required",explode('|',$currentField['validation'])) ? '<span class="required">*</span>' : '')
                                        .'</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" placeholder="" value="'.$row->{$currentField['fieldId']}.'" readonly >
                                        </div>
                                    </div>';
                                }
                            }
                            ?>
                        </div>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>

    </div>
    
</div>


@endsection