@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="{{route('superadmin.registration.participants.index')}}">Registration 1424</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Participant List</span>
      </li>
    </ul>
  </div>

  <h1 class="page-title"> Details of Participant </h1>

      <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Details of Participant
        </div>
        <div class="tools">

            <button
              type="button"
              class="btn btn-info pull-right"
              onclick="printJS({
                  printable: 'example0',
                  type: 'html',
                  header: 'Shera Radhuni-1424 Participant Information - {{{ $participant->id }}}' })">
                Print
            </button>


        </div>
      </div>

      <div class="portlet light tasks-widget bordered">
        <div class="portlet-body util-btn-margin-bottom-5">

           <div class="table-responsive">

            <table class="table table-bordered table-hover table-condensed" id="example0">

              <tr><th nowrap="nowrap">Audition Zone</th><td>{{$participant->audition_zone}}</td></tr>
              <tr><th nowrap="nowrap">Name</th><td>{{$participant->name}}</td></tr>
              <tr><th nowrap="nowrap">Age</th><td>{{$participant->age}}</td></tr>
              <tr><th nowrap="nowrap">Date of Birth</th><td>{{$participant->dob}}</td></tr>

              <tr><th nowrap="nowrap">Address</th><td>{{$participant->address}}</td></tr>
              <tr><th nowrap="nowrap">Mobile</th><td>{{$participant->mobile}}</td></tr>
              <tr><th nowrap="nowrap">Phone Home</th><td>{{$participant->phone_home}}</td></tr>
              <tr><th nowrap="nowrap">Occupation</th><td>{{$participant->occupation}}</td></tr>

              <tr><th nowrap="nowrap">Email</th><td>{{$participant->email}}</td></tr>
              <tr><th nowrap="nowrap">NID / Passport No / Birth Certificate</th><td>{{$participant->nid_pp_no_birth_cert}}</td></tr>
              <tr><th nowrap="nowrap">Previously Perticipated</th><td>{{$participant->previously_perticipated ? "Yes" : "No"}}</td></tr>
              <tr><th nowrap="nowrap">Year of Participation</th><td>{{$participant->year_of_participation}}</td></tr>

              <tr><th nowrap="nowrap">Registration From</th><td>{{$participant->type}}</td></tr>

              <tr>
                <th nowrap="nowrap">Participant Image 1</th>
                <td>
                  @if($participant->image1 !="")
                    <img

                      @if($participant->type == "mobile")
                        src="{{asset($participant->image1)}}"
                      @else
                        src="{{$participant->image1}}"
                      @endif
                        class="img response-img" width="250" />

                    <a
                      @if($participant->type == "mobile")
                        href="{{asset($participant->image1)}}"
                      @else
                        href="{{$participant->image1}}"
                      @endif class="btn btn-secondary" download>Download</a>
                  @endif
                </td>
              </tr>

              <tr><th nowrap="nowrap">Participant Image 2</th><td>
                @if($participant->image2 !="")
                  <img
                  @if($participant->type == "mobile")
                    src="{{asset($participant->image2)}}"
                  @else
                    src="{{$participant->image2}}"
                  @endif
                  class="img response-img" width="250" download/>


                  <a
                      @if($participant->type == "mobile")
                        href="{{asset($participant->image2)}}"
                      @else
                        href="{{$participant->image2}}"
                      @endif class="btn btn-secondary" download>Download</a>

                @endif </td>
              </tr>

              <tr><th nowrap="nowrap">Participant Image 3</th><td>
                @if($participant->image3 !="")
                  <img
                  @if($participant->type == "mobile")
                    src="{{asset($participant->image3)}}"
                  @else
                    src="{{$participant->image3}}"
                  @endif
                  class="img response-img" width="250" download/>

                    <a
                      @if($participant->type == "mobile")
                        href="{{asset($participant->image3)}}"
                      @else
                        href="{{$participant->image3}}"
                      @endif class="btn btn-secondary" download>Download</a>


                @endif </td>
              </tr>

              <tr><th nowrap="nowrap">Recipe Image </th><td>
                @if($participant->recipe1 !="")

                  <img
                  @if($participant->type == "mobile")
                    src="{{asset($participant->recipe1)}}"
                  @else
                    src="{{$participant->recipe1}}"
                  @endif
                  class="img response-img" width="250" download/>

                  <a
                      @if($participant->type == "mobile")
                        href="{{asset($participant->recipe1)}}"
                      @else
                        href="{{$participant->recipe1}}"
                      @endif class="btn btn-secondary" download>Download</a>


                @endif </td>
                </tr>

              <tr><th nowrap="nowrap">Cooking Process Image</th><td>
                @if($participant->recipe2 !="")
                  <img
                  @if($participant->type == "mobile")
                    src="{{asset($participant->recipe2)}}"
                  @else
                    src="{{$participant->recipe2}}"
                  @endif
                  class="img response-img" width="250" download />

                  <a
                      @if($participant->type == "mobile")
                        href="{{asset($participant->recipe2)}}"
                      @else
                        href="{{$participant->recipe2}}"
                      @endif class="btn btn-secondary" download>Download</a>


                @endif </td></tr>


              <tr><th nowrap="nowrap">Recipe Image </th><td>
                @if($participant->recipe3 !="")
                  <img
                  @if($participant->type == "mobile")
                    src="{{asset($participant->recipe3)}}"
                  @else
                    src="{{$participant->recipe3}}"
                  @endif
                  class="img response-img" width="250" download />

                   <a
                      @if($participant->type == "mobile")
                        href="{{asset($participant->recipe3)}}"
                      @else
                        href="{{$participant->recipe3}}"
                      @endif class="btn btn-secondary" download>Download</a>


                @endif </td></tr>

            </table>
          </div>


        </div>
      </div>


@endsection
