<div class="table-responsive">
  <table class="table table-bordered table-hover table-condensed" id="example0">
    <thead >
      <tr>
        <th>Audition</th>
        <th>name</th>
        <th>Age</th>
        <th>mobile</th>
        <th>Registered at</th>
        <th>Action</th>
      </tr>
    </thead>

    <tbody>
      @foreach($participants as $participant)
        <tr>
          <td>{{$participant->audition_zone}}</td>
          <td>{{$participant->name}}</td>
          <td>{{$participant->age}}</td>
          <td>{{$participant->mobile}}</td>
          <td>@if($participant->created_at != ""){{date('Y-m-d H:i:s', strtotime($participant->created_at))}}@endif</td>
          <td>
            <a class="btn btn-info" href="{{route('superadmin.registration.participants.show', $participant->id)}}"><i class="fa fa-eye"></i>Show</a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
{{$participants->appends($old)->links()}}