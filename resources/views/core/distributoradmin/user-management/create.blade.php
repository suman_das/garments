@extends('layouts.appinside')

@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('distributoradmin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{route('distributoradmin.retail-management.index')}}">Retail Management</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Add New</span>
        </li>
    </ul>
</div>

@include('partials.errors')

<h1 class="page-title"> Retail Management <small></small> </h1>

<div class="col-md-12">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Add New Retail </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

        <div class="portlet-body form">
            {!! Form::open(array('route' => 'distributoradmin.retail-management.store', 'method' => 'post','class'=>'form-horizontal','id'=>'createForm')) !!}

            {{csrf_field()}}
            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">

                <div class="row">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Name</label>
                                <div class="col-md-4">
                                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Shop Name</label>
                                <div class="col-md-4">
                                    {!! Form::text('shop_name', null, ['class' => 'form-control', 'placeholder' => 'Shop name', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Email</label>
                                <div class="col-md-4">
                                    {!! Form::email('email1', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Designation</label>
                                <div class="col-md-4">
                                    {!! Form::text('designation', null, ['class' => 'form-control', 'placeholder' => 'Designation', 'required' => 'required']) !!}

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Address</label>
                                <div class="col-md-4">
                                    {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Address', 'required' => 'required']) !!}

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">Contact No.</label>
                            <div class="col-md-4">
                                {!! Form::text('contact_no', null, ['class' => 'form-control', 'placeholder' => 'Contact Number', 'required' => 'required']) !!}

                            </div>
                        </div>
                    </div>
                </div>

                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">Division</label>
                            <div class="col-md-4">
                                {!! Form::select('division',array('' => 'Add Division')+$divisions,null, ['class' => 'form-control', 'required' => 'required', 'id' => 'division']) !!}
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">District</label>
                            <div class="col-md-4">
                                {!! Form::select('district',array('' => 'Add District'),null, ['class' => 'form-control', 'required' => 'required', 'id' => 'district']) !!}

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">Thana</label>
                            <div class="col-md-4">
                                {!! Form::text('thana',null, ['class' => 'form-control', 'placeholder' => 'Thana', 'required' => 'required']) !!}

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">Sales Persion</label>
                            <div class="col-md-4">
                                {!! Form::text('sales_persion', null, ['class' => 'form-control', 'placeholder' => 'Enter Sales Person', 'required' => 'required']) !!}

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">Distributor  Name:</label>
                            <div class="col-md-4">
                                <label class="control-label col-md-3">{!! $user->name ?: ' ' !!}</label>

                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-6">
                            <input type="submit" class="btn green" value="Add New Retailer" />
                        </div>
                    </div>
                </div>


            </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>

@endsection

@section('my_js')
<script type="text/javascript">
       $(document).ready(function () {
    $('#division').on('change', function() {
            var division=$(this).val();
            var csrftoken = $("#csrf-token").val();
            if(division==''){
                $('#district').attr('disabled', 'disabled');
            }else{
                $.getJSON('{{ route('superadmin.user-management.districtlist') }}?division='+division+'&_token='+csrftoken, function (data) {
                    $('select[name="district"]').empty();
                    $('select[name="district"]').append('<option value="">Select District</option>');
                    $.each(data, function(key, value) {
                        $('select[name="district"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                });
            }
        });
    });
</script>
@endsection