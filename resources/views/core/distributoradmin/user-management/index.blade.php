@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('distributoradmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Retail Management</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Retail List</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  <h1 class="page-title"> Retail<small></small> </h1>

  

  @if(count($users))

  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
      <div class="portlet-body util-btn-margin-bottom-5">

        <div class="table-responsive">
          <style>
            #example0 td {
              white-space: nowrap;
              font-size: 10px;
            }
            #example0 th {
              font-size: 10px;
            }
          </style>

          <table class="table table-bordered table-hover table-condensed" id="example0">
            <thead >
              <tr>
              <th>Shop Name</th>
              <th>Distributor Name</th>
              <th>Designation</th>
              <th>Address</th>

              <th>Contact No</th>
              <th>Retailer Code</th>
              <th>Customer Type</th>
              <th>Region</th>

              <th>Division</th>
              <th>District</th>
              <th>Thana</th>
              <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @foreach($users as $user)
             
                <tr>

                  <td>{{$user->shop_name}}</td>
                  <td>{{$user->name}}</td>
                  <td>{{$user->designation}} </td>
                  <td>{{ $user->address}}</td>

                  <td>{{$user->contact_no}} </td>
                  <td>{{$user->email}}</td>
                  <td>{{@$user->roles->first()->display_name}}</td>
                  <td>{{ @$user->region }}</td>
                  <td>{{$user->division}}</td>

                  <td>{{$user->district}}</td>
                  <td>{{$user->thana}}</td>
                  <td>
                    <div class="btn-group">
                        <a class="btn green" href="javascript:;" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-user"></i> Option
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{route('distributoradmin.retail-management.show', $user->id)}}">
                                <i class="fa fa-eye"></i>
                                Show </a>
                            </li>
                            <li>
                                <a href="{{route('distributoradmin.retail-management.edit', $user->id)}}">
                                <i class="fa fa-pencil"></i>
                                Edit </a>
                            </li>
                           

                        </ul>
                    </div>

                  </td>
                </tr>
              @endforeach
            </tbody>

            <tfoot></tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>



  @else
  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
      <p>
        No Data Found
      </p>
    </div>
  </div>
  @endif
@endsection


@section('my_js')

@endsection