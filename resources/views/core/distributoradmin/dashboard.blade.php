@extends('layouts.appinside')
@section('content')
<div class="row widget-row">
 
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-shopping-cart fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$totalActivation}}</div>
                        <div>Total Activation</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa icon-bar-chart fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$stockW}} </div>
                        <div>Total Stock</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 
  <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-compress fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$totalSell}}</div>
                        <div>Total Sale</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row widget-row">
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-inbox fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$totalModel}}</div>
                        <div>Total Model</div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection