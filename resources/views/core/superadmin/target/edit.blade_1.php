@extends('layouts.master-dashboard')
@section('title', 'Target Edit')
@section('body-class', 'layout-top-nav')
@section('style')
    <link rel="stylesheet" href="{{ asset('') }}plugins/morris/morris.css">
@endsection
@section('content')
      <div class="content-wrapper">
      
            <section class="container">
                  <div class="row">
                        <div class="col-md-12">
                              <header class="section-header">
                                    <h3>Line Target Update</h3>
                                    <h5>You can update only upcomming hour</h5>
                              </header>
                              @include('partials.status')
                        </div>
                        <div class="col-md-12">
                              <div class="box">
                                    <div class="box-header">
                                          <h3 class="box-title">Edit Target {{ $summary->target }} on hour of {{ $time }}</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                          <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                                <div class="row">
                                                    <div class="col-sm-6"></div>
                                                    <div class="col-sm-6"></div>
                                                </div>
                                                <div class="row">
                                                      {!! Form::open(['route' => ['target.update', $target->id]]) !!}
                                                            <div class="form-group col-md-12">
                                                                  {!! Form::text('updated_target', $summary->target, ['class' => 'form-control']) !!}
                                                            </div>
                                                            <div class="form-group col-md-12">
                                                                  <br>
                                                                  <button type="submit" class="btn btn-success">UPDATE</button>
                                                            </div>
                                                      </form>
                                                </div>
                                          </div>
                                          <!-- /.box-body -->
                                    </div>
                              </div>
                        </div>
            </section>
      </div>
        
@endsection


@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('') }}dist/js/demo.js"></script>
    <!-- page script -->
    <script>
        $(function () {
                "use strict";

                @php
                    if( !empty($req) )
                    {
                        foreach($req as $key => $val)
                        {
                            echo "
                            if( $('#".$key."').length > 0 )
                            {
                                    document.getElementById('".$key."').value = '".$val."';
                            }
                            ";
                        }
                    }
                @endphp
        });
    </script>
@endsection
