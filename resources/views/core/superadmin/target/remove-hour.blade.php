@extends('layouts.appinside')
@section('title', 'Target Edit')
@section('body-class', 'layout-top-nav')
@section('style')
    <link rel="stylesheet" href="{{ asset('') }}plugins/morris/morris.css">
@endsection
@section('content')
      <div class="content-wrapper">
      
            <section class="container">
                  <div class="row">
                        <div class="col-md-12">
                              <header class="section-header">
                                    <h3>Line Target Update</h3>
                                    <h5>You can remove additional target on upcomming hours where target is set.</h5>
                              </header>
                              
                        </div>
                        <div class="col-md-12">
                              
                              <div class="box">
                                    <div class="box-header">
                                          @if( count($summary) )
                                                <div class="col-md-12">
                                                      <h3 style="text-transform:uppercase;">Total Target : {{ $target->todays_target }}</h3>
                                                      <hr style="margin-bottom: 0;">
                                                </div>

                                          <div style="text-align:right;" class="row">
                                                      <div class="col-md-1">
                                                            Hour
                                                      </div>
                                                      <div class="col-md-1" style="text-align:left; border-left:1px solid #ccc;">
                                                            Target
                                                      </div>
                                                      <div class="col-md-1">&nbsp;</div>
                                                      <div class="col-md-1">
                                                            Hour
                                                      </div>
                                                      <div class="col-md-1" style="text-align:left; border-left:1px solid #ccc;">
                                                            Target
                                                      </div>
                                                      <div class="col-md-1">&nbsp;</div>
                                                      <div class="col-md-1">
                                                            Hour
                                                      </div>
                                                      <div class="col-md-1" style="text-align:left; border-left:1px solid #ccc;">
                                                            Target
                                                      </div>
                                                      <div class="col-md-1">&nbsp;</div>
                                                      <div class="col-md-1">
                                                            Hour
                                                      </div>
                                                      <div class="col-md-1" style="text-align:left; border-left:1px solid #ccc;">
                                                            Target
                                                      </div>
                                                      <div class="col-md-1">&nbsp;</div>

                                                      <div class="col-md-12">
                                                            <hr style="margin:0">
                                                      </div>
                                                      
                                                      @foreach( $summary as $sm )
                                                            <div class="col-md-1">
                                                                  {{ $sm->hour_no }}hr
                                                            </div>
                                                            <div class="col-md-1" style="text-align:left; border-left:1px solid #ccc;">
                                                                  {{ $sm->target }}
                                                            </div>
                                                            <div class="col-md-1">&nbsp;</div>
                                                      @endforeach
                                                </div>
                                          @endif

                                          <div class="col-md-12">
                                                <hr>
                                          </div>                                          
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body mt-3">
                                          
                                                
                                                      {!! Form::open(['class'=>'col-md-12','route' => ['superadmin.target.removeSingleHour', $target->id]]) !!}
                                                      <div class="row">
                                                       <div class="col-md-6">
                                                      <h3 class="box-title">Remove additional hour</h3>
                                                      </div>
                                                      
                                                            <div class="form-group col-md-3">
                                                                  {!! Form::select('additional_hour', $hours, null, ['class' => 'form-control', 'placeholder' => 'SELECT ADDITIONAL HOUR', 'required' => true]) !!}
                                                            </div>
                                                            <div class="form-group col-md-3">
                                                                  <br>
                                                                  <button type="submit" class="btn btn-success">UPDATE</button>
                                                            </div>
                                                      
                                                </div>
                                            {!! Form::close() !!}
                                          <!-- /.box-body -->
                                    </div>
                              </div>
                        </div>
            </section>
      </div>
        
@endsection


@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('') }}dist/js/demo.js"></script>
    <!-- page script -->
    <script>
        $(function () {
                "use strict";

                @php
                    if( !empty($req) )
                    {
                        foreach($req as $key => $val)
                        {
                            echo "
                            if( $('#".$key."').length > 0 )
                            {
                                    document.getElementById('".$key."').value = '".$val."';
                            }
                            ";
                        }
                    }
                @endphp
        });
    </script>
@endsection
