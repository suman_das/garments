@extends('layouts.appinside')

@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('superadmin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{route('superadmin.retailer-management.index')}}">User Management</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Add New</span>
        </li>
    </ul>
</div>

@include('partials.errors')

<h1 class="page-title"> User Management <small></small> </h1>

<div class="col-md-12">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Update User </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

        <div class="portlet-body form">
            {!! Form::model($user, array('route' => ['superadmin.retailer-management.update', $user->id], 'method' => 'POST','class'=>'form-horizontal')) !!}

            {{csrf_field()}}
            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">

                <div class="row">
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Distributor</label>
                                <div class="col-md-4">
                                    {!! Form::select('dealer_id',array('' => 'Add Distributor')+$distributors,$user->dealer_id, ['class' => 'form-control', 'required' => 'required', 'id' => 'distributor']) !!}

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" >
                            <div class="form-group" id="dealerinfo">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Name</label>
                                <div class="col-md-4">
                                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name', 'required' => 'required', 'readonly'=>'true']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Shop Name</label>
                                <div class="col-md-4">
                                    {!! Form::text('shop_name', null, ['class' => 'form-control', 'placeholder' => 'Shop name', 'required' => 'required', 'readonly'=>'true']) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Email</label>
                                <div class="col-md-4">
                                    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required' => 'required', 'readonly'=>'true']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Designation</label>
                                <div class="col-md-4">
                                    {!! Form::text('designation', null, ['class' => 'form-control', 'placeholder' => 'Designation', 'required' => 'required']) !!}

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Address</label>
                                <div class="col-md-4">
                                    {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Address', 'required' => 'required']) !!}

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">Contact No.</label>
                            <div class="col-md-4">
                                {!! Form::text('contact_no', null, ['class' => 'form-control', 'placeholder' => 'Contact Number', 'required' => 'required']) !!}

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-4">
                                {!! Form::select('status', array('1' => 'Active','0' => 'Inactive'), null, ['class' => 'form-control js-example-basic-single', 'required' => 'required']) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">Type</label>
                            <div class="col-md-4">
                                {!! Form::select('roles',$roles,@$user->roles->first()->id, ['class' => 'form-control js-example-basic-single js-country', 'required' => 'required', 'id' => 'id']) !!}

                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">Division</label>
                            <div class="col-md-4">
                                {!! Form::text('division', null, ['class' => 'form-control', 'placeholder' => 'Division', 'required' => 'required']) !!}
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">District</label>
                            <div class="col-md-4">
                                {!! Form::select('district',array('' => 'Add District')+$districts,$user->district, ['class' => 'form-control', 'required' => 'required', 'id' => 'district']) !!}

                            </div>
                        </div>
                    </div>
                </div>




                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">Thana</label>
                            <div class="col-md-4">
                                {!! Form::text('thana', null, ['class' => 'form-control', 'placeholder' => 'Thana', 'required' => 'required']) !!}

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3">Sales Persion</label>
                            <div class="col-md-4">
                                {!! Form::text('sales_persion', null, ['class' => 'form-control', 'placeholder' => 'Enter Sales Person', 'required' => 'required']) !!}

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-6">
                            <input type="submit" class="btn green" value="Update" />
                        </div>
                    </div>
                </div>


            </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>

@endsection

@section('my_js')

@endsection