@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Traders Management</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Traders List</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  <h1 class="page-title"> Traders List <small></small> </h1>

  

  @if(count($users))

  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
      <div class="portlet-body util-btn-margin-bottom-5">

        <div class="table-responsive">
          <style>
            #example0 td {
              white-space: nowrap;
              font-size: 10px;
            }
            #example0 th {
              font-size: 10px;
            }
          </style>

          <table class="table table-bordered table-hover table-condensed" id="example0">
            <thead >
              <tr>
              <th>Outlet Code</th>
              <th>Outlet Name</th>
              <th>Address</th>
              <th>Outlet Contact No</th>
              <th>Chaneel Name</th>
              <th>Route</th>
              <th>Territory</th>
              <th>PPO</th>
             
              </tr>
            </thead>

            <tbody>
              @foreach($users as $user)
             
                <tr>

                  <td>{{$user->outlet_code}}</td>
                  <td>{{$user->outlet_name}}</td>
                  <td>{{$user->address}} </td>
                  <td>{{ $user->outlet_contact}}</td>
                  <td>{{$user->channel_name}} </td>
                  <td>{{$user->route}}</td>
                  <td>{{ @$user->territory }}</td>
                  <td>{{$user->ppo}}</td>

                 
                </tr>
              @endforeach
            </tbody>

            <tfoot></tfoot>
          </table>
        </div>
          {{ $users->links() }}
      </div>
    </div>
  </div>



  @else
  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
      <p>
        No Data Found
      </p>
    </div>
  </div>
  @endif
@endsection


@section('my_js')

@endsection