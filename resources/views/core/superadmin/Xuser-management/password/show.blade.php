@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('superadmin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{route('superadmin.user-management.index')}}">User Management</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Add New</span>
    </li>
  </ul>
</div>

@include('partials.errors')

<h1 class="page-title"> User Management <small></small> </h1>

<div class="col-md-12">
  <div class="portlet box blue">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i>Update User </div>
        <div class="tools">
          <a href="javascript:;" class="collapse"> </a>
        </div>
      </div>

      <div class="portlet-body form">
        <form action="{{route('superadmin.user-management.password-change.post', $user->id)}}" class="form-horizontal" method="POST" id="createForm">
          {{csrf_field()}}
          <input type="hidden" name="action" value="{{time()}}" />

          <div class="form-body">

            <div class="row">

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Name</label>
                    <div class="col-md-4">
                      <input
                      type="text"
                      class="form-control "
                      value="{{$user->name}}"
                      required="required"
                      disabled
                       />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Mobile Number</label>
                    <div class="col-md-4">
                      <input
                      type="text"
                      class="form-control"
                      value="{{$user->email}}"
                      disabled
                      />
                    </div>
                  </div>
                </div>
              </div>

              
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">New Password</label>
                    <div class="col-md-4">
                      <input
                      type="password"
                      class="form-control"
                      name="password"
                      required="required"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Password Confirmation</label>
                    <div class="col-md-4">
                      <input
                      type="password"
                      class="form-control"
                      name="confirm_password"
                      required="required"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="form-group">
                  <div class="col-md-offset-1 col-md-6">
                    <input type="submit" class="btn green" value="Update Password" />
                  </div>
                </div>
              </div>


            </div>
          </form>

        </div>
      </div>
    </div>

    @endsection
