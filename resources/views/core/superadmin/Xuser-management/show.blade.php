@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="{{route('superadmin.user-management.index')}}">User Management</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Details of User</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  <h1 class="page-title"> User Details </h1>

  <div class="col-md-offset-1 col-md-10">
    <div class="portlet light tasks-widget bordered">
      <div class="portlet-body util-btn-margin-bottom-5">

        <div class="table-responsive">
          <style>
            #example0 td {
              white-space: nowrap;
            }
          </style>

          <table class="table table-bordered table-hover" id="example0">

              <tr><th>Name</th>   <td>{{$user->name}}</td> </tr>
              <tr><th>Email</th>  <td>{{$user->email1}}</td> </tr>
              <tr><th>Address</th><td>{{$user->address}}</td></tr>
              <tr><th>Contact Number</th><td>{{$user->contact_no}}</td></tr>
              <tr><th>Created At</th><td>{{date('Y-m-d H:i:s', strtotime($user->created_at) ) }}</td></tr>
              <tr><th>Created By</th><td>
                <?php
                  $created = "";

                  if($user->created_by != "")
                  {
                    $created = \DB::table('users')->where('id', $user->created_by)->get();
                    if( count($created) )
                    {
                      $created = $created->first()->name;
                    }
                  }

                ?>@if($created != "") {{$created}}@endif</td></tr>
              <tr><th>Updated At</th><td>{{date('Y-m-d H:i:s', strtotime( $user->updated_at) ) }}</td></tr>
              <tr><th>Updated By</th><td>
                <?php
                  $updated = "";

                  if($user->created_by != "")
                  {
                    $updated = \DB::table('users')->where('id', $user->updated_by)->get();
                    if( count($updated) )
                    {
                      $updated = $updated->first()->name;
                    }
                  }

                ?>@if($updated != "") {{$updated}}@endif</td></tr>

              <tr><th>Status</th>  <td>{{$user->status}}</td> </tr>
              <tr><th>Role</th>   <td>@if($user->roles != "") {{@$user->roles->first()->display_name}} @endif</td> </tr>
              <tr><th>Region</th> <td>@if($user->region != "") {{$user->region}} @endif</td> </tr>
              <tr><th>Division</th> <td>@if($user->division != "") {{$user->division}} @endif</td> </tr>
              <tr><th>District</th> <td>@if($user->district != "") {{$user->district}} @endif</td> </tr>
              <tr><th>Thana</th> <td>@if($user->thana != "") {{$user->thana}} @endif</td> </tr>
              <tr><th>Sales Person</th>   <td>@if($user->sales_persion != "") {{$user->sales_persion}} @endif</td> </tr>

              

          </table>
        </div>
      </div>
    </div>
  </div>




@endsection


