@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">User Management</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>User List</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  <h1 class="page-title"> User List <small> </small> </h1>

  

  @if(count($users))

  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
      <div class="portlet-body util-btn-margin-bottom-5">

        <div class="table-responsive">
          <style>
            #example0 td {
              white-space: nowrap;
              font-size: 10px;
            }
            #example0 th {
              font-size: 10px;
            }
          </style>

          <table class="table table-bordered table-hover table-condensed" id="example0">
            <thead >
              <tr>
              <th>PPO Name</th>
              <th>PPO Mobile</th>
              <th>Type</th>
              <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @foreach($users as $user)
             
                <tr>

                  <td>{{$user->name}}</td>
                  <td>{{$user->email}}</td>
                  <td>{{@$user->roles->first()->display_name}}</td>
                  <td>
                    <div class="btn-group">
                        <a class="btn green" href="javascript:;" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-user"></i> Option
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu" role="menu">

                            <li>
                                <a href="{{route('superadmin.user-management.password-change', $user->id)}}">
                                <i class="fa fa-key"></i>
                                Change Password </a>
                            </li>

                        </ul>
                    </div>

                  </td>
                </tr>
              @endforeach
            </tbody>

            <tfoot></tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>



  @else
  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
      <p>
        No Data Found
      </p>
    </div>
  </div>
  @endif
@endsection


@section('my_js')

@endsection