@extends('layouts.appinside')

@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('superadmin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{route('superadmin.user-management.index')}}">User Management</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Add New</span>
        </li>
    </ul>
</div>

@include('partials.errors')

<h1 class="page-title"> User Management <small></small> </h1>

<div class="col-md-12">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Add New User </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

        <div class="portlet-body form">
            {!! Form::open(array('route' => 'superadmin.user-management.store', 'method' => 'post','class'=>'form-horizontal','id'=>'createForm')) !!}

            {{csrf_field()}}
            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">

                <div class="row">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">PPO Name</label>
                                <div class="col-md-4">
                                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">PPO Mobile</label>
                                <div class="col-md-4">
                                    {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'PPO Mobile', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Password</label>
                                 <div class="col-md-4">
                                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'New Password', 'required' => 'required']) !!}
                            </div>
                                 </div>
                        </div>
                    </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-6">
                            <input type="submit" class="btn green" value="Add New User" />
                        </div>
                    </div>
                </div>


            </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>

@endsection

@section('my_js')

@endsection

