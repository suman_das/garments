@extends('layouts.appinside')

@section('content')

<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ URL::to('home') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ URL::to('createBulkDealer') }}">Bulk Code<small>Dealer Wise</small> </a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Insert</span>
        </li>
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Dealer Wise Product Upload
    <small></small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->

@include('partials.errors')
<div class="col-md-12">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <a href="{{ URL::asset('/uploads/excell/2017_11_06_09_53_04.xlsx') }}" class="btn btn-primary"> Download Sample File</a> </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

        <div class="portlet-body form">
            
            {!! Form::open(array('route' => 'superadmin.product-upload.dealerimeiinformation.upload', 'method' => 'post','class'=>'form-horizontal','id'=>'createForm','files' => true)) !!}

            {{csrf_field()}}
            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">

                <div class="row">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Attache File</label>
                                <div class="col-md-4">
                                    {!! Form::file('file', null, ['class' => 'form-control filament', 'placeholder' => 'Code file', 'required' => 'required','id'=>'photo']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-offset-1 col-md-6">
                                 {!! Form::submit('Upload', ['class' => 'btn green  center-block']) !!}
                            </div>
                        </div>
                    </div>


                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>






    

    <script type="text/javascript">

        $(document).ready(function () {
            // Navigation Highlight
            highlight_nav('upload-dealer', 'Dealer');


        });



    </script>

    @endsection
