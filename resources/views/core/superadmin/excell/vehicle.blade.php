@extends('layouts.appinside')

@section('content')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ URL::to('home') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ URL::to('createVehicle') }}">Bulk Vehicle Upload</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Insert</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Vehicle
        <small>create new Vehicle</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->

    <div class="mt-element-step">
        <div class="row step-background-thin">
            <div class="col-md-12 bg-grey-steel mt-step-col active">
                <div class="mt-step-number"></div>
                <div class="mt-step-title uppercase font-grey-cascade ">Bulk Upload</div>
                <div class="mt-step-content font-grey-cascade">Vehicle</div>
            </div>
           
        </div>

            {!! Form::open(array('url' => 'storeVehicle', 'method' => 'post','files' => true)) !!}

                <div class="row">

                    @include('partials.errors')
                    <div class="row padding-top-20"></div>
                    <div class="col-md-6 col-md-offset-3">

                        <div class="form-group">
                            <label class="control-label">Attache Vehicle Upload File</label>
                            {!! Form::file('file', null, ['class' => 'form-control', 'placeholder' => 'vehile file', 'required' => 'required','id'=>'photo']) !!}
                        </div>

                        
                        


                    </div>

                    

                </div>

                &nbsp;
                <div class="row padding-top-10">
<!--                    <a href="javascript:history.back()" class="btn default"> Cancel </a>-->
                    {!! Form::submit('Upload', ['class' => 'btn green  center-block']) !!}
                </div>

            {!! Form::close() !!}


    </div>

    <script type="text/javascript">

        $(document ).ready(function() {
            // Navigation Highlight
            highlight_nav('upload-bike','bike');

            
        });

        

    </script>

@endsection
