@extends('layouts.appinside')

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('superadmin.dashboard') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('superadmin.npt.index')}}">Department wise NPT mints</a></li>
    <li class="breadcrumb-item active" aria-current="page"><a href="{{route('superadmin.npt.create')}}">New NPT</a></li>
  </ol>
</nav>


@include('partials.errors')

<h1 class="page-title d-flex justify-content-center"> Department wise NPT mints <small></small> </h1>
<div class="portlet mb-3">
        <div class="portlet-title">
            <div class="caption" style="text-align: center">
                <i class="fa fa-gift"></i>Add New NPT </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
    </div>
<div class="col-md-12 d-flex justify-content-center">
    
{!! Form::open(array('route' => 'superadmin.npt.store', 'method' => 'post','class'=>'needs-validation col-md-9','id'=>'createForm')) !!}
            <div class="portlet mb-3">
        <div class="portlet-title">
            <div class="caption" style="text-align: center">
                <i class="fa fa-gift"></i>Add New NPT </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
    </div>
<div class="row clearfix">
            <div class="col-md-4 mb-3 pr-3">
              <label for="tch">Technical :</label>
              {!! Form::text('tch', 0, ['class' => 'form-control', 'required' => 'required' ,'id'=>'tch']) !!}
            </div>
            <div class="col-md-4 mb-3 pr-3">
              <label for="mr">Marketing :</label>
              {!! Form::text('mr', 0, ['class' => 'form-control', 'placeholder' => 'Marketing :', 'required' => 'required','id'=>'mr']) !!}
            </div>
            <div class="col-md-4 mb-3 pr-3">
              <label for="mch">MERCHANDISER :</label>
              {!! Form::text('mch', 0, ['class' => 'form-control', 'placeholder' => 'MERCHANDISER :', 'required' => 'required','id'=>'mch']) !!}
            </div>
</div>
<div class="row clearfix">
            <div class="col-md-4 mb-3 pr-3">
              <label for="cd">Cutting :</label>
              {!! Form::text('cd', 0, ['class' => 'form-control', 'placeholder' => 'Cutting', 'required' => 'required','id'=>'cd']) !!}
            </div>
            <div class="col-md-4 mb-3 pr-3">
              <label for="ped">PRINT & EMB :</label>
              {!! Form::text('ped', 0, ['class' => 'form-control', 'placeholder' => 'PRINT & EMB', 'required' => 'required','id'=>'ped']) !!}
            </div>
            <div class="col-md-4 mb-3 pr-3">
              <label for="od">Production :</label>
              {!! Form::text('pd', 0, ['class' => 'form-control', 'placeholder' => 'Production', 'required' => 'required','id'=>'pd']) !!}
            </div>
</div>
<div class="row clearfix">
            <div class="col-md-4 mb-3 pr-3">
              <label for="pl">PLANNING :</label>
              {!! Form::text('pl', 0, ['class' => 'form-control', 'placeholder' => 'PLANNING', 'required' => 'required','id'=>'pl']) !!}
            </div>
            <div class="col-md-4 mb-3 pr-3">
              <label for="qa">Quality :</label>
              {!! Form::text('qa', 0, ['class' => 'form-control', 'placeholder' => 'Quality', 'required' => 'required','id'=>'qa']) !!}
            </div>
            <div class="col-md-4 mb-3 pr-3">
              <label for="mb">Machanic :</label>
              {!! Form::text('mb', 0, ['class' => 'form-control', 'placeholder' => 'Machanic', 'required' => 'required','id'=>'mb']) !!}
            </div>
</div>
<div class="row clearfix">
            <div class="col-md-4 mb-3 pr-3">
              <label for="ad">Acces  Store :</label>
              {!! Form::text('ad', 0, ['class' => 'form-control', 'placeholder' => 'Acces  Store', 'required' => 'required','id'=>'as']) !!}
            </div>
            <div class="col-md-4 mb-3 pr-3">
              <label for="fd">Fabric      Store :</label>
              {!! Form::text('fd', 0, ['class' => 'form-control', 'placeholder' => 'Fabric      Store', 'required' => 'required','id'=>'fd']) !!}
            </div>
            <div class="col-md-4 mb-3 pr-3">
              <label for="pf">Power Failure :</label>
              {!! Form::text('pf', 0, ['class' => 'form-control', 'placeholder' => 'Power Failure', 'required' => 'required','id'=>'pf']) !!}
            </div>
</div>
<div class="row clearfix">
            <div class="col-md-4 mb-3 pr-3">
              <label for="vp">OTHERS :</label>
              {!! Form::text('vp', 0, ['class' => 'form-control', 'placeholder' => 'OTHERS', 'required' => 'required','id'=>'vp']) !!}
            </div>
            <div class="col-md-4 mb-3 pr-3">
              <label for="unit">Unit:</label>
              {!! Form::select('unit', array('CGL' => 'CGL','LILI' => 'LILY'), null, ['class' => 'form-control js-example-basic-single', 'required' => 'required','id'=>'unit']) !!}
            </div>
            <div class="col-md-4 mb-3 pr-3">
              <label for="unit">line No:</label>
              {!! Form::select('line', array('1' => '1','2' => '2','3' => '3','4' => '4','5' => '5','6' => '6','7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12','13' => '13','14' => '14','15' => '15','16' => '16'), null, ['class' => 'form-control js-example-basic-single', 'required' => 'required','id'=>'line']) !!}
            </div>
<!--            <div class="col-md-4 mb-3 pr-3">
              <label for="date">Start Date:</label>
              {!! Form::text('date', null, ['class' => 'form-control', 'placeholder' => 'date:', 'required' => 'required','id'=>'date']) !!}
            </div>-->
</div>
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to create</button>
            {{csrf_field()}}
            <input type="hidden" name="query" value="{{time()}}" />
            
            {!! Form::close() !!}

        
    </div>

<script type='text/javascript'>
    $('#date').datepicker({
    format: 'yyyy-mm-dd',
});
</script>


@endsection

@section('my_js')


@endsection

