@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Registration 1424</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Participant List</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  <h1 class="page-title"> Participant <small>Registered for 1424</small> </h1>

  @include('core.superadmin.registration.participants._search')

<div class="portlet box blue">
  <div class="portlet-title">
      <div class="caption">
          <i class="fa fa-gift"></i>Participant List</div>
      <div class="tools">
          <a href="javascript:;" class="collapse"> </a>
      </div>
  </div>

  <div class="portlet-body">
    @if(count($participants))
      @include('core.superadmin.registration.participants._table')
    @else
      <p>No Data Found</p>
    @endif
  </div>
</div>

@endsection
