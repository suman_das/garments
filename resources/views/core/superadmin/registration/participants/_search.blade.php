<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Query Form
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
        <form action="{{route('superadmin.registration.participants.index')}}" class="form-horizontal" method="GET" id="searchForm">

            {{csrf_field()}}

            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">

                <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Audition Zone</label>
                            <div class="col-md-9">

                                <select class="form-control" name="audition_zone">
                                <option value="">Choose One</option>
                                <option @if($old['audition_zone'] != "" && $old['audition_zone'] == "Dhaka") selected="selected" @endif value="Dhaka">Dhaka</option>
                                <option @if($old['audition_zone'] != "" && $old['audition_zone'] == "Chittagong") selected="selected" @endif value="Chittagong">Chittagong</option>
                                <option @if($old['audition_zone'] != "" && $old['audition_zone'] == "Khulna") selected="selected" @endif value="Khulna">Khulna</option>
                                <option @if($old['audition_zone'] != "" && $old['audition_zone'] == "Rajshahi") selected="selected" @endif value="Rajshahi">Rajshahi</option>
                                <option @if($old['audition_zone'] != "" && $old['audition_zone'] == "Rangpur") selected="selected" @endif value="Rangpur">Rangpur</option>
                                <option @if($old['audition_zone'] != "" && $old['audition_zone'] == "Bagura") selected="selected" @endif value="Bagura">Bagura</option>
                                <option @if($old['audition_zone'] != "" && $old['audition_zone'] == "Sylhet") selected="selected" @endif value="Sylhet">Sylhet</option>
                                <option @if($old['audition_zone'] != "" && $old['audition_zone'] == "Barishal") selected="selected" @endif value="Barishal">Barishal</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="name" @if(!empty( $old['name'])) value="{{$old['name']}}" @endif >
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label col-md-6">Age</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="age" @if(!empty( $old['age'])) value="{{$old['age']}}" @endif />
                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">DOB</label>
                            <div class="col-md-9">

                                <input
                                    type="date"
                                    class="form-control date-picker"
                                    name="dob"
                                    data-date-format="yyyy-mm-dd"
                                    readonly
                                    @if(!empty( $old['dob']))
                                        value="{{$old['dob']}}"
                                    @endif
                                />
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Address</label>
                            <div class="col-md-9">

                                <input
                                    type="text"
                                    class="form-control"
                                    name="address"
                                    placeholder="Address"
                                    @if(!empty( $old['address']))
                                        value="{{$old['address']}}"
                                    @endif
                                />

                            </div>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Mobile</label>
                            <div class="col-md-9">

                                <input
                                    type="text"
                                    class="form-control"
                                    name="mobile"
                                    placeholder="Mobile"

                                    @if(!empty( $old['mobile']))
                                        value="{{$old['mobile']}}"
                                    @endif
                                />

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Phone (Home)</label>
                            <div class="col-md-9">

                                <input
                                    type="text"
                                    class="form-control"
                                    name="phone_home"
                                    @if(!empty( $old['phone_home']))
                                        value="{{$old['phone_home']}}"
                                    @endif
                                />

                            </div>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Occupation</label>
                            <div class="col-md-9">

                                <input
                                    type="text"
                                    class="form-control"
                                    name="occupation"
                                    placeholder="Ex. Student, Job"

                                    @if(!empty( $old['occupation']))
                                        value="{{$old['occupation']}}"
                                    @endif
                                />

                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">NID/PP NO/Birth Certificate</label>
                            <div class="col-md-9">
                            <input
                                type="text"
                                class="form-control"
                                name="nid_pp_no_birth_cert"
                                @if(!empty( $old['nid_pp_no_birth_cert']))
                                    value="{{$old['nid_pp_no_birth_cert']}}"
                                @endif
                            />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Previously Perticipated</label>
                            <div class="col-md-9">
                            <select name="previously_perticipated" class="form-control">
                                <option value="">Choose One</option>
                                <option
                                    value="1"
                                    @if($old['previously_perticipated'] != "" && $old['previously_perticipated'] == 1)
                                        selected="selected"
                                    @endif >Yes</option>
                                <option
                                    value="0"
                                    @if($old['previously_perticipated'] != "" && $old['previously_perticipated'] == 0)
                                        selected="selected"
                                    @endif >No</option>
                            </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Starting Date</label>
                            <div class="col-md-9">
                                <input
                                    type="date"
                                    class="form-control date-picker"
                                    data-date-format="yyyy-mm-dd"
                                    readonly
                                    name="created_at_starting_date"
                                    @if(!empty( $old['created_at_starting_date']))
                                        value="{{$old['created_at_starting_date']}}"
                                    @endif
                                />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Ending Date</label>
                            <div class="col-md-9">
                                <input
                                    type="date"
                                    class="form-control date-picker"
                                    data-date-format="yyyy-mm-dd"
                                    readonly
                                    name="created_at_ending_date"
                                    @if(!empty( $old['created_at_ending_date']))
                                        value="{{$old['created_at_ending_date']}}"
                                    @endif
                                />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <input type="submit" class="btn green" value="Search" />
                        <a href="{{route('superadmin.registration.participants.index')}}" class="btn default">Clear</a>
                    </div>
                    <div class="col-md-6 ">
                        <a href="{{route('superadmin.registration.participants.download', $old)}}" class="btn btn-info pull-right">Download CSV</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>