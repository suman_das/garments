@extends('layouts.appinside')

@section('content')
<?php //echo $order->id;exit;?>
    <link href="{{ URL::asset('assets/pages/css/profile.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ URL::to('home') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ URL::to('superadmin/ProductDetails/view/'.$order->id) }}">Product</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>View</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Order Product View
        <small> view</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PROFILE SIDEBAR -->
            

                <!-- PORTLET MAIN -->
               
                    <!-- SIDEBAR USERPIC -->
                   
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle col-md-12">
                        <div class="profile-usertitle-name">Outlet Code: {{ $order->tradersinfo->outlet_code }} </div>
                        <div class="profile-usertitle-name"> Outlt Name:{{ $order->tradersinfo->outlet_name }} </div>
                        <div class="profile-usertitle-name"> Address:{{ $order->tradersinfo->address }} </div>
                        <div class="profile-usertitle-name"> Territory:{{ $order->tradersinfo->territory }} </div>
                        <div class="profile-usertitle-name"> PPO:{{ $order->tradersinfo->ppo }} </div>
                    </div>
                  
               
                <!-- END PORTLET MAIN -->
                
                 <table class="table table-bordered table-hover col-md-12" id="example0">
                     <tr>
                         <th>Product Name</th>
                         <th>Qty</th>
                         <th>Price</th>
                     </tr>
                     @foreach ($order->ordersinfo as $ordersinfo)
                        <tr>
                            <tD>{{$ordersinfo->productinfo->sku_name }}</tD> 
                            <tD>{{$ordersinfo->qty }}</tD> 
                            <td>{{$ordersinfo->price}}</td>
                        </tr>
                    @endforeach
                    

              

                </table>

          


        </div>
    </div>

    

@endsection
