<script src="{{ URL::asset('custom/js/reference-list.js') }}" type="text/javascript"></script>

{!! Form::model($user, array('url' => '/user/'.$user->id.'?step=3', 'method' => 'put')) !!}

    <div class="row">

        @include('partials.errors')

        <div class="col-md-6">
            
            <div class="form-group">
                <label class="control-label">Select Type</label>
                {!! Form::select('user_type_id', $user_types, null, ['class' => 'form-control js-example-basic-single js-country', 'required' => 'required', 'id' => 'user_type_id']) !!}
            </div>

            @if($reference_list!=null)
                <div class="form-group reference-area">
                    <label class="control-label">Select Reference</label>
                    {!! Form::select('reference_id', $reference_list, null, ['class' => 'form-control js-example-basic-single js-country', 'required' => 'required', 'id' => 'reference_id']) !!}
                </div>
            @else
                <div class="form-group reference-area">
                    <label class="control-label">Select Reference</label>
                    <select name="reference_id" class="form-control js-example-basic-single js-country" id="reference_id" style="width:100%">
                        <option value="">Select Reference</option>
                    </select>
                </div>
            @endif

            <div class="form-group reference-area"></div>

        </div>

    </div>

    &nbsp;
    <div class="row padding-top-10">
        <a href="{{ URL::to('user/'.$id.'/edit?step=1') }}" class="btn default"> Back </a>
        {!! Form::submit('Next', ['class' => 'btn green pull-right']) !!}
    </div>

{!! Form::close() !!}

<script type="text/javascript">
    // Get Reference list On UserType Change
    $('#user_type_id').on('change', function() {
        get_reference($(this).val(),'{{ $user->reference_id }}');
    });
</script>