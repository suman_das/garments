@extends('layouts.appinside')

@section('content')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ URL::to('home') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ URL::to('user') }}">User</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Update</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Users
        <small>update user</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->

    <div class="mt-element-step">
        <div class="row step-background-thin">
            <a href="{{ URL::to('user-management/'.$user->id.'/edit') }}">
                <div class="col-md-12 bg-grey-steel mt-step-col active">
                    <div class="mt-step-number"></div>
                    <div class="mt-step-title uppercase font-grey-cascade">Edit Basic</div>
                    <div class="mt-step-content font-grey-cascade">Personal information</div>
                </div>
            </a>

        </div>

            @include('core.superadmin.user-management.edit.1')


    </div>

    <script type="text/javascript">

        $(document ).ready(function() {
            // Navigation Highlight
            highlight_nav('user-manage', 'users');
        });

    </script>

@endsection
