@extends('layouts.appinside')

@section('content')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ URL::to('home') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ URL::to('user') }}">User</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Insert</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Users
        <small>create new</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->

    <div class="mt-element-step">
        <div class="row step-background-thin">
            <div class="col-md-12 bg-grey-steel mt-step-col active">
                <div class="mt-step-number"></div>
                <div class="mt-step-title uppercase font-grey-cascade ">Basic</div>
                <div class="mt-step-content font-grey-cascade">Personal information</div>
            </div>
           
        </div>

            {!! Form::open(array('route' => 'superadmin.user-management.store', 'method' => 'post')) !!}

                <div class="row">

                    @include('partials.errors')
                    <div class="row padding-top-20"></div>
                    <div class="col-md-6 col-md-offset-3">

                        <div class="form-group">
                            <label class="control-label">Name</label>
                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name', 'required' => 'required']) !!}
                        </div>

                        <div class="form-group">
                            <label class="control-label">Email</label>
                            {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required' => 'required']) !!}
                        </div>
                        <div class="form-group">
                            <label class="control-label">Password</label>
                            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Re-type New Password', 'oninput' => 'check(this)']) !!}
                        </div>
                        <div class="form-group">
                            <label class="control-label">Mobile Number</label>
                            
                            
                                {!! Form::text('msisdn', null, ['class' => 'form-control', 'placeholder' => 'Mobile Number',  'required' => 'required']) !!}
                            
                        </div>

                        

                        <div class="form-group">
                            <label class="control-label">Select Status</label>
                            {!! Form::select('status', array('1' => 'Active','0' => 'Inactive'), null, ['class' => 'form-control js-example-basic-single', 'required' => 'required']) !!}
                        </div>
                         <div class="form-group"> 
                <label class="control-label">Select User Group</label>
                {!! Form::select('roles',$roles, [], ['class' => 'form-control js-example-basic-single js-country', 'required' => 'required', 'id' => 'id']) !!}
            </div>
                                  <div class="form-group">
<!--                    <a href="javascript:history.back()" class="btn default"> Cancel </a>-->
                    {!! Form::submit('SUBMIT', ['class' => 'btn green  center-block col-lg-12']) !!}
                </div>

                    </div>

                    

                </div>

                &nbsp;
      

            {!! Form::close() !!}


    </div>

    <script type="text/javascript">

        $(document ).ready(function() {
            // Navigation Highlight
            highlight_nav('user-add', 'users');

            // Get State list
            var country_id = $('#country_id').val();
            get_states(country_id);
        });

        // Get State list On Country Change
        $('#country_id').on('change', function() {
            get_states($(this).val());
        });

        // Get City list On State Change
        $('#state_id').on('change', function() {
            get_cities($(this).val());
        });

        // Get Zone list On City Change
        $('#city_id').on('change', function() {
            get_zones($(this).val());
        });

    </script>

@endsection
