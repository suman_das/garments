@extends('layouts.appinside')

@section('content')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ URL::to('home') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ URL::to('water') }}">Waters</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Update</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Waters
        <small>create new</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->

            {!! Form::model($water, array('url' => '/water/'.$water->id, 'method' => 'put')) !!}

                <div class="row">

                    @include('partials.errors')

                    <div class="col-md-6">

                       <div class="form-group">
                           <label class="control-label col-md-12 np-lr">Ward No</label>
                           {!! Form::select('Ward_no', array(''=>'Select Ward') + $up, null, ['class' => 'form-control', 'required' => 'required']) !!}
                       </div>

                       <div class="form-group">
                           <label class="control-label col-md-12 np-lr">Village Name</label>
                          {!! Form::text('Village', null, ['class' => 'form-control', 'placeholder' => 'Village Name', 'required' => 'required']) !!} 
                       </div>

                        <div class="form-group">
                            <label class="control-label">TW_No: (6 digit upcode + tw sl)</label>
                            {!! Form::number('TW_No', null, ['class' => 'form-control', 'placeholder' => 'TW_No', 'required' => 'required','min'=>"1",'max'=>"3" ]) !!}
                        </div>

                        <div class="form-group">
                            <label class="control-label">Land Owner name:</label>
                            {!! Form::text('Landowner', null, ['class' => 'form-control', 'placeholder' => 'Land Owner name', 'required' => 'required']) !!}
                        </div>
                        <div class="form-group">
                           <label class="control-label">Caretaker(male) name:</label>
                           {!! Form::text('Caretaker_male', null, ['class' => 'form-control', 'placeholder' => 'Caretaker(male) name', 'required' => 'required']) !!}
                       </div>

                       <div class="form-group">
                           <label class="control-label">Caretaker (female) name:</label>
                           {!! Form::text('Caretaker_female', null, ['class' => 'form-control', 'placeholder' => 'Caretaker (female) name', 'required' => 'required']) !!}
                       </div>

                       <div class="form-group">
                           <label class="control-label">Number of Household benefited:</label>
                           {!! Form::number('HH_benefited', null, ['class' => 'form-control', 'placeholder' => 'Number of Household benefited', 'required' => 'required']) !!}
                       </div>
                      <div class="form-group">
                           <label class="control-label">Total Beneficiary_male:</label>
                           {!! Form::number('beneficiary_male', null, ['class' => 'form-control', 'placeholder' => 'Height', 'required' => 'required']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label">Hardcore Beneficiary:</label>
                           {!! Form::number('beneficiary_hardcore', null, ['class' => 'form-control', 'placeholder' => 'Height', 'required' => 'required']) !!}
                       </div>

                    </div>

                    <div class="col-md-6">

                       
                        <div class="form-group">
                           <label class="control-label">CDF_no: (3 digits):</label>
                           {!! Form::number('CDF_no', null, ['class' => 'form-control', 'placeholder' => 'CDF no', 'required' => 'required','max'=>"3"]) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label">Technology_Type:</label>
                           
                           {!! Form::select('Technology_Type', array(''=>'Select Technology') + $tech,null, ['class' => 'form-control', 'required' => 'required']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label">Approval date (YYYY-MM-DD), if approved:</label>
                           {!! Form::text('App_date', null, ['class' => 'form-control', 'required' => 'required']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label">Work Order/Lot No.:</label>
                           {!! Form::select('Tend_lot', array(''=>'Select Work Order') + $wordOrderNO, null, ['class' => 'form-control', 'required' => 'required']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label">Implementation Status:</label>
                           {!! Form::select('imp_status', array(''=>'Select Status') + $ImplementStatus, null, ['class' => 'form-control', 'required' => 'required']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label">Year of Completion: (4 digits):</label>
                           {!! Form::text('year', null, ['class' => 'form-control', 'placeholder' => 'Year', 'required' => 'required']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label">Number of Hardcore HH benefited:</label>
                           {!! Form::number('HCHH_benefited', null, ['class' => 'form-control', 'placeholder' => 'Number of Hardcore HH benefited', 'required' => 'required']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label">Total Beneficiary_female:</label>
                           {!! Form::number('beneficiary_female', null, ['class' => 'form-control', 'placeholder' => 'Total Beneficiary_female', 'required' => 'required']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label">Number of Beneficiary under safetynet:</label>
                           {!! Form::number('beneficiary_safetynet', null, ['class' => 'form-control', 'placeholder' => 'Height', 'required' => 'required']) !!}
                       </div>

                    </div>

                    

                </div>

                &nbsp;
                <div class="row padding-top-10">
                    <a href="javascript:history.back()" class="btn default"> Cancel </a>
                    {!! Form::submit('Update', ['class' => 'btn green pull-right']) !!}
                </div>

            {!! Form::close() !!}


    </div>

    <script type="text/javascript">

        $(document ).ready(function() {
            // Navigation Highlight
            highlight_nav('warehouse-manage', 'shelfs');

        });

    </script>

@endsection
