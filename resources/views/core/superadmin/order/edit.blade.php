@extends('layouts.appinside')

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('superadmin.dashboard') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('superadmin.order.index')}}">Order</a></li>
    <li class="breadcrumb-item active" aria-current="page"><a href="{{route('superadmin.order.edit', $manpower->id)}}">Update Order</a></li>
  </ol>
</nav>


@include('partials.errors')

<h1 class="page-title d-flex justify-content-center"> Order Management <small></small> </h1>
<div class="portlet mb-3">
        <div class="portlet-title">
            <div class="caption" style="text-align: center">
                <i class="fa fa-gift"></i>Update New Order </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
    </div>
<div class="col-md-12 d-flex justify-content-center">
    
{!! Form::model($manpower,array('route' => ['superadmin.order.update',$manpower->id], 'method' => 'post','class'=>'needs-validation col-md-8','id'=>'createForm')) !!}
            <div class="portlet mb-3">
        <div class="portlet-title">
            <div class="caption" style="text-align: center">
                <i class="fa fa-gift"></i>update manpower </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>
    </div>
            <div class="mb-3">
              <label for="buyer_name">Buyer :</label>
              {!! Form::text('buyer_name', null, ['class' => 'form-control', 'placeholder' => 'Buyer Name', 'required' => 'required' ,'id'=>'buyer_name']) !!}
            </div>
            <div class="mb-3">
              <label for="style">Style/ Buyer Refference :</label>
              {!! Form::text('style', null, ['class' => 'form-control', 'placeholder' => 'Style/ Buyer Refference :', 'required' => 'required','id'=>'style']) !!}
            </div>
            <div class="mb-3">
              <label for="model">Model :</label>
              {!! Form::text('model', null, ['class' => 'form-control', 'placeholder' => 'Model :', 'required' => 'required','id'=>'model']) !!}
            </div>
            <div class="mb-3">
              <label for="item">Item :</label>
              {!! Form::text('item', null, ['class' => 'form-control', 'placeholder' => 'Item', 'required' => 'required','id'=>'item']) !!}
            </div>
            <div class="mb-3">
              <label for="unit">Unit:</label>
              {!! Form::select('unit', array('CGL' => 'CGL','LILI' => 'LILY'), null, ['class' => 'form-control js-example-basic-single', 'required' => 'required','id'=>'unit']) !!}
            </div>
            <div class="mb-3">
              <label for="date">Start Date:</label>
              {!! Form::text('start_date', null, ['class' => 'form-control', 'placeholder' => 'date:', 'required' => 'required','id'=>'date']) !!}
            </div>
            
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to update</button>
            {{csrf_field()}}
            <input type="hidden" name="query" value="{{time()}}" />
            
            {!! Form::close() !!}

        
    </div>

<script type='text/javascript'>
    $('#date').datepicker({
    format: 'yyyy-mm-dd',
});
</script>


@endsection

@section('my_js')


@endsection

