@extends('layouts.appinside')

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('superadmin.dashboard') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('superadmin.order.index')}}">Order</a></li>
        <li class="breadcrumb-item active" aria-current="page"><a href="{{route('superadmin.order.create')}}">New Order</a></li>
    </ol>
</nav>


@include('partials.errors')

<h1 class="page-title d-flex justify-content-center"> Order Management <small></small> </h1>
<div class="portlet mb-3">
    <div class="portlet-title">
        <div class="caption" style="text-align: center">
            <i class="fa fa-gift"></i>Add New Order </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
</div>
<div class="col-md-12 d-flex justify-content-center">

    {!! Form::open(array('route' => 'superadmin.order.store', 'method' => 'post','class'=>'needs-validation col-md-8','id'=>'createForm')) !!}
    <div class="portlet mb-3">
        <div class="portlet-title">
            <div class="caption" style="text-align: center">
                <i class="fa fa-gift"></i>Add New Order </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

    </div>
    <div class="mb-3">
        <label for="buyer_name">Buyer :</label>
         {!! Form::select('buyer_id', $buyer, null, ['class' => 'form-control','id'    => 'buyer']) !!}
        <button type="button" class="btn btn-primary mt-3" data-toggle="modal" data-target="#AddNewBuyer">
            Add New Buyer
        </button>

    </div>
    <div class="mb-3">
        <label for="style">Style/ Buyer Refference :</label>
         {!! Form::select('style_id', $style, null, ['class' => 'form-control','id'    => 'style']) !!}
        <button type="button" class="btn btn-primary mt-3" data-toggle="modal" data-target="#AddNewStyle">
            Add New Style
        </button>
        
    </div>
    <div class="mb-3">
        <label for="model">Model :</label>
         {!! Form::select('model_id', $model, null, ['class' => 'form-control','id'    => 'model']) !!}

        <button type="button" class="btn btn-primary mt-3" data-toggle="modal" data-target="#AddNewModel">
            Add New Model
        </button>
        
    </div>
    <div class="mb-3">
        <label for="item">Item :</label>
         {!! Form::select('item_id', $item, null, ['class' => 'form-control','id'    => 'item']) !!}
        <button type="button" class="btn btn-primary mt-3" data-toggle="modal" data-target="#AddNewItem">
            Add New Item
        </button>
        
    </div>
    <!--    <div class="mb-3">
            <label for="unit">Unit:</label>
            {!! Form::select('unit', array('CGL' => 'CGL','LILI' => 'LILY'), null, ['class' => 'form-control js-example-basic-single', 'required' => 'required','id'=>'unit']) !!}
        </div>-->
    <div class="mb-3">
        <label for="date">Start Date:</label>
        {!! Form::text('start_date', null, ['class' => 'form-control', 'placeholder' => 'date:', 'required' => 'required','id'=>'date']) !!}
    </div>

    <hr class="mb-4">
    <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to create</button>
    {{csrf_field()}}
    <input type="hidden" name="query" value="{{time()}}" />

    {!! Form::close() !!}

    <div class="modal fade" id="AddNewBuyer" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <form>
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="text-center col-12">ADD BUYER</h2>
                        <!--                        <h5 class="mtextodal-title text-center" id="exampleModalLongTitle">Modal title</h5>-->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="mb-3">
                            <label for="buyer">Buyer Name :</label>
                            {!! Form::text('buyer_name', null, ['class' => 'form-control', 'placeholder' => 'Buyer Name', 'required' => 'required' ,'id'=>'buyername']) !!}
                            <span class="error-info"></span>
                        </div>   

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="SaveBuyer" class="btn btn-primary">Save changes</button>
                    </div>

                </div>
            </div>
        </form>  
    </div>
    <div class="modal fade" id="AddNewItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="text-center col-12">ADD ITEM</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="buyer">Item Name :</label>
                            {!! Form::text('item', null, ['class' => 'form-control', 'placeholder' => 'Item Name', 'required' => 'required' ,'id'=>'itemname']) !!}
                            <span class="error-info"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="saveItem" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
    </div>
    <div class="modal fade" id="AddNewModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="text-center col-12">ADD MODEL</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="buyer">Model Name :</label>
                            {!! Form::text('model', null, ['class' => 'form-control', 'placeholder' => 'Model Name', 'required' => 'required' ,'id'=>'modelname']) !!}
                            <span class="error-info"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="saveModel" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
    </div>
    <div class="modal fade" id="AddNewStyle" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="text-center col-12">ADD STYLE</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="buyer">Style Name :</label>
                            {!! Form::text('style', null, ['class' => 'form-control', 'placeholder' => 'Style Name', 'required' => 'required' ,'id'=>'stylename']) !!}
                            <span class="error-info"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="saveStyle" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
</div>

<script type='text/javascript'>
    $('#date').datepicker({
        format: 'yyyy-mm-dd',
    });
</script>


@endsection

@section('my_js')
<script type="text/javascript">
  $(document).ready(function(){
    $('#SaveBuyer').click(function(e){
         e.preventDefault(); // this prevents the form from submitting
      $.ajax({
        url: "{{route('superadmin.order.buyerstore')}}",
        type: "post",
        data: {'buyer_name':$('#buyername').val(), '_token': $('input[name=_token]').val()},
        dataType: 'JSON',
        success: function (data) {
            var res = data;
             // alert(res.msg);
            if( res.error == 0 )
            {
                $("select[name='buyer_id']").append(`
                      <option value="`+res.buyer.id+`">`+res.buyer.buyer_name+`</option>
                `);
                document.getElementById("buyer").value = res.buyer.id;
                $('#AddNewBuyer').modal('hide');
            }
            else{
               // $('#buyer').addClass("has-error");
                $('#AddNewBuyer .error-info').text(res.msg);
            }
        }
      });
    });
    
     $('#saveStyle').click(function(e){
         e.preventDefault(); // this prevents the form from submitting
      $.ajax({
        url: "{{route('superadmin.order.stylestore')}}",
        type: "post",
        data: {'style_name':$('#stylename').val(), '_token': $('input[name=_token]').val()},
        dataType: 'JSON',
        success: function (data) {
            var res = data;
             // alert(res.msg);
            if( res.error == 0 )
            {
                $("select[name='style_id']").append(`
                      <option value="`+res.style.id+`">`+res.style.style_name+`</option>
                `);
                document.getElementById("style").value = res.style.id;
                $('#AddNewStyle').modal('hide');
            }
            else{
               // $('#buyer').addClass("has-error");
                $('#AddNewStyle .error-info').text(res.msg);
            }
        }
      });
    });
    
  
  
  $('#saveModel').click(function(e){
         e.preventDefault(); // this prevents the form from submitting
      $.ajax({
        url: "{{route('superadmin.order.modelstore')}}",
        type: "post",
        data: {'model_name':$('#modelname').val(), '_token': $('input[name=_token]').val()},
        dataType: 'JSON',
        success: function (data) {
            var res = data;
             // alert(res.msg);
            if( res.error == 0 )
            {
                $("select[name='model_id']").append(`
                      <option value="`+res.model.id+`">`+res.model.model_name+`</option>
                `);
                document.getElementById("model").value = res.model.id;
                $('#AddNewItem').modal('hide');
            }
            else{
               // $('#buyer').addClass("has-error");
                $('#AddNewModel .error-info').text(res.msg);
            }
        }
      });
    });
    
  
  
  $('#saveItem').click(function(e){
         e.preventDefault(); // this prevents the form from submitting
      $.ajax({
        url: "{{route('superadmin.order.itemstore')}}",
        type: "post",
        data: {'item_name':$('#itemname').val(), '_token': $('input[name=_token]').val()},
        dataType: 'JSON',
        success: function (data) {
            var res = data;
             // alert(res.msg);
            if( res.error == 0 )
            {
                $("select[name='item_id']").append(`
                      <option value="`+res.item.id+`">`+res.item.item_name+`</option>
                `);
                document.getElementById("item").value = res.item.id;
                $('#AddNewItem').modal('hide');
            }
            else{
               // $('#buyer').addClass("has-error");
                $('#AddNewItem .error-info').text(res.msg);
            }
        }
      });
    });
    
  });
</script>

@endsection

