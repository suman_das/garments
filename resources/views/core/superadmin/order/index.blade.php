@extends('layouts.appinside')

@section('content')

  <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('superadmin.dashboard') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('superadmin.order.index')}}">Order</a></li>
    <li class="breadcrumb-item active" aria-current="page"><a href="{{route('superadmin.order.create')}}">New Order</a></li>
  </ol>
</nav>

  @include('partials.errors')

  <h1 class="page-title"> Order List <small></small> </h1>

  <div class="row my-3 card no-b shadow">
      <div class="col-md-12">
          <div class="card-header white p-0 pt-4">
              <h6 class="text-uppercase d-inline-block"> Daily Order Report</h6>
              @if(Session::has('success'))
            <div class="alert alert-warning alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            {{Session::get('success')}}
</div>
@endif
              <div class="float-right d-inline-block table-filter">
                  <div class="input-group mb-2 mr-sm-2">
                      <button type="submit" class="btn btn-default mr-2 rounded-0" id="dashboardFilter"><span
                              class="s-18 icon-search-plus2 mr-2"></span>Filter
                      </button>
                     
                  </div>
              </div>
              <div class="mt-4">
                  <form action="{{route('superadmin.manpower.index')}}" class="" method="GET" id="createForm">
                      <input type="hidden" name="query" value="{{time()}}" />
                  <div class="filterTable mb-4 white">
                      <div class="col-md-12">
                          <div class="p-0 pt-4">
                              <div class="row">
                                  <div class="col-md-5 pr-1 pl-0">
                                      {!! Form::select('unit', array('CGL' => 'CGL','LILI' => 'LILY'), null, ['class' => 'form-control js-example-basic-single', 'required' => 'required','id'=>'unit']) !!}
                                  </div>
                                  
                                   <div class="col-md-5 p-0 pr-1">
              
                                    {!! Form::text('date', null, ['class' => 'form-control', 'placeholder' => 'Start date:', 'required' => 'required','id'=>'date']) !!}
                                    </div>
                                  <div class="col-md-2 p-0 pr-1">
                                      <button type="submit" class="btn btn-outline-primary btn-sm rounded-0 filter-btn w-100"><i class="icon-arrow_forward mr-2"></i>Search</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  {!! Form::close() !!}
              </div>
          </div>
      </div>
  </div>
  @if(count($manpowers))

  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
      <div class="portlet-body util-btn-margin-bottom-5">

        <div class="table-responsive">
          <style>
            #example0 td {
              white-space: nowrap;
              font-size: 10px;
            }
            #example0 th {
              font-size: 10px;
            }
          </style>
        <div class="card-body table-responsive white px-0">
          <table class="table table-striped table-hover r-0" id="example0">
            <thead >
              <tr>
              <th>Buyer :</th>
              <th>Style/ Buyer Refference</th>
              <th>Model</th>
              <th>Item</th>
              <th>Start Date</th>
             <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @foreach($manpowers as $manpower)
             
                <tr>

                  <td>{{$manpower->buyer_name}}</td>
                  <td>{{$manpower->style_name}} </td>
                  <td>{{ $manpower->model_name}}</td>
                  <td>{{$manpower->item_name}} </td>
                  <td>{{$manpower->start_date}}</td>
                  <td>
                       <p>
        
<!--        <a href="{{route('superadmin.order.edit', $manpower->id)}}" class="btn btn-primary">Edit Task</a>-->
    </p>
                  </td>

                 
                </tr>
              @endforeach
            </tbody>

            <tfoot></tfoot>
          </table>
        </div>
        </div>
          {{ $manpowers->appends($_GET)->links() }}
      </div>
    </div>
  </div>



  @else
  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
      <p>
        No Data Found
      </p>
    </div>
  </div>
  
  @endif
  <script type='text/javascript'>
    $('#date').datepicker({
    format: 'yyyy-mm-dd',
});
</script>
@endsection


@section('my_js')

@endsection