@extends('layouts.appinside')

@section('content')

<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ URL::to('home') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Report</span>
        </li>
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Details Report
    <small></small>
</h1>

<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-filtter">
                        {!! Form::open(array('method' => 'get')) !!}

                        <div class="form-group col-lg-3">
                            <label for="fdate" class="control-label col-lg-12">From: </label>
                            <div class="col-lg-12 row">
                                {{ Form::text('fdate', '', array('class' => 'form-control','placeholder'=>'From date','id'=>'fdate')) }}
                                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="fdate" class="control-label col-lg-12">To: </label>
                            <div class="col-lg-12 row">
                                {{ Form::text('tdate', '', array('class' => 'form-control','placeholder'=>'To date','id'=>'tdate')) }}
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="fdate" class="control-label col-lg-12">Dealer Code: </label>
                            <div class="col-lg-12 row">
                                {{ Form::text('dcode', '', array('class' => 'form-control','placeholder'=>'Dealer Code','id'=>'dcode')) }}
                            </div>
                        </div>

                        <div class="form-group col-lg-3">
                            <label for="fdate" class="control-label col-lg-12">Mobile Number: </label>
                            <div class="col-lg-12 row">
                                {{ Form::text('amobile', '', array('class' => 'form-control','placeholder'=>'Customer Number','id'=>'cmobile')) }}
                            </div>
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="fdate" class="control-label col-lg-12">Status: </label>
                            <div class="col-lg-12 row">
                                {!! Form::select('status', array(''=>'Select Staatus','Invalid Traders'=>'Invalid Traders','Invalid Product'=>'Invalid Product','declined'=>'declined'), null, ['class' => 'form-control', 'id' => 'status']) !!}
                            </div>
                        </div>


                        <div class="clearfix"></div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-users font-dark"></i>
                    <span class="caption-subject bold uppercase">Report Details</span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table id="employee-grid2"  cellpadding="0" cellspacing="0" border="0" class="display table" width="100%">


                </table>
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="employee-grid">
                    <thead>
                        <tr role="row" class="heading">
                            <th>Mobile Number</th>
                            <th>SMS</th>
                            <th>Reply</th>
                            <th>Traders Code</th>
                            <th>Customer Amount</th>
                            <th>Trader Amount</th>
                            <th>Status</th>
                            <th>Date</th>
                        </tr>
                    </thead>

                </table>

            </div>
        </div>

    </div>
</div>


<script type="text/javascript" language="javascript" >
    $(document).ready(function () {
        var csrftoken = $("#csrf-token").val();
        var dataTable = $('#employee-grid').DataTable({
            "processing": true,
            "serverSide": true,
            "aoColumnDefs": [
                {
                    'bSortable': false

                }],
            "ajax": {
                url: '{{ \URL::to('superadmin/detailsreport/detaisldata') }}', // json datasource
                type: "post", // method  , by default get
                data: {'_token': csrftoken},
                error: function () {  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display", "none");
                },
                "dataSrc": function (json) {
                    $("#employee-grid2").html("");
                    $("#employee-grid2").append(json.query);
                    return json.data;
                }
            }
        });
        $("#employee-grid_filter").css("display", "none"); // hiding global search box

        $('#fdate').on('change', function () {   // for text boxes
            var i = $(this).attr('data-column'); // getting column index
            var v = $(this).val(); // getting search input value
            dataTable.columns(0).search(v).draw();
        });
        $('#tdate').on('change', function () {   // for select box
            var i = $(this).attr('data-column');
            var v = $(this).val();
            dataTable.columns(1).search(v).draw();
        });
        $('#dcode').on('change', function () {   // for select box
            var i = $(this).attr('data-column');
            var v = $(this).val();
            dataTable.columns(2).search(v).draw();
        });
        
        
        $('#amobile').on('change', function () {   // for select box
            var i = $(this).attr('data-column');
            var v = $(this).val();
            dataTable.columns(3).search(v).draw();
        });
        $('#status').on('change', function () {   // for select box
            var i = $(this).attr('data-column');
            var v = $(this).val();
            dataTable.columns(4).search(v).draw();
        });
        
       
    });</script>
<script type="text/javascript">
    $("#fdate").datepicker({
// format: "dd MM yyyy - hh:ii",
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: true,
        minuteStep: 10
    });
    $("#tdate").datepicker({
// format: "dd MM yyyy - hh:ii",
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: true,
        minuteStep: 10
    });
</script> 

<style media="screen">
    .table-filtter .btn{ width: 100%;}
    .table-filtter {
        margin: 20px 0;
    }
</style>

@endsection
