<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>

            <li class="nav-item start dashboard">
                <a href="{{route('superadmin.dashboard')}}">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>

             
            
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-folder"></i>
                    <span class="title">Report</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{route('superadmin.report.detailsreport.index')}}" class="nav-link">
                            <i class="icon-bar-chart"></i> SMS Report </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('superadmin.Product.ProductDetails.index')}}" class="nav-link">
                            <i class="icon-bar-chart"></i> Order Report </a>
                    </li>
                    
                </ul>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-folder"></i>
                    <span class="title">User Management</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{route('superadmin.user-management.index')}}" class="nav-link">
                            <i class="icon-bar-chart"></i> PPO List </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('superadmin.user-management.create')}}" class="nav-link">
                            <i class="icon-bar-chart"></i> Create New PPO </a>
                    </li>
                    
                </ul>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-folder"></i>
                    <span class="title">Traders Management</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{route('superadmin.retailer-management.index')}}" class="nav-link">
                            <i class="icon-bar-chart"></i> Trader List </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('superadmin.retailer-management.create')}}" class="nav-link">
                            <i class="icon-bar-chart"></i> Create New Traders </a>
                    </li>
                    
                </ul>
            </li>
        </ul>
    </div>
</div>
