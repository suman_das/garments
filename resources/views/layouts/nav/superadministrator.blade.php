 <li>
                        <a class="nav-link s-14 active font-weight-light" href="{{route('superadmin.dashboard')}}">Home</a>
                    </li>
                    <li>
                        <a class="nav-link s-14  font-weight-light" href="{{route('superadmin.manpower.create')}}">Manpower</a>
                    </li>
                    <li>
                        <a class="nav-link s-14  font-weight-light" href="{{route('superadmin.manpower.index')}}">Manpower list</a>
                    </li>
                    <li>
                        <a class="nav-link s-14  font-weight-light" href="{{route('superadmin.order.create')}}">Order New</a>
                    </li>
                    <li>
                        <a class="nav-link s-14  font-weight-light" href="{{route('superadmin.order.index')}}">Order list</a>
                    </li>
                    <li>
                        <a class="nav-link s-14  font-weight-light" href="{{route('superadmin.target.create')}}">Target</a>
                    </li>
                    <li>
                        <a class="nav-link s-14  font-weight-light" href="{{route('superadmin.target.index')}}">Target list</a>
                    </li>
                    <li>
                        <a class="nav-link s-14  font-weight-light" href="{{route('superadmin.npt.create')}}">NPT New</a>
                    </li>
                    <li>
                        <a class="nav-link s-14  font-weight-light" href="{{route('superadmin.npt.index')}}">NPT list</a>
                    </li>
                    <li>
                        <a class="nav-link s-14  font-weight-light" href="{{route('superadmin.machinedata.create')}}">Machine New</a>
                    </li>
                    <li>
                        <a class="nav-link s-14  font-weight-light" href="{{route('superadmin.machinedata.index')}}">Machine data list</a>
                    </li>
                    <li>
                        <a class="nav-link s-14  font-weight-light" href="line-report.html">Line Report</a>
                    </li>

                    <!-- User Account-->
                    <li class="dropdown custom-dropdown user user-menu">
                        <a href="#" class="nav-link" data-toggle="dropdown">
                            <img src="assets/img/basic/u8.png" class="user-image" alt="User Image">
                        </a>
                        <div class="dropdown-menu">
                            <div class="list-group mt-3" id="userSettingsCollapse">
                                <a href="index.html" class="list-group-item list-group-item-action ">
                                    <i class="mr-2 icon-umbrella text-blue"></i>Profile
                                </a>
                                <a href="#" class="list-group-item list-group-item-action"><i
                                        class="mr-2 icon-cogs text-yellow"></i>Settings</a>
                                <a href="#" class="list-group-item list-group-item-action"><i
                                        class="mr-2 icon-security text-purple"></i>Change Password</a>
                            </div>
                        </div>
                    </li>