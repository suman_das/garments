<div class="top-menu">
    <ul class="nav navbar-nav pull-right">

        <li class="dropdown dropdown-user">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                 <span class="username username-hide-on-mobile">{{\Auth::user()->name}}</span>
                <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
                <li class="divider"> </li>
                <li>
                    <a href="{{ route('logout') }}">
                    <i class="icon-key"></i> Sign Out </a>
                </li>
            </ul>
        </li>

    </ul>
</div>