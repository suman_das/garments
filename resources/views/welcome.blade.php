@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    Your Application's Landing Page.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



        <div class="row">
            <div class="col-md-8 p-0">
                <div class="blue accent-2 px-3 py-5">
                    <div class="chart">
                        <canvas
                                data-chart="line"
                                data-dataset="[[15, 26, 36, 32, 40, 35, 37, 40, 53, 60, 80, 90],[48, 54, 53, 58, 56, 62, 61, 59, 76, 78, 90, 100]]"
                                data-labels="['9 AM', '10 AM', '11 AM', '12 PM', '1 PM', '2 PM', '3 PM', '4 PM', '6 PM', '7 PM', '8 PM', '9 PM']"
                                data-dataset-options="datasets: [{
                    label: 'Revenue',
                    fill: !0,
                    lineTension: 0,
                    backgroundColor: 'rgba(0,172,255, 0.1)',
                    borderWidth: 2,
                    borderColor: '#00AAFF',
                    borderCapStyle: 'butt',
                    borderDash: [4],
                    borderDashOffset: 0,
                    borderJoinStyle: 'miter',
                    pointRadius: 4,
                    pointBorderColor: '#00AAFF',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 2,
                    pointHoverRadius: 6,
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#00AAFF',
                    pointHoverBorderWidth: 2,
                    spanGaps: !1
                }, {
                    label: 'Profit',
                    fill: !0,
                    lineTension: 0,
                    backgroundColor: 'rgba(163,136,227, 0.1)',
                    borderWidth: 2,
                    borderColor: '#886CE6',
                    pointRadius: 4,
                    pointBorderColor: '#886CE6',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 2,
                    pointHoverRadius: 6,
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#886CE6',
                    pointHoverBorderWidth: 2,
                    spanGaps: !1
                }]"
                                data-options="{
                legend: {
                    display: 0,
                    labels: {
                        fontColor: '#7F8FA4',
                        fontFamily: 'Source Sans Pro, sans-serif',
                        boxRadius: 4,
                        usePointStyle: !0
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0
                    }
                },
                scales: {
                    xAxes: [{
                        display: !0,
                        ticks: {
                            fontSize: '11',
                            fontColor: '#6cd2ff'
                        },
                        gridLines: {
                            color: 'rgba(7, 112, 199, 1)',
                            zeroLineColor: 'rgba(0,0,0,0.0)'
                        }
                    }],
                    yAxes: [{
                        display: !0,
                        gridLines: {
                            color: 'rgba(7, 112, 199, 1)',
                            zeroLineColor: 'rgba(0,0,0,0.0)'
                        },
                        ticks: {
                            beginAtZero: !0,
                            max: 100,
                            stepSize: 25,
                            fontSize: '11',
                            fontColor: '#6cd2ff'
                        }
                    }]
                }
            }">
                        </canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-4 px-0 blue accent-2 text-white">
                <div class="p-3s">
                    <div class="counter-box overview-box r-3">
                        <div class="p-4">
                            <div class="float-right">
                                <span class="s-36 icon-stop-watch3"></span>
                            </div>
                            <div class="counter-title s-18">Efficiency <span
                                    class="s-14 icon-long-arrow-right mx-2"></span> <span
                                    class="s-18 font-weight-normal">64%</span></div>
                        </div>
                        <div class="progress progress-xs r-0">
                            <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25"
                                 aria-valuemin="0" aria-valuemax="128"></div>
                        </div>
                    </div>
                    <div class="counter-box overview-box r-3">
                        <div class="p-4">
                            <div class="float-right">
                                <span class="s-36 icon-barometer"></span>
                            </div>
                            <div class="counter-title">Total Production <span
                                    class="s-12 icon-long-arrow-right mx-2"></span><span
                                    class="s-14 font-weight-normal">25,708</span></div>
                            <div class="s-12 font-weight-lighter"> Average Work Hours
                                <span class="text-primary"><i
                                        class="icon-angle-double-down text-red mx-1"></i> 11.03</span>
                            </div>
                        </div>
                        <div class="progress progress-xs r-0">
                            <div class="progress-bar" role="progressbar" style="width: 35%;" aria-valuenow="35"
                                 aria-valuemin="0" aria-valuemax="128"></div>
                        </div>
                    </div>
                    <div class="counter-box overview-box r-3">
                        <div class="p-4">
                            <div class="float-right">
                                <span class="s-36 icon-golf"></span>
                            </div>
                            <div class="counter-title">Total Manpower Present <span
                                    class="s-12 icon-long-arrow-right mx-2"></span> <span
                                    class="s-14 font-weight-normal">25,708</span></div>
                            <div class="s-12 font-weight-lighter"> Present Operator
                                <span class="text-primary"><i
                                        class="icon-angle-double-down text-red mx-1"></i> 325</span>
                            </div>
                        </div>
                        <div class="progress progress-xs r-0">
                            <div class="progress-bar" role="progressbar" style="width: 50%;" aria-valuenow="50"
                                 aria-valuemin="0" aria-valuemax="128"></div>
                        </div>
                    </div>
                    <div class="counter-box overview-box r-3">
                        <div class="p-4">
                            <div class="float-right">
                                <span class="s-36 icon-box5"></span>
                            </div>
                            <div class="counter-title">Total MCs in Factory <span
                                    class="s-12 icon-long-arrow-right mx-2"></span><span
                                    class="s-14 font-weight-normal">25,708</span></div>
                            <div class="s-12 font-weight-lighter"> Rented Machine in Factory
                                <span class="text-primary"><i class="icon-angle-double-down text-red mx-1"></i> 0</span>
                            </div>
                        </div>
                        <div class="progress progress-xs r-0">
                            <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25"
                                 aria-valuemin="0" aria-valuemax="128"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid animatedParent animateOnce no-p">
            <div class="animated fadeInUpShort">
                <div class="card no-b shadow">
                    <div class="card-body p-0">
                        <div class="lightSlider" data-item="6" data-item-xl="4" data-item-md="2" data-item-sm="2"
                             data-pause="7000" data-pager="false" data-auto="true"
                             data-loop="true">
                            <div class="p-5 light lslide">
                                <h5 class="font-weight-normal s-14 text-uppercase">Line 01</h5>
                                <span class="s-48 font-weight-lighter text-green">300</span>
                                <div> Efficiency
                                    <span class="text-green">
                                    <i class="icon icon-arrow_upward"></i> 67%</span>
                                </div>
                            </div>

                            <div class="p-5">
                                <h5 class="font-weight-normal s-14 text-uppercase">Line 02</h5>
                                <span class="s-48 font-weight-lighter text-green">675</span>
                                <div> Efficiency
                                    <span class="text-green">
                                    <i class="icon icon-arrow_upward"></i> 67%</span>
                                </div>
                            </div>
                            <div class="p-5 light lslide">
                                <h5 class="font-weight-normal s-14 text-uppercase">Line 03</h5>
                                <span class="s-48 font-weight-lighter text-red">300</span>
                                <div> Efficiency
                                    <span class="text-red">
                                    <i class="icon icon-arrow_downward"></i> 67%</span>
                                </div>
                            </div>

                            <div class="p-5">
                                <h5 class="font-weight-normal s-14 text-uppercase">Line 04</h5>
                                <span class="s-48 font-weight-lighter text-green">675</span>
                                <div> Efficiency
                                    <span class="text-green">
                                    <i class="icon icon-arrow_upward"></i> 67%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-3 card no-b shadow">
                    <div class="col-md-12">
                        <div class="card-header white p-0 pt-4">
                            <h6 class="text-uppercase d-inline-block"> Daily Efficiency Report</h6>
                            <div class="float-right d-inline-block table-filter">
                                <div class="input-group mb-2 mr-sm-2">
                                    <button type="submit" class="btn btn-default mr-2 rounded-0" id="dashboardFilter"><span
                                            class="s-18 icon-search-plus2 mr-2"></span>Filter
                                    </button>
                                    <input type="text" class="form-control rounded-0" id="" placeholder="Search..">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-default input-group-text rounded-0"><span
                                                class="s-24 icon-search2"></span></button>
                                        <button type="submit" class="btn btn-primary ml-2"><span
                                                class="s-18 icon-document-file-xls mx-2"></span>Export
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-4">
                                <div class="filterTable mb-4 white">
                                    <div class="col-md-12">
                                        <div class="p-0 pt-4">
                                            <div class="row">
                                                <div class="col-md-1 pr-1 pl-0">
                                                    <select class="custom-select select2">
                                                        <option value="">Unit</option>
                                                        <option value="1">CGL</option>
                                                        <option value="2">CGL</option>
                                                        <option value="3">CGL</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-1 p-0 pr-1">
                                                    <select class="custom-select select2">
                                                        <option value="">Floor</option>
                                                        <option value="1">1st</option>
                                                        <option value="2">2nd</option>
                                                        <option value="3">3rd</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-1 p-0 pr-1">
                                                    <select class="custom-select select2">
                                                        <option value="">Line</option>
                                                        <option value="1">01</option>
                                                        <option value="2">01</option>
                                                        <option value="3">01</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-1 p-0 pr-1">
                                                    <select class="custom-select select2">
                                                        <option value="">Buyer</option>
                                                        <option value="1">PVH</option>
                                                        <option value="2">PVH</option>
                                                        <option value="3">PVH</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2 p-0 pr-1">
                                                    <select class="custom-select select2">
                                                        <option value="">Style</option>
                                                        <option value="1">488735</option>
                                                        <option value="2">488735</option>
                                                        <option value="3">488735</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2 p-0 pr-1">
                                                    <select class="custom-select select2">
                                                        <option value="">Model</option>
                                                        <option value="1">RVH 4201</option>
                                                        <option value="2">RVH 4201</option>
                                                        <option value="3">RVH 4201</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2 p-0 pr-1">
                                                    <select class="custom-select select2">
                                                        <option value="">Item</option>
                                                        <option value="1">L/S Shirt</option>
                                                        <option value="2">L/S Shirt</option>
                                                        <option value="3">L/S Shirt</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2 p-0 pr-1">
                                                    <button type="button" class="btn btn-outline-primary btn-sm rounded-0 filter-btn w-100"><i class="icon-arrow_forward mr-2"></i>Search</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body table-responsive white px-0">
                            <table class="table table-striped table-hover r-0">
                                <thead>
                                <tr class="no-b">
                                    <th class="text-uppercase">Unit</th>
                                    <th class="text-uppercase">Floor</th>
                                    <th class="text-uppercase">Line</th>
                                    <th class="text-uppercase">Buyer</th>
                                    <th class="text-uppercase">Style</th>
                                    <th class="text-uppercase">Model</th>
                                    <th class="text-uppercase">Item</th>
                                    <th class="text-uppercase">Efficiency</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>CGL</td>
                                    <td>1st</td>
                                    <td>01</td>
                                    <td>PVH</td>
                                    <td>488735</td>
                                    <td>RVH 4201</td>
                                    <td>L/S Shirt</td>
                                    <td><i class="icon-angle-double-down text-red mr-2"></i>10%</td>
                                </tr>
                                <tr>
                                    <td>CGL</td>
                                    <td>1st</td>
                                    <td>01</td>
                                    <td>PVH</td>
                                    <td>488735</td>
                                    <td>RVH 4201</td>
                                    <td>L/S Shirt</td>
                                    <td><i class="icon-angle-double-down text-red mr-2"></i>10%</td>
                                </tr>
                                <tr>
                                    <td>CGL</td>
                                    <td>1st</td>
                                    <td>01</td>
                                    <td>PVH</td>
                                    <td>488735</td>
                                    <td>RVH 4201</td>
                                    <td>L/S Shirt</td>
                                    <td><i class="icon-angle-double-down text-red mr-2"></i>10%</td>
                                </tr>
                                <tr>
                                    <td>CGL</td>
                                    <td>1st</td>
                                    <td>01</td>
                                    <td>PVH</td>
                                    <td>488735</td>
                                    <td>RVH 4201</td>
                                    <td>L/S Shirt</td>
                                    <td><i class="icon-angle-double-down text-red mr-2"></i>10%</td>
                                </tr>
                                <tr>
                                    <td>CGL</td>
                                    <td>1st</td>
                                    <td>01</td>
                                    <td>PVH</td>
                                    <td>488735</td>
                                    <td>RVH 4201</td>
                                    <td>L/S Shirt</td>
                                    <td><i class="icon-angle-double-down text-red mr-2"></i>10%</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
