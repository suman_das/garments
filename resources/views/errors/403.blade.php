@extends('layouts.appinside')

@section('content')

<link href="{{ asset('assets/pages/css/error.min.css') }}" rel="stylesheet" type="text/css" />
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="#">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Error</span>
            </li>
        </ul>
    </div>

    <div class="row">
        <div class="col-md-12 page-404">
            <div class="number font-green" style="top:10px"> 403 </div>
            <div class="details">
                <h3>Oops! You're lost.</h3>
                <p> We can not find the page you're looking for.
                    <br/>
                    <a href="#"> Return home </a></p>
            </div>
        </div>
    </div>
@endsection
