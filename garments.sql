/*
SQLyog Ultimate - MySQL GUI v8.2 
MySQL - 5.5.5-10.1.30-MariaDB : Database - puritmis2018
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `buyers` */

CREATE TABLE `buyers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `buyer_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `buyers` */

insert  into `buyers`(`id`,`buyer_name`,`created_at`,`updated_at`) values (1,'www','2018-07-23 13:14:43','2018-07-23 13:14:43');
insert  into `buyers`(`id`,`buyer_name`,`created_at`,`updated_at`) values (2,'w','2018-07-23 13:20:54','2018-07-23 13:20:54');
insert  into `buyers`(`id`,`buyer_name`,`created_at`,`updated_at`) values (3,'q','2018-07-23 13:23:38','2018-07-23 13:23:38');
insert  into `buyers`(`id`,`buyer_name`,`created_at`,`updated_at`) values (4,'3','2018-07-23 13:23:45','2018-07-23 13:23:45');
insert  into `buyers`(`id`,`buyer_name`,`created_at`,`updated_at`) values (5,'ww','2018-07-23 13:23:54','2018-07-23 13:23:54');
insert  into `buyers`(`id`,`buyer_name`,`created_at`,`updated_at`) values (6,'1','2018-07-23 14:38:21','2018-07-23 14:38:21');
insert  into `buyers`(`id`,`buyer_name`,`created_at`,`updated_at`) values (7,'6','2018-07-23 14:39:20','2018-07-23 14:39:20');
insert  into `buyers`(`id`,`buyer_name`,`created_at`,`updated_at`) values (8,'7','2018-07-23 14:39:53','2018-07-23 14:39:53');
insert  into `buyers`(`id`,`buyer_name`,`created_at`,`updated_at`) values (9,'t','2018-07-23 14:40:43','2018-07-23 14:40:43');

/*Table structure for table `daily_target_summary` */

CREATE TABLE `daily_target_summary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `l_date` date DEFAULT NULL,
  `line_no` int(11) DEFAULT '0',
  `order_id` int(11) DEFAULT '0',
  `target_id` int(11) DEFAULT '0',
  `unit_setting_id` int(10) NOT NULL,
  `hour_no` int(11) DEFAULT '0',
  `hour_planned_status` varchar(45) DEFAULT 'P',
  `initial_target` int(11) DEFAULT '0',
  `target` int(11) DEFAULT '0',
  `output` int(11) DEFAULT '0',
  `line_def` int(11) DEFAULT '0',
  `output_percentage` double(15,4) DEFAULT '0.0000',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8;

/*Data for the table `daily_target_summary` */

insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (1,'2018-07-16',1,1,23,0,0,'P',0,0,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 18:53:53');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (2,'2018-07-16',1,1,NULL,0,1,'P',0,0,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (3,'2018-07-16',1,1,NULL,0,2,'P',0,0,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (4,'2018-07-16',1,1,NULL,0,3,'P',0,0,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (5,'2018-07-16',1,1,NULL,0,4,'P',0,0,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (6,'2018-07-16',1,1,NULL,0,5,'P',0,0,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (7,'2018-07-16',1,1,NULL,0,6,'P',0,0,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (8,'2018-07-16',1,1,NULL,0,7,'P',0,0,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (9,'2018-07-16',1,1,NULL,0,8,'P',22,22,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (10,'2018-07-16',1,1,NULL,0,9,'P',20,20,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (11,'2018-07-16',1,1,NULL,0,10,'P',20,20,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (12,'2018-07-16',1,1,NULL,0,11,'P',20,20,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (13,'2018-07-18',1,1,13,0,8,'U',20,20,2000,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-18 14:00:38');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (14,'2018-07-18',1,1,13,0,9,'U',2222,760,2000,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-18 14:00:41');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (15,'2018-07-18',1,1,13,0,10,'U',20,0,85900,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-18 14:14:09');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (16,'2018-07-18',1,1,13,0,22,'P',20,760,3590,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-18 14:00:44');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (17,'2018-07-18',1,1,14,0,12,'U',20,760,599,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-18 14:00:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (18,'2018-07-16',1,1,NULL,0,17,'P',20,20,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (19,'2018-07-16',1,1,NULL,0,18,'P',20,20,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (20,'2018-07-16',1,1,NULL,0,19,'P',20,20,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (21,'2018-07-16',1,1,NULL,0,20,'P',0,0,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (22,'2018-07-16',1,1,NULL,0,21,'P',0,0,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (23,'2018-07-16',1,1,NULL,0,22,'P',0,0,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (24,'2018-07-16',1,1,NULL,0,23,'P',0,0,0,0,0.0000,'2018-07-16 12:44:45',0,'2018-07-16 12:44:45');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (25,'2018-07-19',1,1,NULL,0,0,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (26,'2018-07-19',1,1,NULL,0,1,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (27,'2018-07-19',1,1,NULL,0,2,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (28,'2018-07-19',1,1,NULL,0,3,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (29,'2018-07-19',1,1,NULL,0,4,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (30,'2018-07-19',1,1,NULL,0,5,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (31,'2018-07-19',1,1,NULL,0,6,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (32,'2018-07-19',1,1,NULL,0,7,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (33,'2018-07-19',1,1,NULL,0,8,'P',9,9,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (34,'2018-07-19',1,1,NULL,0,9,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (35,'2018-07-19',1,1,NULL,0,10,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (36,'2018-07-19',1,1,NULL,0,11,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (37,'2018-07-19',1,1,NULL,0,12,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (38,'2018-07-19',1,1,NULL,0,13,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (39,'2018-07-19',1,1,NULL,0,14,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (40,'2018-07-19',1,1,NULL,0,15,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (41,'2018-07-19',1,1,NULL,0,16,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (42,'2018-07-19',1,1,NULL,0,17,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (43,'2018-07-19',1,1,NULL,0,18,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (44,'2018-07-19',1,1,NULL,0,19,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (45,'2018-07-19',1,1,NULL,0,20,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (46,'2018-07-19',1,1,NULL,0,21,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (47,'2018-07-19',1,1,NULL,0,22,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (48,'2018-07-19',1,1,NULL,0,23,'P',0,0,0,0,0.0000,'2018-07-19 15:40:46',0,'2018-07-19 15:40:46');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (49,'2018-07-19',14,1,NULL,0,0,'P',0,0,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (50,'2018-07-19',14,1,NULL,0,1,'P',0,0,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (51,'2018-07-19',14,1,NULL,0,2,'P',0,0,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (52,'2018-07-19',14,1,NULL,0,3,'P',0,0,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (53,'2018-07-19',14,1,NULL,0,4,'P',0,0,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (54,'2018-07-19',14,1,NULL,0,5,'P',0,0,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (55,'2018-07-19',14,1,NULL,0,6,'P',0,0,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (56,'2018-07-19',14,1,NULL,0,7,'P',0,0,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (57,'2018-07-19',14,1,NULL,0,8,'P',2147483647,2147483647,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (58,'2018-07-19',14,1,NULL,0,9,'P',2147483647,2147483647,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (59,'2018-07-19',14,1,NULL,0,10,'P',2147483647,2147483647,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (60,'2018-07-19',14,1,NULL,0,11,'P',2147483647,2147483647,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (61,'2018-07-19',14,1,NULL,0,12,'P',2147483647,2147483647,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (62,'2018-07-19',14,1,NULL,0,13,'P',0,0,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (63,'2018-07-19',14,1,NULL,0,14,'P',2147483647,2147483647,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (64,'2018-07-19',14,1,NULL,0,15,'P',2147483647,2147483647,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (65,'2018-07-19',14,1,NULL,0,16,'P',2147483647,2147483647,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (66,'2018-07-19',14,1,NULL,0,17,'P',2147483647,2147483647,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (67,'2018-07-19',14,1,NULL,0,18,'P',2147483647,2147483647,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (68,'2018-07-19',14,1,NULL,0,19,'P',2147483647,2147483647,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (69,'2018-07-19',14,1,NULL,0,20,'P',0,0,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (70,'2018-07-19',14,1,NULL,0,21,'P',0,0,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (71,'2018-07-19',14,1,NULL,0,22,'P',0,0,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (72,'2018-07-19',14,1,NULL,0,23,'P',0,0,0,0,0.0000,'2018-07-19 15:41:43',0,'2018-07-19 15:41:43');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (73,'2018-07-23',1,1,NULL,0,0,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (74,'2018-07-23',1,1,NULL,0,1,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (75,'2018-07-23',1,1,NULL,0,2,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (76,'2018-07-23',1,1,NULL,0,3,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (77,'2018-07-23',1,1,NULL,0,4,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (78,'2018-07-23',1,1,NULL,0,5,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (79,'2018-07-23',1,1,NULL,0,6,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (80,'2018-07-23',1,1,NULL,0,7,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (81,'2018-07-23',1,1,NULL,0,8,'P',2,2,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (82,'2018-07-23',1,1,NULL,0,9,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (83,'2018-07-23',1,1,NULL,0,10,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (84,'2018-07-23',1,1,NULL,0,11,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (85,'2018-07-23',1,1,NULL,0,12,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (86,'2018-07-23',1,1,NULL,0,13,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (87,'2018-07-23',1,1,NULL,0,14,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (88,'2018-07-23',1,1,NULL,0,15,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (89,'2018-07-23',1,1,NULL,0,16,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (90,'2018-07-23',1,1,NULL,0,17,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (91,'2018-07-23',1,1,NULL,0,18,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (92,'2018-07-23',1,1,NULL,0,19,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (93,'2018-07-23',1,1,NULL,0,20,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (94,'2018-07-23',1,1,NULL,0,21,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (95,'2018-07-23',1,1,NULL,0,22,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (96,'2018-07-23',1,1,NULL,0,23,'P',0,0,0,0,0.0000,'2018-07-23 15:21:55',0,'2018-07-23 15:21:55');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (97,'2018-07-23',5,1,NULL,0,0,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (98,'2018-07-23',5,1,NULL,0,1,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (99,'2018-07-23',5,1,NULL,0,2,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (100,'2018-07-23',5,1,NULL,0,3,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (101,'2018-07-23',5,1,NULL,0,4,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (102,'2018-07-23',5,1,NULL,0,5,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (103,'2018-07-23',5,1,NULL,0,6,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (104,'2018-07-23',5,1,NULL,0,7,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (105,'2018-07-23',5,1,NULL,0,8,'P',3,3,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (106,'2018-07-23',5,1,NULL,0,9,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (107,'2018-07-23',5,1,NULL,0,10,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (108,'2018-07-23',5,1,NULL,0,11,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (109,'2018-07-23',5,1,NULL,0,12,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (110,'2018-07-23',5,1,NULL,0,13,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (111,'2018-07-23',5,1,NULL,0,14,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (112,'2018-07-23',5,1,NULL,0,15,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (113,'2018-07-23',5,1,NULL,0,16,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (114,'2018-07-23',5,1,NULL,0,17,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (115,'2018-07-23',5,1,NULL,0,18,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (116,'2018-07-23',5,1,NULL,0,19,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (117,'2018-07-23',5,1,NULL,0,20,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (118,'2018-07-23',5,1,NULL,0,21,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (119,'2018-07-23',5,1,NULL,0,22,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (120,'2018-07-23',5,1,NULL,0,23,'P',0,0,0,0,0.0000,'2018-07-23 16:34:05',0,'2018-07-23 16:34:05');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (121,'2018-07-23',6,2,19,0,0,'P',0,0,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (122,'2018-07-23',6,2,19,0,1,'P',0,0,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (123,'2018-07-23',6,2,19,0,2,'P',0,0,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (124,'2018-07-23',6,2,19,0,3,'P',0,0,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (125,'2018-07-23',6,2,19,0,4,'P',0,0,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (126,'2018-07-23',6,2,19,0,5,'P',0,0,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (127,'2018-07-23',6,2,19,0,6,'P',0,0,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (128,'2018-07-23',6,2,19,0,7,'P',0,0,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (129,'2018-07-23',6,2,19,0,8,'P',22,22,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (130,'2018-07-23',6,2,19,0,9,'P',20,20,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (131,'2018-07-23',6,2,19,0,10,'P',20,20,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (132,'2018-07-23',6,2,19,0,11,'P',20,20,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (133,'2018-07-23',6,2,19,0,12,'P',20,20,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (134,'2018-07-23',6,2,19,0,13,'P',0,0,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (135,'2018-07-23',6,2,19,0,14,'P',20,20,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (136,'2018-07-23',6,2,19,0,15,'P',20,20,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (137,'2018-07-23',6,2,19,0,16,'P',20,20,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (138,'2018-07-23',6,2,19,0,17,'P',20,20,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (139,'2018-07-23',6,2,19,0,18,'P',20,20,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (140,'2018-07-23',6,2,19,0,19,'P',20,20,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (141,'2018-07-23',6,2,19,0,20,'U',2,2,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:54');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (142,'2018-07-23',6,2,19,0,21,'P',0,0,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (143,'2018-07-23',6,2,19,0,22,'P',0,0,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');
insert  into `daily_target_summary`(`id`,`l_date`,`line_no`,`order_id`,`target_id`,`unit_setting_id`,`hour_no`,`hour_planned_status`,`initial_target`,`target`,`output`,`line_def`,`output_percentage`,`created_at`,`created_by`,`updated_at`) values (144,'2018-07-23',6,2,19,0,23,'P',0,0,0,0,0.0000,'2018-07-23 17:07:31',0,'2018-07-23 17:07:31');

/*Table structure for table `daily_targets` */

CREATE TABLE `daily_targets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `line_no` int(11) NOT NULL,
  `num_of_machine` int(11) DEFAULT '0',
  `num_of_hours` varchar(245) DEFAULT NULL,
  `todays_target` int(11) NOT NULL,
  `status` int(11) DEFAULT '0',
  `sam` double DEFAULT NULL,
  `a_machine` int(10) DEFAULT '0',
  `a_operator` int(10) DEFAULT '0',
  `p_operator` int(10) DEFAULT '0',
  `s_helper_ironman` int(10) DEFAULT '0',
  `li_sv` int(10) DEFAULT '0',
  `remarks` text,
  `created_by` int(10) NOT NULL,
  `updated_by` int(10) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

/*Data for the table `daily_targets` */

insert  into `daily_targets`(`id`,`order_id`,`line_no`,`num_of_machine`,`num_of_hours`,`todays_target`,`status`,`sam`,`a_machine`,`a_operator`,`p_operator`,`s_helper_ironman`,`li_sv`,`remarks`,`created_by`,`updated_by`,`created_at`,`updated_at`) values (17,1,1,0,'8,9,10,11,12,14,15,16,17,18,19',2,1,2,2,2,2,2,2,'2',0,0,'2018-07-23 15:21:55','2018-07-23 15:21:55');
insert  into `daily_targets`(`id`,`order_id`,`line_no`,`num_of_machine`,`num_of_hours`,`todays_target`,`status`,`sam`,`a_machine`,`a_operator`,`p_operator`,`s_helper_ironman`,`li_sv`,`remarks`,`created_by`,`updated_by`,`created_at`,`updated_at`) values (18,1,5,0,'8,9,10,11,12,14,15,16,17,18,19',3,1,23,3,3,3,3,3,NULL,0,0,'2018-07-23 16:34:05','2018-07-23 16:34:05');
insert  into `daily_targets`(`id`,`order_id`,`line_no`,`num_of_machine`,`num_of_hours`,`todays_target`,`status`,`sam`,`a_machine`,`a_operator`,`p_operator`,`s_helper_ironman`,`li_sv`,`remarks`,`created_by`,`updated_by`,`created_at`,`updated_at`) values (19,2,6,0,'8,9,10,11,12,14,15,16,17,18,19,20',224,1,2,2,2,2,2,2,NULL,0,0,'2018-07-23 17:07:31','2018-07-23 17:07:54');

/*Table structure for table `items` */

CREATE TABLE `items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `items` */

insert  into `items`(`id`,`item_name`,`created_at`,`updated_at`) values (1,'w','2018-07-23 14:09:42','2018-07-23 14:09:42');
insert  into `items`(`id`,`item_name`,`created_at`,`updated_at`) values (2,'q','2018-07-23 14:10:55','2018-07-23 14:10:55');

/*Table structure for table `machine_data` */

CREATE TABLE `machine_data` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `unit_id` int(10) DEFAULT NULL,
  `rented_machine` int(10) NOT NULL,
  `idle_machine` int(10) NOT NULL,
  `used_machine` int(10) NOT NULL,
  `sample_others` int(10) NOT NULL,
  `sample_section` int(10) DEFAULT NULL,
  `finishing_section` int(10) DEFAULT NULL,
  `cutting_section` int(10) DEFAULT NULL,
  `power` int(10) DEFAULT NULL,
  `mvc_godwon` int(10) DEFAULT NULL,
  `total` int(10) NOT NULL,
  `date` date NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `machine_data` */

insert  into `machine_data`(`id`,`unit_id`,`rented_machine`,`idle_machine`,`used_machine`,`sample_others`,`sample_section`,`finishing_section`,`cutting_section`,`power`,`mvc_godwon`,`total`,`date`,`created_by`,`created_at`,`updated_at`) values (1,NULL,0,0,0,0,0,0,0,0,0,0,'2018-07-12',0,'2018-07-12 07:28:11','2018-07-12 07:28:11');

/*Table structure for table `manpower` */

CREATE TABLE `manpower` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `unit_id` int(10) DEFAULT NULL,
  `line_no` int(10) DEFAULT NULL,
  `p_operator` int(10) NOT NULL,
  `p_swing_iron` int(10) NOT NULL,
  `p_lc_sv` int(10) NOT NULL,
  `total_manpower` int(10) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `manpower` */

insert  into `manpower`(`id`,`unit_id`,`line_no`,`p_operator`,`p_swing_iron`,`p_lc_sv`,`total_manpower`,`date`,`created_at`,`updated_at`) values (1,2,1,1,1000,4,0,'2018-07-17','2018-07-11 09:02:20','2018-07-17 12:11:47');
insert  into `manpower`(`id`,`unit_id`,`line_no`,`p_operator`,`p_swing_iron`,`p_lc_sv`,`total_manpower`,`date`,`created_at`,`updated_at`) values (2,2,2,1,5,4,0,'2018-07-17','2018-07-11 10:08:14','2018-07-17 12:11:46');

/*Table structure for table `migrations` */

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1);
insert  into `migrations`(`id`,`migration`,`batch`) values (2,'2014_10_12_100000_create_password_resets_table',1);
insert  into `migrations`(`id`,`migration`,`batch`) values (3,'2018_02_05_050414_entrust_setup_tables',2);

/*Table structure for table `models` */

CREATE TABLE `models` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `model_name` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `models` */

insert  into `models`(`id`,`model_name`,`created_at`,`updated_at`) values (1,'eeee','2018-07-23 14:17:50','2018-07-23 14:17:50');

/*Table structure for table `npt_report` */

CREATE TABLE `npt_report` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tch` int(10) NOT NULL,
  `mr` int(10) NOT NULL,
  `mch` int(10) NOT NULL,
  `cd` int(10) NOT NULL,
  `ped` int(10) NOT NULL,
  `pd` int(10) NOT NULL,
  `pl` int(10) NOT NULL,
  `qa` int(10) NOT NULL,
  `mb` int(10) NOT NULL,
  `ad` int(10) NOT NULL,
  `fd` int(10) NOT NULL,
  `pf` int(10) NOT NULL,
  `vp` int(10) NOT NULL,
  `date` date NOT NULL,
  `unit_id` int(10) DEFAULT NULL,
  `line` int(10) DEFAULT NULL,
  `hour_no` int(10) DEFAULT NULL,
  `order_no` int(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `npt_report` */

insert  into `npt_report`(`id`,`tch`,`mr`,`mch`,`cd`,`ped`,`pd`,`pl`,`qa`,`mb`,`ad`,`fd`,`pf`,`vp`,`date`,`unit_id`,`line`,`hour_no`,`order_no`,`created_by`,`created_at`,`updated_at`) values (1,0,0,0,0,0,0,0,0,0,0,0,0,0,'2018-07-18',2,1,8,13,0,'2018-07-12 06:11:56','2018-07-18 14:01:21');
insert  into `npt_report`(`id`,`tch`,`mr`,`mch`,`cd`,`ped`,`pd`,`pl`,`qa`,`mb`,`ad`,`fd`,`pf`,`vp`,`date`,`unit_id`,`line`,`hour_no`,`order_no`,`created_by`,`created_at`,`updated_at`) values (2,0,0,0,0,0,0,0,0,0,0,0,0,0,'2018-07-18',2,1,9,13,0,'2018-07-12 06:21:37','2018-07-18 14:01:18');

/*Table structure for table `order_info` */

CREATE TABLE `order_info` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `unit_id` int(10) NOT NULL,
  `buyer_id` int(10) NOT NULL,
  `style_id` int(10) NOT NULL,
  `model_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `start_date` date NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `order_info` */

insert  into `order_info`(`id`,`unit_id`,`buyer_id`,`style_id`,`model_id`,`item_id`,`start_date`,`created_by`,`created_at`,`updated_at`) values (1,2,1,1,1,1,'2018-07-21',0,'2018-07-11 16:35:32','2018-07-23 15:12:50');
insert  into `order_info`(`id`,`unit_id`,`buyer_id`,`style_id`,`model_id`,`item_id`,`start_date`,`created_by`,`created_at`,`updated_at`) values (2,2,1,1,1,1,'2018-07-21',0,'2018-07-23 14:50:38','2018-07-23 15:12:56');

/*Table structure for table `password_resets` */

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `permission_role` */

CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `permission_role` */

insert  into `permission_role`(`permission_id`,`role_id`) values (1,1);

/*Table structure for table `permissions` */

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`name`,`display_name`,`description`,`created_at`,`updated_at`) values (1,'create-post','Create Posts','create new blog posts','2018-02-05 05:52:52','2018-02-05 05:52:52');

/*Table structure for table `role_user` */

CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `role_user` */

insert  into `role_user`(`user_id`,`role_id`) values (1,2);
insert  into `role_user`(`user_id`,`role_id`) values (2,1);
insert  into `role_user`(`user_id`,`role_id`) values (3,1);
insert  into `role_user`(`user_id`,`role_id`) values (4,1);
insert  into `role_user`(`user_id`,`role_id`) values (5,1);

/*Table structure for table `roles` */

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`display_name`,`description`,`created_at`,`updated_at`) values (1,'superadmin','Project Owner','User is the owner of a given project','2018-02-05 05:42:19','2018-02-05 05:42:19');
insert  into `roles`(`id`,`name`,`display_name`,`description`,`created_at`,`updated_at`) values (2,'superadmin3','Super Admin','User is owner of this',NULL,NULL);

/*Table structure for table `styles` */

CREATE TABLE `styles` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `style_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `styles` */

insert  into `styles`(`id`,`style_name`,`created_at`,`updated_at`) values (1,'w','2018-07-23 13:35:14','2018-07-23 13:35:14');
insert  into `styles`(`id`,`style_name`,`created_at`,`updated_at`) values (2,'t','2018-07-23 14:40:51','2018-07-23 14:40:51');
insert  into `styles`(`id`,`style_name`,`created_at`,`updated_at`) values (3,'ty','2018-07-23 14:41:13','2018-07-23 14:41:13');
insert  into `styles`(`id`,`style_name`,`created_at`,`updated_at`) values (4,'we','2018-07-23 14:42:22','2018-07-23 14:42:22');

/*Table structure for table `unit_setting` */

CREATE TABLE `unit_setting` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `unit_id` int(10) NOT NULL,
  `floor_name` varchar(100) DEFAULT NULL,
  `line_no` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `unit_setting` */

insert  into `unit_setting`(`id`,`unit_id`,`floor_name`,`line_no`,`created_at`,`updated_at`) values (1,2,'Old building-(2nd floor)',1,'2018-07-18 16:31:56','2018-07-18 16:31:56');
insert  into `unit_setting`(`id`,`unit_id`,`floor_name`,`line_no`,`created_at`,`updated_at`) values (2,2,'Old Building-(3rd Floor)',2,'2018-07-18 16:32:32','2018-07-18 16:32:36');
insert  into `unit_setting`(`id`,`unit_id`,`floor_name`,`line_no`,`created_at`,`updated_at`) values (3,2,'Old Building-(4-th Floor)',3,'2018-07-18 16:33:06','2018-07-18 16:33:06');
insert  into `unit_setting`(`id`,`unit_id`,`floor_name`,`line_no`,`created_at`,`updated_at`) values (4,2,'Old Building-(First Floor)',4,'2018-07-18 16:33:34','2018-07-18 16:33:34');
insert  into `unit_setting`(`id`,`unit_id`,`floor_name`,`line_no`,`created_at`,`updated_at`) values (5,2,' Steel Building-(First Floor)',5,'2018-07-18 16:34:42','2018-07-18 16:34:42');
insert  into `unit_setting`(`id`,`unit_id`,`floor_name`,`line_no`,`created_at`,`updated_at`) values (6,2,' Steel Building-(First Floor)2',6,'2018-07-18 16:34:50','2018-07-18 16:34:50');

/*Table structure for table `units` */

CREATE TABLE `units` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `units` */

insert  into `units`(`id`,`unit_name`,`created_at`,`updated_at`,`deleted_at`) values (1,'Lily','2018-07-12 23:33:36','2018-07-12 23:33:36',NULL);
insert  into `units`(`id`,`unit_name`,`created_at`,`updated_at`,`deleted_at`) values (2,'CGL','2018-07-12 23:33:36','2018-07-12 23:33:36',NULL);

/*Table structure for table `users` */

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `msisdn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`status`,`msisdn`,`created_at`,`updated_at`) values (1,'test','abc','$2y$10$CM8iU8W0t9KzgrbShXKG9uuLElOhBzFyoLjLT608uhig/Fik5Vzoe',NULL,NULL,'0198-7704413','2018-02-05 05:29:39','2018-05-28 06:18:59');
insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`status`,`msisdn`,`created_at`,`updated_at`) values (2,'sss','avs','$2y$10$zA5CfuODjUGXJs3sd2zdNO6iqlYNOqDx1e.9rG5iMbFFU794YO7/6',NULL,NULL,'0198-7704411','2018-02-05 05:34:11','2018-02-05 05:34:11');
insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`status`,`msisdn`,`created_at`,`updated_at`) values (3,'Super Admin','superadmin','$2y$10$zA5CfuODjUGXJs3sd2zdNO6iqlYNOqDx1e.9rG5iMbFFU794YO7/6','goMI016psGgw2Sxoqj4UNbq6AOHyuKX5iPiS3muk7BDQJEpCfZH4OrmBCkvX',NULL,NULL,NULL,NULL);
insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`status`,`msisdn`,`created_at`,`updated_at`) values (4,'dd','dd','$2y$10$ijx32jfx3.LnwA.kgpCWCerQRUWlRRbmD5kuWF1gYvDi/XpJj2mJi','DFgf5aYIKKsPvGECGpEh20Fs3TCnxKeYuNvzWYH8iryRkvDXe2fpuNa4QDxT',NULL,NULL,'2018-04-01 06:47:26','2018-04-01 06:47:26');
insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`status`,`msisdn`,`created_at`,`updated_at`) values (5,'22','suman@yhppps.com','$2y$10$YMNCXslNJSGgJmSbUGbekuOdlbPX8ffHAM7IUM7zAqSAWAbQWfeQy','uuQiWO7NAyE2LKfPtRINbfrD098NI6WdGvKWuVpQfRIwGE9mcpnGN9NK9F8t',0,'1234567890','2018-06-03 07:13:27','2018-06-03 07:13:27');

/*Table structure for table `users_unit` */

CREATE TABLE `users_unit` (
  `user_id` int(10) DEFAULT NULL,
  `unit_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `users_unit` */

insert  into `users_unit`(`user_id`,`unit_id`) values (3,2);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
