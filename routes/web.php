<?php

Route::auth();

Route::get('/', function () {
    if (Auth::check()) {

        $roles = \Auth::user()->roles;
        
        if(!count($roles))
        {
           return redirect()->route('login');
        }

        $role = $roles->first()->name;

        if($role == "superadmin"){
            return redirect()->route('superadmin.dashboard');
        }
        if($role == "distributoradmin"){
            return redirect()->route('distributoradmin.dashboard');
        }

        return redirect()->route('default-user.dashboard');
    }
    return redirect()->route('login');
});

Route::get('logout', function (){
    \Auth::logout();
    return redirect('/');
});
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    Artisan::call('Cache::flush()');
    
});

Route::get('clear-all',function(){
 \Illuminate\Support\Facades\Artisan::call('view:clear');
 \Illuminate\Support\Facades\Artisan::call('config:clear');
 \Illuminate\Support\Facades\Artisan::call('cache:clear');
 \Illuminate\Support\Facades\Artisan::call('config:cache');
 \Illuminate\Support\Facades\Artisan::call('clear-compiled');
 \Illuminate\Support\Facades\Artisan::call('route:clear');

}); 

Route::get('/seed-data', function(){
   $user=App\User::first();
  // dd($user);
   $user->password=\Hash::make('123456');
   $user->update();

});

