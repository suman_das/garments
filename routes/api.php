<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/test',function(){
     return "ok"; 
});

Route::group(['prefix' => 'v1'], function(){
  Route::post('login', 'ApiController@login');
  Route::get('pushpullsms', 'ApiController@pushpullsms');
  Route::post('pushpullsms', 'ApiController@pushpullsms');
  
  Route::get('getpoint', 'ApiController@getpoint');
  Route::post('getpoint', 'ApiController@getpoint');
  
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::get('user', 'ApiController@getAuthUser');
		Route::get('products', 'ApiController@products');
		Route::get('traders', 'ApiController@traders');
		Route::get('saveinformation', 'ApiController@saveinformation');
                Route::post('saveinformation', 'ApiController@saveinformation');
		Route::get('saveRedeemInformation', 'ApiController@saveRedeemInformation');
    });
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
