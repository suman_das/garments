<?php

Route::group(['middleware' => ['auth']], function() {

    Route::group([
    'namespace' => 'SuperAdmin',
    'as' => 'superadmin.',
    'prefix' => 'superadmin'], function(){

       
        
        Route::group(['namespace' => 'Report', 'as' => 'report.'], function() {
            Route::group(['prefix' => 'detailsreport', 'as' => 'detailsreport.'], function(){
                Route::post('/detaisldata', ['as' => 'detaisldata','name'=>'detaisldata', 'uses' => 'DetailsReportController@dataTableList']);
                Route::get('/downloadReport', ['as' => 'downloadReport','name'=>'downloadReport', 'uses' => 'DetailsReportController@downloadExcel']);
                Route::get('/', ['as' => 'index', 'uses' => 'DetailsReportController@report']);
           });
        });
        
        Route::group(['namespace' => 'Manpower', 'as' => 'manpower.','prefix' => 'manpower',], function() {
            Route::get('/create', ['as' => 'create', 'uses' => 'ManpowerController@create']);
            Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'ManpowerController@edit']);
            Route::post('/update/{id}', ['as' => 'update', 'uses' => 'ManpowerController@update']);
            Route::get('/index', ['as' => 'index', 'uses' => 'ManpowerController@index']);
            Route::post('/store', ['as' => 'store', 'uses' => 'ManpowerController@store']);
        });
        Route::group(['namespace' => 'Order', 'as' => 'order.','prefix' => 'order',], function() {
            Route::get('/create', ['as' => 'create', 'uses' => 'OrderController@create']);
            Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'OrderController@edit']);
            Route::post('/update/{id}', ['as' => 'update', 'uses' => 'OrderController@update']);
            Route::get('/index', ['as' => 'index', 'uses' => 'OrderController@index']);
            Route::post('/store', ['as' => 'store', 'uses' => 'OrderController@store']);
        });
        Route::group(['namespace' => 'DeptNPT', 'as' => 'npt.','prefix' => 'npt',], function() {
            Route::get('/create', ['as' => 'create', 'uses' => 'DeptNPTController@create']);
            Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'DeptNPTController@edit']);
            Route::post('/update/{id}', ['as' => 'update', 'uses' => 'DeptNPTController@update']);
            Route::get('/index', ['as' => 'index', 'uses' => 'DeptNPTController@index']);
            Route::post('/store', ['as' => 'store', 'uses' => 'DeptNPTController@store']);
        });
        Route::group(['namespace' => 'MachineData', 'as' => 'machinedata.','prefix' => 'machinedata',], function() {
         Route::get('/create', ['as' => 'create', 'uses' => 'MachineDataController@create']);
         Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'MachineDataController@edit']);
         Route::post('/update/{id}', ['as' => 'update', 'uses' => 'MachineDataController@update']);
         Route::get('/index', ['as' => 'index', 'uses' => 'MachineDataController@index']);
         Route::post('/store', ['as' => 'store', 'uses' => 'MachineDataController@store']);
        });
        
        Route::group(['namespace' => 'Target', 'as' => 'target.','prefix' => 'target',], function() {
         Route::get('/create', ['as' => 'create', 'uses' => 'TargetController@create']);
         Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'TargetController@edit']);
         Route::post('/update/{id}', ['as' => 'update', 'uses' => 'TargetController@update']);
         Route::get('/index', ['as' => 'index', 'uses' => 'TargetController@index']);
         Route::post('/store', ['as' => 'store', 'uses' => 'TargetController@store']);
        });
        
        

        Route::group(['namespace' => 'UserManagement', 'prefix' => 'user-management', 'as' => 'user-management.'], function(){
            Route::get('/password-change/{id}', ['as' => "password-change", 'uses' => 'PasswordController@get']);
            Route::post('/password-change/{id}', ['as' => "password-change.post", 'uses' => 'PasswordController@change']);

            Route::post('/update/{id}', ['as' => 'update', 'uses' => 'UserController@update']);
            Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'UserController@edit']);

            Route::get('/show/{id}', ['as' => 'show', 'uses' => 'UserController@show']);

            Route::post('/store', ['as' => 'store',  'uses' => 'UserController@store']);
            Route::get('/create', ['as' => 'create', 'uses' => 'UserController@create']);

            Route::get('/',       ['as' => 'index',  'uses' => 'UserController@index']);
            
        });
        
        Route::group(['namespace' => 'UserManagement', 'prefix' => 'retailer-management', 'as' => 'retailer-management.'], function(){
            Route::get('districtlist', ['as' => 'districtlist', 'uses' => 'RetailerController@districtlist']);
            Route::get('/downloadReport', ['as' => 'downloadReport','name'=>'downloadReport', 'uses' => 'RetailerController@downloadExcel']);
            Route::get('retailerList', ['as' => 'retailerList', 'uses' => 'RetailerController@retailerList']);
            Route::get('userDetails', ['as' => 'userDetails', 'uses' => 'RetailerController@userDetails']);
            Route::post('/update/{id}', ['as' => 'update', 'uses' => 'RetailerController@update']);
            Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'RetailerController@edit']);
            Route::get('distributorList', ['as' => 'distributorList', 'uses' => 'RetailerController@distributorList']);

            Route::get('/show/{id}', ['as' => 'show', 'uses' => 'RetailerController@show']);

            Route::post('/store', ['as' => 'store',  'uses' => 'RetailerController@store']);
            Route::get('/create', ['as' => 'create', 'uses' => 'RetailerController@create']);
            Route::get('/',       ['as' => 'index',  'uses' => 'RetailerController@index']);
            
            
        });
        
        


        Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@dashboard']);
    });
});