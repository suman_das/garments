<?php

Route::group(['middleware' => ['auth']], function() {

    Route::group([
    'namespace' => 'DistributorAdmin',
    'as' => 'distributoradmin.',
    'prefix' => 'distributoradmin'], function(){

        Route::group(['namespace' => 'Registration', 'as' => 'registration.'], function() {
            Route::group(['prefix' => 'participants', 'as' => 'participants.'], function(){
                Route::get('/download', ['as' => 'download', 'uses' => 'ParticipantController@download']);
                Route::get('/show/{id}', ['as' => 'show', 'uses' => 'ParticipantController@show']);
                Route::get('/', ['as' => 'index', 'uses' => 'ParticipantController@index']);
           });
        });
        
        Route::group(['namespace' => 'Report', 'as' => 'report.'], function() {
            Route::group(['prefix' => 'detailsreport', 'as' => 'detailsreport.'], function(){
                Route::post('/detaisldata', ['as' => 'detaisldata','name'=>'detaisldata', 'uses' => 'DetailsReportController@dataTableList']);
                Route::get('/downloadReport', ['as' => 'downloadReport','name'=>'downloadReport', 'uses' => 'DetailsReportController@downloadExcel']);
                Route::get('/', ['as' => 'index', 'uses' => 'DetailsReportController@report']);
           });
        });
        
        Route::group(['namespace' => 'Excell', 'as' => 'product-upload.','prefix' => 'product-upload',], function() {
            Route::group(['prefix' => 'imeinformation', 'as' => 'imeinformation.'], function(){
                Route::post('/upload', ['as' => 'upload', 'uses' => 'uploadCodeInfoController@stoeBulkCode']);
                Route::get('/', ['as' => 'index', 'uses' => 'uploadCodeInfoController@createBulkCode']);
                
           });
           Route::group(['prefix' => 'dealerimeiinformation', 'as' => 'dealerimeiinformation.'], function(){
                Route::post('/upload', ['as' => 'upload', 'uses' => 'uploadDealerProductController@storeBulkCode']);
                Route::get('/', ['as' => 'index', 'uses' => 'uploadDealerProductController@createBulkCode']);
                
           });
        });

        Route::group(['namespace' => 'UserManagement', 'prefix' => 'retail-management', 'as' => 'retail-management.'], function(){

            Route::post('/status-change/{id}', ['as' => 'status-change', 'uses' => 'StatusController@change']);

            Route::get('/password-change/{id}', ['as' => "password-change", 'uses' => 'PasswordController@get']);
            Route::post('/password-change/{id}', ['as' => "password-change.post", 'uses' => 'PasswordController@change']);

            Route::post('/update/{id}', ['as' => 'update', 'uses' => 'UserController@update']);
            Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'UserController@edit']);

            Route::get('/show/{id}', ['as' => 'show', 'uses' => 'UserController@show']);

            Route::post('/store', ['as' => 'store',  'uses' => 'UserController@store']);
            Route::get('/create', ['as' => 'create', 'uses' => 'UserController@create']);

            Route::get('/',       ['as' => 'index',  'uses' => 'UserController@index']);
        });


        Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@dashboard']);
    });
});