function get_states(country_id){
	$('#state_id').html('<option selected="selected" value="">Loading...</option>');
	var url = site_path + 'location/states/'+country_id;

	$.getJSON(url+'?callback=?',function(data){

		$('#state_id').html('<option selected="selected" value="">Select State</option>');
		$.each(data, function(key, item) { 
		    $('#state_id').append('<option value="'+item.id+'">'+item.name+'</option>');
		});

	});
}

function get_cities(state_id){
	var url = site_path + 'location/cities/'+state_id;

	$.getJSON(url+'?callback=?',function(data){

		$('#city_id').html('<option selected="selected" value="">Select City</option>');
		$.each(data, function(key, item) { 
		    $('#city_id').append('<option value="'+item.id+'">'+item.name+'</option>');
		});

	});
}

function get_zones(city_id){
	var url = site_path + 'location/zones/'+city_id;

	$.getJSON(url+'?callback=?',function(data){

		$('#zone_id').html('<option selected="selected" value="">Select Zone</option>');
		$.each(data, function(key, item) { 
		    $('#zone_id').append('<option value="'+item.id+'">'+item.name+'</option>');
		});

	});
}

$(".js-example-basic-single").select2({
    // placeholder : 'SELECT ONE',
    // allowClear : true
}).trigger('change');